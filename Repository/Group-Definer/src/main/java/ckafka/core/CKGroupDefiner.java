package ckafka.core;

import ckafka.connection.CKConsumer;
import ckafka.connection.CKProducer;
import ckafka.connection.ConsumerListener;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.ScheduledThreadPoolExecutor;

public class CKGroupDefiner implements ConsumerListener {


    /**
     *  Logger
     */
    final Logger logger = LoggerFactory.getLogger(CKGroupDefiner.class);

    private CKProducer ckProducer;

    private CKConsumer ckConsumer;

    private String serialize;
    private String deserialize;

    /**
     *  ThreadPoll
     */
    private final ScheduledThreadPoolExecutor threadPool;

    /**
     *  Selector: responsible for the GD logic
     */
    private CKGroupDefinerSelector selector;

    private UUID applicationID;

    public CKGroupDefiner(CKGroupDefinerSelector Selector){

        selector = Selector;

        applicationID = UUID.randomUUID();

        deserialize = StringDeserializer.class.getName();
        serialize = StringSerializer.class.getName();

        threadPool = new ScheduledThreadPoolExecutor(1000);

        try{
            logger.info("Starting Producer");

            ckProducer = new CKProducer(serialize, serialize);

        }catch (Exception e){
            logger.error("Error starting producer", e);
            System.exit(1);
        }

        try {
            logger.info("Starting Consumer");

            List<String> topics = GetListOfTopicsFromFile();

            ckConsumer = new CKConsumer(topics, deserialize, deserialize);
            ckConsumer.addConsumerListener(this);
            ckConsumer.start();

        }catch (Exception e){
            logger.error("Error starting consumer", e);
            System.exit(1);
        }

        logger.info("Application Started");
    }

    @Override
    public void RecordReceived(ConsumerRecord Record) {
        logger.info("Record Received " + Record.toString());

        try {
            ObjectNode dataRecord = new ObjectMapper().readValue(Record.value().toString(), ObjectNode.class);

            Integer group = selector.GetGroupFromContext(dataRecord);
            dataRecord.put("group", group.toString());

            String GroupAdvertisement = "GroupAdvertisement";

            ProducerRecord<String, String> newRecord = new ProducerRecord<String,String>(GroupAdvertisement,
                    Record.key().toString(), dataRecord.toString());

            SendRecordToGateway(newRecord);

        } catch (IOException e) {
            logger.error("Could not read record value", e);
            return;
        }catch (Exception ex){
            logger.error("Error processing Record Received", ex);
        }


    }

    private void SendRecordToGateway(ProducerRecord Record){

        logger.info("Sending Record To Gateway");

        try {
            threadPool.execute(ckProducer.SendTopic(Record));
        }catch (Exception e){
            logger.error("Error sending topic", e);
        }
    }

    /**
     * Get Topics from file properties.xml file
     *
     * @pre
     * @post
     */
    private List<String> GetListOfTopicsFromFile() throws Exception {
        try {

            logger.info("Get Topics to Consumer from file");

            String rootPath = Thread.currentThread().getContextClassLoader().getResource("").getPath();
            String mnConfigPath = rootPath + "properties.xml";
            Properties mnProps = new Properties();
            mnProps.loadFromXML(new FileInputStream(mnConfigPath));

            List<String> listOfTopics = Arrays.asList(mnProps.getProperty("topics").split(","));

            return listOfTopics;

        } catch (Exception e){

            logger.error("Error loading properties file", e);
            throw e;
        }
    }
}