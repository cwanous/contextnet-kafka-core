package ckafka.core;

import com.fasterxml.jackson.databind.node.ObjectNode;

public class CKGroupDefinerSelectorExample implements CKGroupDefinerSelector{
    @Override
    public int GetGroupFromContext(ObjectNode context) {
        try {
            if(context.get("positionX").asDouble() > 0.5){
                return 1;
            }else {
                return 0;
            }
        }catch (Exception e){
            return 0;
        }
    }
}
