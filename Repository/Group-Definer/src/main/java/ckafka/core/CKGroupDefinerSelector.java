package ckafka.core;

import com.fasterxml.jackson.databind.node.ObjectNode;

public interface CKGroupDefinerSelector {

    public int GetGroupFromContext(ObjectNode context);

}
