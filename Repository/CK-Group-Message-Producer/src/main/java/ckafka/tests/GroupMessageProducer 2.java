package ckafka.tests;

import ckafka.connection.CKProducer;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class GroupMessageProducer {

    /**
     *  Logger
     */
    final Logger logger = LoggerFactory.getLogger(GroupMessageProducer.class);

    private CKProducer ckProducer;

    private String serialize;

    /**
     *  ThreadPoll
     */
    private final ScheduledExecutorService schedulePool;

    public GroupMessageProducer(){

        serialize = StringSerializer.class.getName();

        try{
            logger.info("Starting Producer");

            ckProducer = new CKProducer(serialize, serialize);

        }catch (Exception e){
            logger.error("Error starting producer", e);
            System.exit(1);
        }

        ObjectNode ProducerMessage = JsonNodeFactory.instance.objectNode();
        ProducerMessage.put("group", 0);

        String GroupMessageTopic = "GroupMessageTopic";

        ProducerRecord<String, String> Record = new ProducerRecord<String,String>(GroupMessageTopic,
                GroupMessageProducer.class.toString(), ProducerMessage.toString());

        schedulePool = Executors.newScheduledThreadPool(1000);
        this.schedulePool.scheduleAtFixedRate(this.ckProducer.SendTopic(Record),10, 20, TimeUnit.SECONDS);

    }

    public static void main(String[] args) {

        new GroupMessageProducer();
    }
}

