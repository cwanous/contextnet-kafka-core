package exemples;

import ckafka.GroupDefiner;
import ckafka.GroupSelection;
import ckafka.data.Swap;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;


public class GD implements GroupSelection {

    final Logger logger = LoggerFactory.getLogger(GD.class);

    private String position = "position";
    private Integer groupX;
    private Integer groupY;

    public static void main(String[] args) {
        new GD();
    }

    public GD(){
        ObjectMapper objectMapper = new ObjectMapper();
        Swap swap = new Swap(objectMapper);
        GetEnvVariables();
        new GroupDefiner(this, swap);
    }

    public Set<Integer> GroupsIdentification() {
        Set<Integer> setOfGroups = new HashSet<Integer>();
        setOfGroups.add(groupX);
        setOfGroups.add(groupY);
        return setOfGroups;
    }

    public Set<Integer> GetNodesGroupByContext(ObjectNode contextInfo) {
        Set<Integer> setOfGroups = new HashSet<Integer>();
        if(contextInfo.has(position)){
            int position  = new Integer (String.valueOf(contextInfo.get(this.position)));

            if(position == 0){
                setOfGroups.add(groupX);
            }

            if(position == 1){
                setOfGroups.add(groupY);
            }
        }

        return setOfGroups;
    }

    private void GetEnvVariables() {
        Map<String, String> envVariables = System.getenv();

        groupX = new Integer(envVariables.get("gdf.groupX"));
        groupY = new Integer(envVariables.get("gdf.groupY"));
    }

    public String KafkaConsumerPrefix() {
        return "gdf.consumer";
    }

    public String KafkaProducerPrefix() {
        return "gdf.producer";
    }

}