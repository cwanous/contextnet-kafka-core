package exemples;

import ckafka.GroupDefiner;
import ckafka.GroupSelection;
import ckafka.data.Swap;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.util.HashSet;
import java.util.Set;

public class GroupDefinerOne implements GroupSelection {

    public static void main(String[] args) {
        new GroupDefinerOne();
    }

    public GroupDefinerOne(){
        ObjectMapper objectMapper = new ObjectMapper();
        Swap swap = new Swap(objectMapper);
        new GroupDefiner(this, swap);
    }

    /**
    * @return todos os grupos definidos por este Group Definer
    * */
    public Set<Integer> GroupsIdentification() {
        Set<Integer> setOfGroups = new HashSet<Integer>();
        setOfGroups.add(1);
        setOfGroups.add(2);
        return setOfGroups;
    }

    /**
     * @return retorna os grupos aos quais o MN pertence
     * */
    public Set<Integer> GetNodesGroupByContext(ObjectNode contextInfo) {
        Set<Integer> setOfGroups = new HashSet<Integer>();
        setOfGroups.add(1);
        return setOfGroups;
    }

    public String KafkaConsumerPrefix() {
        return "gd.one.consumer";
    }

    public String KafkaProducerPrefix() {
        return "gd.one.producer";
    }

}
