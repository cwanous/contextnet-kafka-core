package ckafka;

import application.ModelApplication;
import ckafka.data.GroupAdvertisement;
import ckafka.data.Swap;
import ckafka.data.SwapData;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Set;
import java.util.UUID;

public class GroupDefiner extends ModelApplication {

    /**
     *  Logger
     */
    final Logger logger = LoggerFactory.getLogger(GroupDefiner.class);

    private GroupSelection Selection;

    private final UUID GDId;

    private Swap swap;

    public GroupDefiner(GroupSelection selection, Swap swap){

        super(selection.KafkaProducerPrefix(), selection.KafkaConsumerPrefix()); // id producer and consumer
        this.swap = swap;
        Selection = selection;
        GDId = UUID.randomUUID();
    }

    @Override
    public void RecordReceived(ConsumerRecord Record) {
        logger.info("GD Record Received");
        try {
            String nodeID = Record.key().toString();

            SwapData recordContent = swap.SwapDataDeserialization((byte[]) Record.value());

            ObjectNode contextInfo = recordContent.getContext();

            if(contextInfo != null){
                Set<Integer> listOfGroups = Selection.GetNodesGroupByContext(contextInfo);
                GroupAdvertisement advertisement = new GroupAdvertisement(GDId.toString(), listOfGroups);
                SendRecord(CreateRecord("GroupAdvertisement", nodeID, swap.DataSerialization(advertisement)));
            }

        }catch (Exception e){
            logger.error("Erro on RecordReceived", e);
        }


    }
}
