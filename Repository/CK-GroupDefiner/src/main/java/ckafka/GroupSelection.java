package ckafka;

import com.fasterxml.jackson.databind.node.ObjectNode;

import java.util.Set;

public interface GroupSelection {

    public Set<Integer> GroupsIdentification();

    public Set<Integer> GetNodesGroupByContext(ObjectNode contextInfo);

    public String KafkaConsumerPrefix();

    public String KafkaProducerPrefix();
}
