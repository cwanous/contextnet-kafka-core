import application.ModelApplication;
import ckafka.data.Swap;
import ckafka.data.SwapData;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.StandardCharsets;



public class GroupMesSender extends ModelApplication {

    private final Logger logger = LoggerFactory.getLogger(GroupAdvSender.class);

    private ObjectMapper objectMapper;
    private Swap swap;

    public static void main(String[] args) {
        new GroupMesSender();
    }

    public GroupMesSender(){
        super("gm.test.pr", "gm.test.cm");

        objectMapper = new ObjectMapper();
        swap = new Swap(objectMapper);

        SendGroupMessage();
    }

    private void SendGroupMessage(){
        try{
            int GroupID = 1;
            SendRecord(CreateRecord("GroupMessageTopic", Integer.toString(GroupID), swap.SwapDataSerialization(GetSwapData())));
        }catch (Exception e)
        {
            logger.error("Error SendPrivateMessage", e);
        }
    }

    public SwapData GetSwapData()
    {
        byte[] content = "Model Application Test".getBytes(StandardCharsets.UTF_8);
        SwapData serializableData = new SwapData();
        serializableData.setMessage(content);
        serializableData.setDuration(60);
        return serializableData;
    }
}