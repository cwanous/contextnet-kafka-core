import application.ModelApplication;
import ckafka.data.GroupAdvertisement;
import ckafka.data.Swap;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Set;

public class GroupAdvSender extends ModelApplication {

    final Logger logger = LoggerFactory.getLogger(GroupAdvSender.class);

    ObjectMapper objectMapper;
    Swap swap;
    String gdID;

    public static void main(String[] args) {
        new GroupAdvSender();
    }

    public GroupAdvSender(){
        super("ga.test.pr", "ga.test.cm");

        //this.gdID = UUID.randomUUID().toString();
        this.gdID = "64aa631b-bd5f-479b-9816-b8b6f7eeb1bb";

        objectMapper = new ObjectMapper();
        swap = new Swap(objectMapper);

        String nodeID1 = "5b221977-1216-426f-b8a5-e2a718cf9eef";
        Set<Integer> set1 = new HashSet<Integer>();
        //set1.add(2);
        SendGA(nodeID1, set1);

        String nodeID2 = "c38384b0-ddb5-4a3d-a223-f8bd395edf6f";
        Set<Integer> set2 = new HashSet<Integer>();
        set2.add(1);
        SendGA(nodeID2, set2);
    }

    private void SendGA(String nodeId, Set<Integer> listOfGroups) {
        logger.info("Send GA");
        try {
            GroupAdvertisement advertisement = new GroupAdvertisement(nodeId, this.gdID, listOfGroups);
            SendRecord(CreateRecord("GroupAdvertisement", nodeId, swap.DataSerialization(advertisement)));

        }catch (Exception e) {
            logger.error("Send GA Error", e);
        }
    }
}
