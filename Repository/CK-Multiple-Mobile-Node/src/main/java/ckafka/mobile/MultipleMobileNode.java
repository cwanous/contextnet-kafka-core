package ckafka.mobile;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

public class MultipleMobileNode {

    final Logger logger = LoggerFactory.getLogger(MultipleMobileNode.class);

    private static ArrayList<CKMobileNode> ListMultipleMN;

    public static void main(String[] args) {

        ListMultipleMN = new ArrayList<>();

        /*
        String id1 = "5b221977-1216-426f-b8a5-e2a718cf9eef";
        AddNode(id1);

        String id2 = "c38384b0-ddb5-4a3d-a223-f8bd395edf6f";
        AddNode(id2);
        */

        AddMultipleRandom(10);

        Runtime.getRuntime().addShutdownHook(new Thread( () -> {
            closeMultipleMN();
        }));

    }

    private static void AddNode(String uuid){
        CKMobileNode mnPrototype = new CKMobileNode(uuid);
        ListMultipleMN.add(mnPrototype);
    }

    private static void AddMultipleRandom(int total){
        for(int i =0; i <total; i++){
            CKMobileNode mnPrototype = new CKMobileNode();
            ListMultipleMN.add(mnPrototype);
        }
    }

    public static void closeMultipleMN(){
        for(CKMobileNode MN: ListMultipleMN){
            MN.close();
        }
    }
}
