package ckafka.mobile;

import java.util.ArrayList;

public class MultipleMobileNode {

    private static ArrayList<CKMobileNode> ListMultipleMN;

    public static void main(String[] args) {

        ListMultipleMN = new ArrayList<CKMobileNode>();

        for(int i =0; i <1; i++){
            CKMobileNode mnPrototype = new CKMobileNode();
            ListMultipleMN.add(mnPrototype);
        }

        Runtime.getRuntime().addShutdownHook(new Thread( () -> {
            closeMultipleMN();
        }));

    }

    public static void closeMultipleMN(){
        for(CKMobileNode MN: ListMultipleMN){
            MN.close();
        }
    }
}
