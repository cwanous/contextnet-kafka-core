package kcore;

import application.ModelApplication;
import ckafka.data.PingConfig;
import ckafka.data.Swap;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.List;

public class SendPingConfig extends ModelApplication {
    private final Logger logger = LoggerFactory.getLogger(SendPingConfig.class);
    private static List<String> topicsList = Collections.singletonList("Teste");

    private ObjectMapper objectMapper;
    private Swap swap;

    public static void main(String[] args) {
        new SendPingConfig();
    }

    public SendPingConfig(){
        super("sp.test.pr", "sp.test.cm", topicsList);

        objectMapper = new ObjectMapper();
        swap = new Swap(objectMapper);

        SendPing();
    }

    private void SendPing() {
        try {
            String gwID = "a61ee41e-32d7-48fe-a98c-f8905b7997b6";
            PingConfig pingConfig = new PingConfig(12000, 600);
            SendRecord(CreateRecord("PingConfig", null, swap.DataSerialization(pingConfig)));
        }catch (Exception e){
            logger.error("Error SendPing", e);
        }
    }

}
