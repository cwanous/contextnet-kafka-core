package kcore;

import ckafka.connection.CKProducer;
import ckafka.data.Swap;
import ckafka.data.SwapData;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.ByteArraySerializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ScheduledThreadPoolExecutor;

public class PoAProducerTest{

    /**
     *  Logger
     */
    final Logger logger = LoggerFactory.getLogger(PoAProducerTest.class);

    /**
     * Set Data Handler
     */
    private Swap swap;
    private ObjectMapper objectMapper;

    /**
     * Gateway ThreadPoll
     */
    private ScheduledThreadPoolExecutor threadPool;

    private CKProducer ckProducer;

    public static void main(String[] args) {
        new PoAProducerTest();
    }


    public PoAProducerTest(){
        super();
        try{

            String serializeKey = StringSerializer.class.getName();
            String serializeValue = ByteArraySerializer.class.getName();
            //String serializeValue = StringSerializer.class.getName();
            this.threadPool = new ScheduledThreadPoolExecutor(1000);

            try{
                logger.info("Starting Producer");

                ckProducer = new CKProducer(serializeKey, serializeValue);

            }catch (Exception e){
                logger.error("Error starting producer", e);
                System.exit(1);
            }

            // Data Swap
            this.objectMapper = new ObjectMapper();
            this.swap = new Swap(objectMapper);

            List<String> Keys = Arrays.asList(new String[]{"a", "b", "c"});

            for(String key: Keys){
                Thread.sleep(5);
                this.threadPool.execute(this.ckProducer.SendTopic(CreateRecord(key,"2")));
            }

            Keys = Arrays.asList(new String[]{"a", "b", "c", "d", "e"});

            /*for(String key: Keys){
                Thread.sleep(5);
                this.threadPool.execute(this.ckProducer.SendTopic(CreateRecord(key,"2")));
            }*/

            Thread.sleep(2000);
            System.exit(0);

        }catch (Exception e){
            logger.error("",e);
        }

    }


    public ProducerRecord CreateRecord(String Key, String Value){
        try {
            SwapData Data = new SwapData();

            ProducerRecord<String, byte[]> record
                    = new ProducerRecord<>("topicsTradeBy", Key, swap.SwapDataSerialization(Data));

            /*ProducerRecord<String, String> record
                    = new ProducerRecord<>("topicsTrade", Key, Value);*/

            return record;

        }catch (Exception e){
            logger.error("", e);
            return null;
        }
    }


}
