package kcore;

import ckafka.core.streams.PoAStreams;
import ckafka.core.streams.util.GatewayValidation;
import org.junit.Before;
import org.junit.jupiter.api.Test;

import java.time.Instant;

class PoAStreamsTest {

    PoAStreams streams;

    @Before
    public void setup(){
    }

    @Test
    void testIsGatewayValid() {

        // loadReportWindowSize = 60000 milliseconds = 60 seconds
        GatewayValidation gwValidation = new GatewayValidation(1, 60000);

        Instant now = Instant.now();

        // record1 > lostWindows = 0
        Instant record1 = now.minusSeconds(30);
        assert gwValidation.IsGatewayValid(record1, now);

        // record2 > lostWindows = 1
        Instant record2 = now.minusSeconds(90);
        assert gwValidation.IsGatewayValid(record2, now);

        // record3 > lostWindows = 2
        Instant record3 = now.minusSeconds(150);
        assert !gwValidation.IsGatewayValid(record3, now);

        GatewayValidation gwValidation2 = new GatewayValidation(2, 60000);

        // record3 > lostWindows = 2
        assert gwValidation2.IsGatewayValid(record3, now);

        // record4 > lostWindows = 3
        Instant record4 = now.minusSeconds(210);
        assert !gwValidation.IsGatewayValid(record4, now);

    }
}
