package data;

import org.apache.kafka.clients.consumer.ConsumerRecord;

import java.time.Duration;
import java.time.Instant;

public class LRLatestTime {

    private Instant ultimateInstant;
    private Instant penultimateInstant;
    private ConsumerRecord loadReport;

    public LRLatestTime(ConsumerRecord LoadReport){
        this.ultimateInstant = Instant.now();
        this.loadReport = LoadReport;
    }

    public void UpdateLRLatestTime(ConsumerRecord LoadReport){
        this.penultimateInstant = this.ultimateInstant;
        this.ultimateInstant = Instant.now();
        this.loadReport = LoadReport;
    }

    // Latency in milliseconds
    public Long Latency(){
        try{
            if(penultimateInstant != null){
                Duration latency = Duration.between(ultimateInstant, penultimateInstant);

                return latency.toMillis();
            }else{
                return 0L;
            }
        }catch (Exception e){
            return 0L;
        }
    }

    public ConsumerRecord getLoadReport(){
        return this.loadReport;
    }

    public boolean InWindow(Integer window, Instant from){
        try{
            if(penultimateInstant != null){

                from.minusSeconds( (long) window);

                if(this.ultimateInstant.isAfter(from)){
                    return true;
                }else{
                    return false;
                }

            }else{
                return true;
            }
        }catch (Exception e){
            return true;
        }
    }

}
