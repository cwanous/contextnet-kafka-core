package data;

import ckafka.data.ConnectionReport;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.time.Instant;
import java.util.Set;

public class ConnectionStatistics {

    final Logger logger = LoggerFactory.getLogger(ConnectionStatistics.class);

    private String gatewayID;

    private ConnectionReport report;

    private DescriptiveStatistics stats;

    // Statistics
    private Double mean;
    private Double std;
    private Double median;
    private Double max;
    private Double min;

    private Set<String> nodesSet;

    private Double variance;

    private Double pingRate;

    public ConnectionStatistics(ConnectionReport connectionRecord,
                                String GatewayID){

        try{

            gatewayID = GatewayID;
            report = connectionRecord;
            nodesSet = connectionRecord.getListNodesConnected();
            stats = new DescriptiveStatistics();

            // Add durations from the ConnectionReport
            for(Instant time : connectionRecord.getListReturnTimes()) {

                Duration latency = Duration.between(connectionRecord.getPingBeginTime(), time);
                this.stats.addValue(latency.toMillis());
            }

            // Compute statistics
            try{
                mean = stats.getMean();
            }catch (Exception e){
                logger.error("Error getting mean value", e);
                mean = null;
            }

            try{
                std = stats.getStandardDeviation();
            }catch (Exception e){
                logger.error("Error getting std value", e);
                std = null;
            }

            try{
                median = stats.getPercentile(50);
            }catch (Exception e){
                logger.error("Error getting median value", e);
                median = null;
            }

            try{
                max = stats.getMax();
            }catch (Exception e){
                logger.error("Error getting max value", e);
                max = null;
            }

            try{
                min = stats.getMin();
            }catch (Exception e){
                logger.error("Error getting min value", e);
                min = null;
            }

            try{
                variance = stats.getVariance();
            }catch (Exception e){
                logger.error("Error getting variance value", e);
                variance = null;
            }

            try{
                pingRate = ((double) connectionRecord.getListReturnTimes().size()
                        /(double) connectionRecord.getListNodesConnected().size());
            }catch (Exception e){
                logger.error("Error getting pingRate value", e);
                pingRate = null;
            }

        }catch (Exception e){
            throw e;
        }

    }

    public String getGatewayID() {
        return gatewayID;
    }

    public ConnectionReport getReport() {
        return report;
    }

    public Double getMean() {
        return mean;
    }

    public Double getStandardDeviation() {
        return std;
    }

    public Double getMedian() {
        return median;
    }

    public Double getMax() {
        return max;
    }

    public Double getMin() {
        return min;
    }

    public Double getVariance() {
        return variance;
    }

    public Double getPingRate() {
        return pingRate;
    }

    public boolean isMeanValid(){
        if(mean != null){
            return true;
        }else{
            return false;
        }
    }

    public boolean isStandardDeviationValid(){
        if(std != null){
            return true;
        }else{
            return false;
        }
    }

    public boolean isMedianValid(){
        if(median != null){
            return true;
        }else{
            return false;
        }
    }

    public boolean isMaxValid(){
        if(max != null){
            return true;
        }else{
            return false;
        }
    }

    public boolean isMinValid(){
        if(min != null){
            return true;
        }else{
            return false;
        }
    }

    public boolean isVarianceValid(){
        if(variance != null){
            return true;
        }else{
            return false;
        }
    }

    public boolean isPingRateValid(){
        if(pingRate != null){
            return true;
        }else{
            return false;
        }
    }

    public Set<String> getNodesSet() {
        return nodesSet;
    }

}
