package data;

import org.apache.kafka.clients.consumer.ConsumerRecord;

import java.time.Instant;

public class RecordWithTime {

    private ConsumerRecord record;

    private Instant inTime;

    public RecordWithTime(ConsumerRecord Record){
        this.record = Record;
        this.inTime = Instant.now();
    }

    public ConsumerRecord getRecord(){
        return this.record;
    }

    public Instant getInTime(){
        return this.inTime;
    }
}
