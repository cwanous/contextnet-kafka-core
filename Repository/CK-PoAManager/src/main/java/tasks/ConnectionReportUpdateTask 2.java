package tasks;

import ckafka.core.streams.PoAStreams;
import data.ConnectionStatistics;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;

public class ConnectionReportUpdateTask implements Callable<Map<String, ConnectionStatistics>> {

    /**
     *  Logger
     */
    private final Logger logger = LoggerFactory.getLogger(ConnectionReportUpdateTask.class);

    private PoAStreams poAStreams;

    private CountDownLatch latch;

    public ConnectionReportUpdateTask(CountDownLatch Latch, PoAStreams PoAStreams){
        this.latch = Latch;
        this.poAStreams = PoAStreams;
    }


    @Override
    public Map<String, ConnectionStatistics> call() {
        try {
            logger.info("Start running Connection Report Update Task");

            Map<String, ConnectionStatistics> connectionMap = this.poAStreams.GetGatewayConnectionMap();

            // Notifying end to RankAlgorithmTask
            latch.countDown();

            return connectionMap;

        } catch (Exception e) {
            logger.info("Error running Connection Report Update Task", e);
            return null;
        }
    }
}
