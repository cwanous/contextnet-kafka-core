package tasks;

import ckafka.core.streams.PoAStreams;
import data.ConnectionStatistics;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class ReallocationTask implements Runnable {

    /**
     *  Logger
     */
    private final Logger logger = LoggerFactory.getLogger(ReallocationTask.class);

    private PoAStreams poAStreams;

    private Map<String, Integer>  gatewayRank;

    private Map<String, ConnectionStatistics> connectionMap;


    public ReallocationTask(PoAStreams PoAStreams,
                            Map<String, Integer>  GatewayRank,
                            Map<String, ConnectionStatistics> connectionMap){
        this.poAStreams = PoAStreams;
        this.gatewayRank = GatewayRank;
        this.connectionMap = connectionMap;
    }

    @Override
    public void run() {
        logger.info("Start running Reallocation Task");
        try {
            // Reallocation
            this.poAStreams.Reallocation(this.gatewayRank, this.connectionMap);

        } catch (Exception e) {
            logger.error("Error running ReallocationTask", e);
        }

    }
}
