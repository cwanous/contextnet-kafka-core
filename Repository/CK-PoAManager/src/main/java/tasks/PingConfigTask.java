package tasks;

import ckafka.core.streams.PoAStreams;
import data.ConnectionStatistics;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class PingConfigTask implements Runnable{

    /**
     *  Logger
     */
    private final Logger logger = LoggerFactory.getLogger(PingConfigTask.class);

    private PoAStreams poAStreams;

    private Map<String, ConnectionStatistics> gatewayConnectionMap;

    public PingConfigTask(PoAStreams PoAStreams, Map<String, ConnectionStatistics> GatewayConnectionMap){
        this.poAStreams = PoAStreams;
        this.gatewayConnectionMap = GatewayConnectionMap;
    }

    @Override
    public void run() {
        logger.info("Starting Ping Config Task");
        try{

            this.poAStreams.PingReconfiguration(this.gatewayConnectionMap);

        }catch (Exception e){
            logger.error("Error executing Ping Config Task",e );
        }
    }
}
