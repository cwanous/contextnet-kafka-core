package tasks;

import ckafka.core.streams.PoAStreams;
import ckafka.data.LoadReport;
import data.ConnectionStatistics;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class RankAlgorithmTask implements Runnable{

    /**
     *  Logger
     */
    private final Logger logger = LoggerFactory.getLogger(RankAlgorithmTask.class);

    private PoAStreams poAStreams;
    private CountDownLatch updateLatch;

    public RankAlgorithmTask(PoAStreams poAStreams){
        this.poAStreams = poAStreams;
        updateLatch = new CountDownLatch(2);
    }

    @Override
    public void run() {
        logger.info("Start Rank Algorithm Task");
        try{

            // 1. Get Connections and Load Reports

            ExecutorService executorService = Executors.newFixedThreadPool(4);

            Future<Map<String, ConnectionStatistics>> futureConnectionMap =
                    executorService.submit(new ConnectionReportUpdateTask(updateLatch, poAStreams));
            Future<Map<String, LoadReport>> futureLoadReportMap =
                    executorService.submit(new LoadReportUpdateTask(updateLatch, poAStreams));

            // 2. Wait until the the "get" tasks are finished
            updateLatch.await();

            Map<String, ConnectionStatistics> connectionMap = futureConnectionMap.get();

            Map<String, LoadReport> loadReportMap = futureLoadReportMap.get();

            // 3. Start Reallocation Task (Rank Algorithm)
            this.poAStreams.StartReallocationTask(connectionMap, loadReportMap);

            executorService.shutdown();

        }catch (Exception e){
            logger.error("Error running RankAlgorithmTask", e);
        }
    }
}
