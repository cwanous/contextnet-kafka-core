package tasks;

import ckafka.core.streams.PoAStreams;
import ckafka.data.LoadReport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;

public class LoadReportUpdateTask implements Callable<Map<String, LoadReport>> {

    /**
     *  Logger
     */
    private final Logger logger = LoggerFactory.getLogger(LoadReportUpdateTask.class);

    private PoAStreams poAStreams;

    private CountDownLatch latch;

    public LoadReportUpdateTask(CountDownLatch Latch, PoAStreams PoAStreams){
        this.latch = Latch;
        this.poAStreams = PoAStreams;
    }

    @Override
    public Map<String, LoadReport> call(){
        try {
            logger.info("Start running Load Report Update Task");

            Map<String, LoadReport> loadReportMap = this.poAStreams.GetGatewayLoadRecordMap();

            // Notifying end to RankAlgorithmTask
            latch.countDown();

            return loadReportMap;

        } catch (Exception e) {
            logger.info("Error running Load Report Update Task", e);
            return null;
        }
    }
}
