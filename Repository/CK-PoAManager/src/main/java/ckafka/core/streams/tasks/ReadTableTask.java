package ckafka.core.streams.tasks;

import ckafka.core.streams.PoAStreams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReadTableTask implements Runnable{

    /**
     *  Logger
     */
    final Logger logger = LoggerFactory.getLogger(ReadTableTask.class);

    private PoAStreams poA;

    public ReadTableTask(PoAStreams PoA){
        this.poA = PoA;
    }

    @Override
    public void run() {
        try{

            this.poA.ReadTable();

        }catch (Exception e){
            logger.error("",e);
        }
    }
}
