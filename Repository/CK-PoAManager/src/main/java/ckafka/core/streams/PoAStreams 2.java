package ckafka.core.streams;

import ckafka.core.PoAManagerRankInterface;
import ckafka.core.streams.util.GatewayValidation;
import ckafka.data.ConnectionReport;
import ckafka.data.LoadReport;
import ckafka.data.PingConfig;
import ckafka.data.Swap;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import data.ConnectionStatistics;
import elements.KCoreElement;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.errors.InvalidStateStoreException;
import org.apache.kafka.streams.kstream.KStreamBuilder;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.state.KeyValueIterator;
import org.apache.kafka.streams.state.QueryableStoreType;
import org.apache.kafka.streams.state.QueryableStoreTypes;
import org.apache.kafka.streams.state.ReadOnlyKeyValueStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tasks.PingConfigTask;
import tasks.RankAlgorithmTask;
import tasks.ReallocationTask;

import java.time.Instant;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

public class PoAStreams extends KCoreElement {

    /**
     *  Logger
     */
    private final Logger logger = LoggerFactory.getLogger(PoAStreams.class);

    /**
     *  Kafka Streams Settings
     */
    private KStreamBuilder streamBuilder;
    private KafkaStreams streams;

    /**
     *  Rank Interface
     */
    private PoAManagerRankInterface rankInterface;

    /**
     *  Streams Store
     */
    private String connectionRecordTopic = "ConnectionReport";
    private String storedConnectionRecord = "storedConnectionRecordTopic";
    private String storedConnectionTimeStamps = "storedConnectionTimeStampsTopic";

    private String loadRecordTopic = "LoadReport";
    private String storedLoadRecord = "storedLoadRecordTopic";
    private String storedLoadTimeStamps = "storedLoadTimeStampsTopic";

    /**
     *  Heart beat window
     */
    private GatewayValidation gatewayValidation;
    private Integer maxNumberOfLostLoadReportWindows;

    /**
     * Ping Re-config | Environment Variables
     */
    private Boolean pingReConfigOn;

    private Integer pingFrequency;

    private Integer pingBeginWindow;
    private Integer pingWindow;

    private Map<Float, Float> pingConfigurationMap;

    /**
     * Set Data Handler
     */
    private Swap swap;
    private ObjectMapper objectMapper;

    /**
     * Serializers/deserializers (serde) for Bytes and String
     */
    private final Serde<String> stringSerde = Serdes.String();
    private final Serde<byte[]> bytesSerde = Serdes.ByteArray();

    private Properties properties;


    public PoAStreams(final PoAManagerRankInterface RankInterface){
        super();

        try{
            GetStartPingValues();

            GetGatewayValidationValues();

            this.rankInterface = RankInterface;
            SetPingConfigurationMap();

            // Data Swap
            this.objectMapper = new ObjectMapper();
            this.swap = new Swap(objectMapper);

            // Scheduled Tasks
            RankAlgorithmTask rankTask = new RankAlgorithmTask(this);
            this.threadPool.scheduleWithFixedDelay(rankTask, 10000, 30000, TimeUnit.MILLISECONDS); // Or while true !

            GetProperties();

            // Gateway Validation
            this.gatewayValidation = new GatewayValidation(this.maxNumberOfLostLoadReportWindows, 60000);

            // Streams
            this.streamBuilder = new KStreamBuilder();

            BuildConnectionStreamsBuilder();

            BuildLoadStreamsBuilder();

            this.streams = new KafkaStreams(this.streamBuilder, properties);

            // Set Error Handling
            // here you should examine the throwable/exception and perform an appropriate action!
            streams.setUncaughtExceptionHandler(this::UncaughtExceptionHandler);

            streams.cleanUp();
            streams.start();

            // shutdown hook to correctly close the streams application
            Runtime.getRuntime().addShutdownHook(new Thread(streams::close));

        }catch (Exception e){
            logger.error("Error Initiating PoAStreams", e);
            if (streams != null) {
                streams.close();
            }
            System.exit(1);
        }
    }


    private void GetGatewayValidationValues() {
        logger.info("Get Gateway Validation Values");
        try{

            this.maxNumberOfLostLoadReportWindows = 1;

            String maxNumberOfLostLoadReportWindowsValue =
                    this.GetEnvironmentVariable("maxNumberOfLostLoadReportWindows");
            if(maxNumberOfLostLoadReportWindowsValue != null){
                this.maxNumberOfLostLoadReportWindows = new Integer(maxNumberOfLostLoadReportWindowsValue);
            }else {
                logger.info("Max Number Of Lost Load Report Windows not informed. Default used.");
            }

        }catch (Exception e){
            logger.error("Error Getting Gateway Validation Values", e);
        }
    }


    private void GetStartPingValues(){
        logger.info("Get Start Ping Values");
        try{

            SetDefaultPingValues();

            SetPingReConfigOn();

            SetPingFrequencyValue();

            SetPingWindowValue();

            this.pingWindow = this.pingBeginWindow;

        }catch (Exception e){
            logger.error("Error getting Start Ping Values", e);
        }
    }


    private void SetPingReConfigOn(){
        String pingReConfigOnValue = this.GetEnvironmentVariable("pingReConfigOn");
        if(pingReConfigOnValue != null){
            this.pingReConfigOn = Boolean.valueOf(pingReConfigOnValue);
        }else {
            logger.info("PingReConfigOn value not informed. Default used.");
        }
    }


    private void SetPingFrequencyValue(){
        String pingFrequencyValue = this.GetEnvironmentVariable("pingFrequency");
        if(pingFrequencyValue != null){
            this.pingFrequency = new Integer(pingFrequencyValue);
        }else {
            logger.info("Ping Frequency not informed. Default used.");
        }
    }


    private void SetPingWindowValue(){
        String pingWindowValue = this.GetEnvironmentVariable("pingBeginWindow");
        if(pingWindowValue != null){
            this.pingBeginWindow = new Integer(pingWindowValue);
        }else {
            logger.info("Ping Begin Window not informed. Default used.");
        }
    }


    private void SetDefaultPingValues() {
        this.pingReConfigOn = true;
        this.pingFrequency = 60000;
        this.pingBeginWindow = 30000;
    }

    @Override
    public void RecordReceived(ConsumerRecord Record) {
        logger.info("Record Received " + Record.value().toString());

        try {
            if(Record.topic().equals("HelloPoA") ){
                ProcessHelloPoARecord(Record);
            }
        }catch (Exception e){
            logger.error("Error receiving Record", e);
        }
    }


    private void ProcessHelloPoARecord(ConsumerRecord HelloRecord){
        logger.info("Processing HelloPoA Record");
        try {
            PingConfig pingConfig = new PingConfig(this.pingFrequency, this.pingWindow);

            SendPingConfig((String) HelloRecord.key(), pingConfig);

        }catch (Exception e){
            logger.error("Error Processing HelloPoA Record", e);
        }
    }


    private void SendPingConfig(String GatewayKey, PingConfig PingConfig){
        logger.info("Sending Ping Config");
        try {

            SendRecord(CreateRecord("PingConfig",
                    GatewayKey,
                    swap.DataSerialization(PingConfig)));

        }catch (Exception e){
            logger.error("Error sending Ping Config", e);
        }
    }

    @Override
    public List<String> GetInternalListOfStrings(){
        List<String> internalList =  new ArrayList<>();
        internalList.add("HelloPoA");
        return internalList;
    }


    private void BuildConnectionStreamsBuilder(){
        logger.info("Building Connection Streams");
        try{

            KTable<String, byte[]> tableBuilder = this.streamBuilder.table(
                    stringSerde,
                    bytesSerde,
                    this.connectionRecordTopic,
                    this.storedConnectionRecord);

            tableBuilder.mapValues(value -> Instant.now().toString(),
                    stringSerde,
                    this.storedConnectionTimeStamps);

        }catch (Exception e){
            logger.error("Error Building Connection Streams", e);
            throw  e;
        }
    }


    private void BuildLoadStreamsBuilder(){
        logger.info("Building Load Streams");
        try{

            KTable<String, byte[]> tableBuilder = this.streamBuilder.table(
                    stringSerde,
                    bytesSerde,
                    this.loadRecordTopic,
                    this.storedLoadRecord);

            tableBuilder.mapValues(value -> Instant.now().toString(),
                    stringSerde,
                    storedLoadTimeStamps);

        }catch (Exception e){
            logger.error("Error Building Load Streams", e);
            throw  e;
        }
    }


    private void UncaughtExceptionHandler(Thread thread, Throwable throwable) {
        logger.error("Error ");
        streams.close();
    }


    private void GetProperties(){
        try{
            this.properties = new Properties();

            properties.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
            properties.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());

            properties.put(StreamsConfig.APPLICATION_ID_CONFIG, "testPoAS");
            properties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092");
            properties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

            // we disable the cache to demonstrate all the "steps" involved in the transformation - not recommended in prod
            properties.put(StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG, "0"); //10 * 1024 * 1024L

            // Exactly once processing
            properties.put(StreamsConfig.PROCESSING_GUARANTEE_CONFIG, StreamsConfig.EXACTLY_ONCE);

        }catch (Exception e){
            logger.error("Error getting Properties", e);
        }
    }


    public void ReadTable(){
        try{

            System.out.println("READ");

            ReadOnlyKeyValueStore viewRecord = streams.store("", QueryableStoreTypes.keyValueStore());

            ReadOnlyKeyValueStore viewTime = streams.store("", QueryableStoreTypes.keyValueStore());

            /*KeyValueIterator<String, String> rangeRecord = viewRecord.all();
            while (rangeRecord.hasNext()) {
                KeyValue<String, String> next = rangeRecord.next();
                System.out.println("Elements " + next.key + ": " + next.value.toString());
            }

            KeyValueIterator<String, String> rangeTime = viewTime.all();
            while (rangeTime.hasNext()) {
                KeyValue<String, String> next = rangeTime.next();
                System.out.println("count " + next.key + ": " + next.value);
            }*/

            System.out.println("READ END");

        }catch (Exception e){
            logger.error("", e);
        }
    }


    public static <T> T waitUntilStoreIsQueryable(final String storeName,
                                                  final QueryableStoreType<T> queryableStoreType,
                                                  final KafkaStreams streams) throws InterruptedException {
        while (true) {
            try {
                return streams.store(storeName, queryableStoreType);
            } catch (InvalidStateStoreException ignored) {
                // store not yet ready for querying
                Thread.sleep(100);
            }
        }
    }

    public Map<String, ConnectionStatistics> GetGatewayConnectionMap(){
        logger.info("Getting Gateway Connection Map");

        try {

            Map<String, ConnectionStatistics> gatewayConnectionMap
                    = new ConcurrentHashMap<>();

            ReadOnlyKeyValueStore<String, byte[]> viewRecord = streams.store(this.storedConnectionRecord,
                    QueryableStoreTypes.keyValueStore());

            KeyValueIterator<String, byte[]> records = viewRecord.all();

            while (records.hasNext()) {
                KeyValue<String, byte[]> next = records.next();

                ConnectionStatistics statistics = GetConnectionStatisticsFromRecord(next);

                if (statistics != null){
                    gatewayConnectionMap.put(next.key, statistics);
                }
            }

            if(IsPingReConfigOn()){
                PingConfigTask pingTask = new PingConfigTask(this, gatewayConnectionMap);
                this.threadPool.execute(pingTask);
            }

            return gatewayConnectionMap;

        }catch (Exception e){
            logger.error("Error Getting Gateway Connection Map", e);
            return null;
        }
    }


    private ConnectionStatistics GetConnectionStatisticsFromRecord(KeyValue<String, byte[]> next){
        logger.info("Getting Connection Statistics from GW ".concat(next.key));
        try{
            ConnectionReport connectionReport =
                    (ConnectionReport) this.swap.DataDeserialization(next.value, ConnectionReport.class);

            return new ConnectionStatistics(connectionReport, next.key);

        }catch (Exception e){
            logger.error("Error getting Connection Statistics from GW ".concat(next.key), e);
            return null;
        }
    }


    public Map<String, LoadReport> GetGatewayLoadRecordMap(){
        logger.info("Updating Gateway Load Record Map");

        try {

            List<String> validGateways = this.GetValidGateways();

            Map<String, LoadReport> gatewayLoadMap
                    = new ConcurrentHashMap<>();

            ReadOnlyKeyValueStore<String, byte[]> viewRecord = streams.store(this.storedLoadRecord,
                    QueryableStoreTypes.keyValueStore());

            KeyValueIterator<String, byte[]> records = viewRecord.all();

            while (records.hasNext()) {
                KeyValue<String, byte[]> next = records.next();

                LoadReport loadReport = GetLoadReportFromRecord(next);

                if (loadReport != null & validGateways.contains(next.key)){
                    gatewayLoadMap.put(next.key, loadReport);
                }
            }

            return gatewayLoadMap;

        }catch (Exception e){
            logger.error("Error Updating Gateway Connection Map", e);
            return null;
        }
    }


    private List<String> GetValidGateways(){
        logger.info("Getting Invalid Gateways");
        List<String> validGateways = new ArrayList<>();
        try {

            Instant now = Instant.now();

            ReadOnlyKeyValueStore<String, String> viewRecord = streams.store(this.storedLoadTimeStamps,
                    QueryableStoreTypes.keyValueStore());

            KeyValueIterator<String, String> records = viewRecord.all();

            while (records.hasNext()) {
                KeyValue<String, String> next = records.next();
                if(this.gatewayValidation.IsGatewayValid(Instant.parse(next.value), now)){
                    validGateways.add(next.key);
                }
            }

            return validGateways;

        }catch (Exception e){
            logger.error("Error Getting Invalid Gateways", e);
            return validGateways;
        }
    }


    private LoadReport GetLoadReportFromRecord(KeyValue<String,byte[]> next) {
        logger.info("Getting Load Report from GW ".concat(next.key));
        try{

            return (LoadReport) swap.DataDeserialization(next.value, LoadReport.class);

        }catch (Exception e){
            logger.error("Error getting Load Report from GW ".concat(next.key), e);
            return null;
        }
    }


    private Map<String, Integer> RankGateways(Map<String, ConnectionStatistics> connectionMap,
                                             Map<String, LoadReport> loadReportMap) {
        logger.info("Starting RankGateways from Developer Interface");
        try{
            return this.rankInterface.RankGateways(connectionMap, loadReportMap);
        }catch (Exception e){
         logger.error("Error on RankGateways from Developer Interface", e);
            return null;
        }
    }


    public void StartReallocationTask(Map<String, ConnectionStatistics> connectionMap,
                                      Map<String, LoadReport> loadReportMap) {
        logger.info("Starting Reallocation Task");
        try{
            Map<String, Integer> gatewayRank = RankGateways(connectionMap, loadReportMap);

            if(gatewayRank == null){
                logger.error("RankGateways from Developer Interface return null");
            }else{
                ReallocationTask reallocationTask = new ReallocationTask(this, gatewayRank, connectionMap);
                this.threadPool.execute(reallocationTask);
            }
        }catch (Exception e){
            logger.info("Error on Reallocation", e);
        }
    }


    public void Reallocation(Map<String, Integer> gatewayRank,
                             Map<String, ConnectionStatistics> connectionMap) {
        logger.info("Reallocation");
        try {

            Map<String, Integer> rank = ReverseOrderMap(gatewayRank);

            Integer sumValues = GetRankMapSumValues(gatewayRank);

            Integer totalNodes = GetConnectionMapSumValues(connectionMap);

            // GW ID - Ideal Nº MN - Set Nº MN
            Table<String, Integer, Set<String>> gatewaysNodeStatus = GetGatewaysNodeStatus(rank,
                    connectionMap, sumValues, totalNodes);




        }catch (Exception e){
            logger.error("Error on Reallocation", e);
        }
    }

    // From String > GW ID + IP + Port
    private List<String> GetReallocationMap(){
        logger.info("Running GetReallocationMap");
        try{
            List<String> listOfReallocationRecords = new ArrayList<>();


            return listOfReallocationRecords;
        }catch (Exception e){
            logger.error("Error on GetReallocationMap");
            throw e;
        }
    }


    private Table<String, Integer, Set<String>> GetGatewaysNodeStatus(Map<String, Integer> Rank,
                                                                  Map<String, ConnectionStatistics> connectionMap,
                                                                  Integer SumValues,
                                                                  Integer TotalNodes){
        logger.info("Running GetGatewaysNodeStatus");
        try{
            Table<String, Integer, Set<String>> gatewaysNodeStatus = HashBasedTable.create();

            for (Map.Entry<String, Integer> entry: Rank.entrySet()){
                Integer idealNodes = Math.round((entry.getValue()*TotalNodes)/SumValues);
                Set<String> nodes = connectionMap.get(entry.getKey()).getNodesSet();
                gatewaysNodeStatus.put(entry.getKey(), idealNodes, nodes);
            }

            return gatewaysNodeStatus;

        }catch (Exception e){
            logger.error("");
            throw e;
        }
    }


    private Map<String, Integer> ReverseOrderMap(Map<String, Integer> gatewayRank){
        logger.info("Running ReverseOrderMap");
        try {
            Map<String, Integer> rank = new TreeMap<>(Collections.reverseOrder());

            rank.putAll(gatewayRank);

            return rank;
        }catch (Exception e){
            logger.error("Error ReverseOrderMap");
            throw e;
        }
    }


    private Integer GetRankMapSumValues(Map<String, Integer> gatewayRank){
        logger.info("Running GetRankMapSumValues");
        try{
            Collection<Integer> values = gatewayRank.values();

            Iterator<Integer> iterator = values.iterator();

            int sumValues = 0;

            while(iterator.hasNext()){
                Integer value = iterator.next();
                sumValues += value;
            }

            return sumValues;
        }catch (Exception e){
            logger.error("Error on GetRankMapSumValues");
            throw e;
        }
    }


    private Integer GetConnectionMapSumValues(Map<String, ConnectionStatistics> connectionMap){
        logger.info("Running GetConnectionMapSumValues");
        try {
            Collection<ConnectionStatistics> connections = connectionMap.values();

            Iterator<ConnectionStatistics> iterator = connections.iterator();

            int totalNodes = 0;

            while(iterator.hasNext()){
                ConnectionStatistics connection = iterator.next();
                totalNodes += connection.getNodesSet().size();
            }

            return totalNodes;
        }catch (Exception e){
            logger.error("Error GetConnectionMapSumValues");
            throw e;
        }

    }


    private void SetPingConfigurationMap(){
        logger.info("Setting ping configuration map");
        try {
            Map<Float, Float> interfaceMap = this.rankInterface.PingConfigurationMap();

            this.pingConfigurationMap = new TreeMap<>(Collections.reverseOrder());

            this.pingConfigurationMap.putAll(interfaceMap);

        }catch (Exception e){
            logger.error("Error setting ping configuration map", e);
            this.pingConfigurationMap = null;
        }
    }


    private Float GetPingWindowFactor(Float pingRate){
        logger.info("Getting Ping Window Factor");
        try{
            Set<Map.Entry<Float, Float>> set = this.pingConfigurationMap.entrySet();

            for (Map.Entry<Float, Float> rate : set) {
                if(rate.getKey() <= pingRate){
                    return rate.getValue();
                }
            }
            return 1f;

        }catch (Exception e){
            logger.error("Error getting Ping Window Factor", e);
            return null;
        }
    }


    private boolean IsPingReConfigOn(){
        return this.pingReConfigOn;
    }


    public void PingReconfiguration(Map<String, ConnectionStatistics> gatewayConnectionMap) {
        try {
            Float pingRate = GetPingRate(gatewayConnectionMap);
            if(pingRate != null){
                Float windowFactor = GetPingWindowFactor(pingRate);
                if(windowFactor != null){
                    if(windowFactor != 1f){
                        PingConfig pingConfig = new PingConfig(this.pingFrequency,
                                (int) (this.pingBeginWindow*windowFactor));
                        
                        SendMultiplePingConfigs(gatewayConnectionMap.keySet(), pingConfig);
                        
                    }else {
                        logger.info("Ping Reconfiguration will not run. windowFactor is 1.");
                    }
                }else {
                    logger.error("Could not run Ping Reconfiguration. windowFactor null.");
                }
            }else {
                logger.error("Could not run Ping Reconfiguration. pingRate null.");
            }

        }catch (Exception e){
            logger.error("Could not run Ping Reconfiguration.", e);
        }
    }


    private void SendMultiplePingConfigs(Set<String> keySet, PingConfig pingConfig) {
        logger.info("Sending multiple Ping Configs");
        try {

            List<ProducerRecord> producerRecordList = CreatePingConfigRecords(keySet, pingConfig);

            if(producerRecordList != null){
                SendMultipleRecords(producerRecordList);
            }else {
                logger.error("Error sending multiple Ping Configs. SendMultipleRecords returned null.");
            }

        }catch (Exception e){
            logger.error("Error sending multiple Ping Configs", e);
        }
    }


    private List<ProducerRecord> CreatePingConfigRecords(Set<String> keySet, PingConfig PingConfig){
        logger.info("Getting Ping Config Records");
        List<ProducerRecord> producerRecordList = new ArrayList<>();
        try{
            for(String gatewayKey: keySet){
                ProducerRecord pingRecord = CreateRecord("PingConfig",
                        gatewayKey,
                        swap.DataSerialization(PingConfig));

                producerRecordList.add(pingRecord);
            }

            return producerRecordList;

        }catch (Exception e){
            logger.error("Error CreatePingConfigRecords", e);
            return null;
        }
    }


    private Float GetPingRate(Map<String, ConnectionStatistics> gatewayConnectionMap){
        logger.info("Getting PingRate");
        try{
            double pingRateMedian = 0f;
            int totalNodes = 0;
            for(ConnectionStatistics statistics : gatewayConnectionMap.values()){
                pingRateMedian = pingRateMedian + statistics.getPingRate()*statistics.getNodesSet().size();
                totalNodes = totalNodes + statistics.getNodesSet().size();
            }
            return (float) (pingRateMedian / totalNodes);
        }catch (Exception e){
            logger.error("Error getting PingRate", e);
            return null;
        }
    }



}


