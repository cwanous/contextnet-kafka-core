package ckafka.core.streams.util;

import java.time.Duration;
import java.time.Instant;

public class GatewayValidation {

    /**
     *  Heart beat window
     */
    private Integer maxNumberOfLostLoadReportWindows; // environment variable
    private Integer loadReportWindowSize; // milliseconds (must be the same as GW)

    public GatewayValidation(Integer MaxNumberOfLostLoadReportWindows,
                                  Integer LoadReportWindowSize){
        this.maxNumberOfLostLoadReportWindows = MaxNumberOfLostLoadReportWindows;
        this.loadReportWindowSize = LoadReportWindowSize;
    }

    public boolean IsGatewayValid(Instant RecordInstant, Instant Now){
        try{
            Long interval = Duration.between(RecordInstant, Now).toMillis();
            Integer lostWindows = Math.toIntExact((interval / this.loadReportWindowSize));
            return lostWindows <= this.maxNumberOfLostLoadReportWindows;
        }catch (Exception e){
            return true;
        }

    }
}
