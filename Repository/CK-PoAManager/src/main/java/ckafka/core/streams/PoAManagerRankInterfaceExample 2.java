package ckafka.core.streams;

import ckafka.core.PoAManagerRankInterface;
import ckafka.data.LoadReport;
import data.ConnectionStatistics;

import java.util.HashMap;
import java.util.Map;

public class PoAManagerRankInterfaceExample implements PoAManagerRankInterface {

    @Override
    public Map<String, Integer> RankGateways(Map<String, ConnectionStatistics> connectionMap,
                                             Map<String, LoadReport> loadMap){
        try {
            Map<String, Integer> rank = new HashMap<>();

            int value = 1;
            for(String gatewayId : connectionMap.keySet()){
                rank.put(gatewayId, value);
                value += 1;
            }

            return rank;
        }catch (Exception e){
            System.out.println(e.getMessage());
            return null;
        }
    }

    @Override
    public Map<Float, Float> PingConfigurationMap() {
        Map<Float, Float> pingConfigurationMap = new HashMap<>();
        pingConfigurationMap.put(0f, 3f);
        pingConfigurationMap.put(0.1f, 2.5f);
        pingConfigurationMap.put(0.3f, 2f);
        pingConfigurationMap.put(0.5f, 1f);
        pingConfigurationMap.put(0.7f, 0.75f);
        pingConfigurationMap.put(0.9f, 0.5f);
        return pingConfigurationMap;
    }
}