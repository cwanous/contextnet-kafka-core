package ckafka.core;

import ckafka.data.LoadReport;
import data.ConnectionStatistics;

import java.util.Map;

public interface PoAManagerRankInterface {

    public Map<String, Integer> RankGateways(Map<String, ConnectionStatistics> connectionMap,
                                             Map<String, LoadReport> loadMap);

    public Map<Float, Float> PingConfigurationMap();
}
