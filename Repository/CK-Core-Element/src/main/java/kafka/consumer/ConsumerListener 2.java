package kafka.consumer;


import org.apache.kafka.clients.consumer.ConsumerRecord;

public interface ConsumerListener {

    void RecordReceived(ConsumerRecord Record);
}
