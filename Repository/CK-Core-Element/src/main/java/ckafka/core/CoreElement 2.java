package ckafka.core;

import ckafka.connection.CKConsumer;
import ckafka.connection.CKProducer;
import ckafka.data.Swap;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.common.serialization.ByteArrayDeserializer;
import org.apache.kafka.common.serialization.ByteArraySerializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

public class CoreElement {

    /**
     *  Logger
     */
    final Logger logger = LoggerFactory.getLogger(CoreElement.class);

    protected CKProducer ckProducer;

    protected CKConsumer ckConsumer;

    /**
     * Swap Data Handler
     */
    protected Swap swap;
    private ObjectMapper objectMapper;
    private String serializeKey;
    private String deserializeKey;
    private String serializeValue;
    private String deserializeValue;

    protected UUID applicationID;

    public CoreElement(){

        applicationID = UUID.randomUUID();

        // Serializer and Deserializer
        serializeKey = StringSerializer.class.getName();
        deserializeKey = StringDeserializer.class.getName();
        serializeValue = ByteArraySerializer.class.getName();
        deserializeValue = ByteArrayDeserializer.class.getName();

        try{
            logger.info("Starting Producer");

            ckProducer = new CKProducer(serializeKey, serializeValue);

        }catch (Exception e){
            logger.error("Error starting producer", e);
            System.exit(1);
        }

        try {
            logger.info("Starting Consumer");

            List<String> topics = GetListOfTopicsFromFile();

            ckConsumer = new CKConsumer(topics, deserializeKey, deserializeValue);
            ckConsumer.start();

        }catch (Exception e){
            logger.error("Error starting consumer", e);
            System.exit(1);
        }

        // Data Swap
        this.objectMapper = new ObjectMapper();
        this.swap = new Swap(objectMapper);

        logger.info("Application Started");
    }

    /**
     * Get Topics from file properties.xml file
     *
     * @pre
     * @post
     */
    protected List<String> GetListOfTopicsFromFile() throws Exception {
        try {

            logger.info("Get Topics to Consumer from file");

            String rootPath = Thread.currentThread().getContextClassLoader().getResource("").getPath();
            String mnConfigPath = rootPath + "properties.xml";
            Properties mnProps = new Properties();
            mnProps.loadFromXML(new FileInputStream(mnConfigPath));

            List<String> listOfTopics = Arrays.asList(mnProps.getProperty("topics").split(","));

            return listOfTopics;

        } catch (Exception e){

            logger.error("Error loading properties file", e);
            throw e;
        }
    }
}
