package ckafka.connection.tasks;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class SendTopicTask extends Thread{

    /**
     *  Logger
     */
    final Logger logger = LoggerFactory.getLogger(SendTopicTask.class);

    KafkaProducer producer;
    ProducerRecord record;

    public SendTopicTask(KafkaProducer Producer, ProducerRecord Record){
        this.producer = Producer;
        this.record = Record;
    }

    // Send Topic
    @Override
    public void run() {

        try {
            producer.send(record);

            logger.info("Topic Sent");

        } catch (Exception e) {
           throw e;
        }
    }

}
