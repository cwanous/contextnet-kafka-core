package ckafka.connection;

import ckafka.connection.tasks.SendMultipleTopicsTask;
import ckafka.connection.tasks.SendTopicTask;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * CKProducer a kafka-record producer
 *
 * @author camilawanous
 */

public class CKProducer {

    /**
     *  Logger
     */
    final Logger logger = LoggerFactory.getLogger(CKProducer.class);

    private Properties fileProperties;
    private Properties producerProperties;
    private Map<String,String> environmentVariables;

    private KafkaProducer producer;

    /**
     * Producers Keys
     *
     */
    private String bootstrap_servers_config;
    private String key_serializer_class_config;
    private String value_serializer_class_config;
    private String acks_config;
    private String retries_config;
    private String linger_ms_config;
    private String enable_idempotence_config;


    public CKProducer(String KeySerializer, String ValueSerializer){

        bootstrap_servers_config = "";
        key_serializer_class_config = "";
        value_serializer_class_config = "";
        acks_config = "";
        retries_config = "";
        linger_ms_config = "";
        enable_idempotence_config = "";

        try{

            FillEnvironmentVariables();

            SetProducerProperties(KeySerializer, ValueSerializer);

            producer = new KafkaProducer<>(producerProperties);

        }catch (Exception e){
            // throw error to notify the user that the producer is not working
            logger.error("Could not initiate Kafka Producer");
            throw e;
        }

    }

    public KafkaProducer getProducer() {
        return producer;
    }

    /**
     * Set Producer Properties
     * If the properties.xml file could not be load set default producer properties
     *
     * @pre must receive key and value serialize
     * @post Set Producer Properties
     */
    private void SetProducerProperties(String KeySerializer, String ValueSerializer){
        try{
            logger.info("Getting Producer Properties");

            fileProperties = GetProducerKeysFromFile();
            SetProducerKeysFromFile(KeySerializer, ValueSerializer);

            producerProperties = new Properties();
            FillProducerProperties();

        }catch (Exception e){
            logger.error("Error on Producer Properties", e);
            producerProperties = null;
        }

    }

    /**
     * Get Properties from file
     * If the properties.xml file could not be load set default producer properties
     *
     * @pre must exists a properties.xml file
     * @post Properties from properties.xml file
     */
    private Properties GetProducerKeysFromFile(){
        try {

            logger.info("Get Properties to Producer from file");

            String rootPath = Thread.currentThread().getContextClassLoader().getResource("").getPath();
            String mnConfigPath = rootPath + "properties.xml";
            Properties mnProps = new Properties();
            mnProps.loadFromXML(new FileInputStream(mnConfigPath));

            return mnProps;

        } catch (Exception e){

            logger.error("Error loading properties file", e);
            return null;
        }
    }

    /**
     * Set values for Producer Keys from File
     * If the properties.xml file could not be load set default values
     *
     * @pre
     * @post
     */
    private void SetProducerKeysFromFile(String KeySerializer, String ValueSerializer){

        key_serializer_class_config = KeySerializer;
        value_serializer_class_config = ValueSerializer;

        SetDefaultProducerKeys();

        try {
            if(fileProperties != null){

                if(fileProperties.getProperty("BOOTSTRAP_SERVERS_CONFIG") != null){
                    bootstrap_servers_config = fileProperties.getProperty("BOOTSTRAP_SERVERS_CONFIG");
                }

                if(fileProperties.getProperty("ACKS_CONFIG") != null){
                    acks_config = fileProperties.getProperty("ACKS_CONFIG");
                }

                if(fileProperties.getProperty("RETRIES_CONFIG") != null){
                    retries_config = fileProperties.getProperty("RETRIES_CONFIG");
                }

                if(fileProperties.getProperty("LINGER_MS_CONFIG") != null){
                    linger_ms_config = fileProperties.getProperty("LINGER_MS_CONFIG");
                }

                if(fileProperties.getProperty("ENABLE_IDEMPOTENCE_CONFIG") != null){
                    enable_idempotence_config = fileProperties.getProperty("ENABLE_IDEMPOTENCE_CONFIG");
                }
            }
        }catch (Exception e){
            logger.error("Error setting Properties - Default Properties loaded", e);
            SetDefaultProducerKeys();
        }

    }

    /**
     * Set default values for Producer Keys
     *
     * @pre
     * @post
     */
    private void SetDefaultProducerKeys(){
        bootstrap_servers_config = "localhost:9092";
        acks_config = "all";
        retries_config = "3";
        linger_ms_config = "1";
        enable_idempotence_config = "true";
    }

    /**
     * Fill Producer Properties
     *
     * @pre
     * @post
     */
    private void FillProducerProperties(){

        producerProperties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrap_servers_config);
        producerProperties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, key_serializer_class_config);
        producerProperties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, value_serializer_class_config);
        producerProperties.setProperty(ProducerConfig.ACKS_CONFIG, acks_config);
        producerProperties.setProperty(ProducerConfig.RETRIES_CONFIG, retries_config);
        producerProperties.setProperty(ProducerConfig.LINGER_MS_CONFIG, linger_ms_config);
        producerProperties.setProperty(ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG, enable_idempotence_config);
    }


    public SendTopicTask SendTopic(ProducerRecord Record){
        try {
            logger.info("Send Topic Task");

            return new SendTopicTask(producer, Record);
        }catch (Exception e){
            logger.error("Error sending topic: " + Record.toString(), e);
            return null;
        }
    }


    public SendMultipleTopicsTask SendMultipleTopics(List<ProducerRecord> Records){
        try {
            logger.info("Sending Multiple Topics");

            return new SendMultipleTopicsTask(producer, Records);
        }catch (Exception e){
            logger.error("Error sending multiple topics: " + Records.toString(), e);
            return null;
        }
    }


    private void FillEnvironmentVariables(){
        try{
            environmentVariables = System.getenv();
        }catch (Exception e){
            environmentVariables = null;
            logger.error("Error filling Environment Variables", e);
        }
    }


    private String GetEnvironmentVariable(String keyVariable){
        try{
            return environmentVariables.get(keyVariable);
        }catch (Exception e){
            logger.error("Error getting Environment Variable " + keyVariable, e);
            return "";
        }
    }

}
