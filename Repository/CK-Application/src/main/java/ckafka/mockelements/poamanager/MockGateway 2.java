package ckafka.mockelements.poamanager;

import ckafka.core.CoreElement;
import ckafka.core.data.LoadReportExtractor;
import ckafka.data.ConnectionReport;
import ckafka.data.LoadReport;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ScheduledThreadPoolExecutor;

public class MockGateway extends CoreElement implements Runnable{

    /**
     *  Logger
     */
    private final Logger logger = LoggerFactory.getLogger(MockGateway.class);

    private Set<String> setOfNodes;

    private Random rand;

    private ScheduledThreadPoolExecutor threadPool;

    public MockGateway(Set<String> setOfNodes) {
        super();
        this.setOfNodes = setOfNodes;
        this.rand = new Random();
        this.threadPool = new ScheduledThreadPoolExecutor(1000);

        logger.info("App ID: " + this.applicationID.toString());
    }

    // Connection Report

    private ConnectionReport GetConnectionReport(){
        try{
            Instant initialInstant = Instant.now();

            List<Instant> pingsSoFar = new ArrayList<>();

            int numberOfPings = this.rand.nextInt(this.setOfNodes.size());

            for(int i=1; i <= numberOfPings; i++){
                pingsSoFar.add(initialInstant.plusSeconds(rand.nextInt(120)));
            }

            return new ConnectionReport(initialInstant, pingsSoFar, this.setOfNodes);

        }catch (Exception e){
            logger.error("Error on GetConnectionReport", e);
            return null;
        }
    }

    private ProducerRecord GetConnectionReportRecord(ConnectionReport Data){
        try {

            byte[] dataSerialize = this.swap.DataSerialization(Data);
            ConnectionReport dataDes =
                    (ConnectionReport) this.swap.DataDeserialization(dataSerialize, ConnectionReport.class);

            ProducerRecord<String, byte[]> record
                    = new ProducerRecord<>("ConnectionReport",
                    this.applicationID.toString(),
                    this.swap.DataSerialization(Data));

            return record;

        }catch (Exception e){
            logger.error("Error converting Shippable to ConnectionReport Producer-Record", e);
            return null;
        }
    }

    // Load Report

    private LoadReport GetLoadReport() {
        logger.info("Getting Load Report");
        try{

            LoadReportExtractor extractor = new LoadReportExtractor(this.applicationID,
                    // Returns the core number of threads.
                    threadPool.getCorePoolSize(),
                    // Returns the approximate number of threads that are actively executing tasks
                    threadPool.getActiveCount(),
                    // Returns Thread Pool Queue Size
                    threadPool.getQueue().size()
            );

            LoadReport loadReport = new LoadReport(
                    extractor.getActivePoolUtilization(),
                    extractor.getCpuLoad(),
                    extractor.getGatewayID(),
                    extractor.getHardwareTotalMemory(),
                    extractor.getJVMFTotalMemory(),
                    extractor.getPoolQueueDimension(),
                    extractor.getPercentageOfAvailableHardwareMemory(),
                    extractor.getPercentageOfAvailableJVMMemory()
            );

            return loadReport;

        }catch (Exception e){
            logger.error("Error getting Load Report", e);
            return null;
        }
    }

    private ProducerRecord GetLoadReportRecord(LoadReport Data){
        try {
            ProducerRecord<String, byte[]> record
                    = new ProducerRecord<>("LoadReport", this.applicationID.toString(), swap.DataSerialization(Data));
            return record;
        }catch (Exception e){
            logger.error("Error converting Shippable to Load Report Producer-Record", e);
            return null;
        }
    }

    @Override
    public void run() {
        while(true){
            try {
                Thread.sleep(6000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            // Connection Report
            ConnectionReport connectionReport = GetConnectionReport();
            ProducerRecord connectionRecord = GetConnectionReportRecord(connectionReport);
            this.threadPool.execute(this.ckProducer.SendTopic(connectionRecord));

            // Load Report
            LoadReport loadReport = GetLoadReport();
            ProducerRecord loadRecord = GetLoadReportRecord(loadReport);
            this.threadPool.execute(this.ckProducer.SendTopic(loadRecord));
        }
    }
}
