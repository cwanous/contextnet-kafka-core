package ckafka.mockelements.poamanager;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MockGatewayInitiator {

    public static void main(String[] args) {

        ExecutorService executor = Executors.newFixedThreadPool(100);

        Set<String> setOfNodes1 = new HashSet<>();
        for(int i=1; i <= 10; i++){
            setOfNodes1.add(UUID.randomUUID().toString());
        }

        Set<String> setOfNodes2 = new HashSet<>();
        for(int i=1; i <= 10; i++){
            setOfNodes2.add(UUID.randomUUID().toString());
        }

        MockGateway mock1 = new MockGateway(setOfNodes1);
        MockGateway mock2 = new MockGateway(setOfNodes2);

        executor.execute(mock1);
        executor.execute(mock2);
    }
}
