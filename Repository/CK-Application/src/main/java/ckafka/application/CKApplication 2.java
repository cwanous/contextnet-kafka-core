package ckafka.application;

import ckafka.connection.CKConsumer;
import ckafka.connection.CKProducer;
import ckafka.connection.ConsumerListener;
import ckafka.data.Swap;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.ByteArrayDeserializer;
import org.apache.kafka.common.serialization.ByteArraySerializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.ScheduledThreadPoolExecutor;

public class CKApplication implements ConsumerListener {


    /**
     *  Logger
     */
    private final Logger logger = LoggerFactory.getLogger(CKApplication.class);

    protected CKProducer ckProducer;

    protected CKConsumer ckConsumer;

    /**
     *  ThreadPoll
     */
    protected final ScheduledThreadPoolExecutor threadPool;

    protected UUID applicationID;

    /**
     * Swap Data Handler
     */
    protected Swap swap;
    protected ObjectMapper objectMapper;
    protected String serializeKey;
    protected String deserializeKey;
    protected String serializeValue;
    protected String deserializeValue;

    public static void main(String[] args) {
        new CKApplication();
    }

    public CKApplication(){

        logger.info("Starting Application");

        applicationID = UUID.randomUUID();

        // Serializer and Deserializer
        serializeKey = StringSerializer.class.getName();
        deserializeKey = StringDeserializer.class.getName();
        serializeValue = ByteArraySerializer.class.getName();
        deserializeValue = ByteArrayDeserializer.class.getName();

        threadPool = new ScheduledThreadPoolExecutor(1000);

        try{
            logger.info("Starting Producer");

            ckProducer = new CKProducer(serializeKey, serializeValue);

        }catch (Exception e){
            logger.error("Error starting producer", e);
            System.exit(1);
        }

        /*try {
            logger.info("Starting Consumer");

            List<String> topics = GetListOfTopicsFromFile();

            ckConsumer = new CKConsumer(topics, deserializeKey, deserializeValue);
            ckConsumer.addConsumerListener(this);
            ckConsumer.start();

        }catch (Exception e){
            logger.error("Error starting consumer", e);
            System.exit(1);
        }*/

        // Data Swap
        this.objectMapper = new ObjectMapper();
        this.swap = new Swap(objectMapper);

        logger.info("Application Started");

        ProduceRecordTest();
    }

    private void ProduceRecordTest() {
        logger.info("Testing Produce Record");

        try{

            byte[] value = null;

            ProducerRecord<String, byte[]> newRecord = new ProducerRecord<>
                    ("temperature", "test", null);

            SendRecordToGateway(newRecord);

        }catch (Exception e){
            logger.error("Error testing Produce Record", e);
        }
    }

    @Override
    public void RecordReceived(ConsumerRecord Record) {

        logger.info("Record Received " + Record.toString());

        try {
            /*SwapData swapData = swap.SwapDataDeserialization((byte[]) Record.value());

            // Application Logic - in case return to the MN sender

            String PrivateMessageTopic = "PrivateMessageTopic";
            swapData.setTopic("PrivateMessageTopic");

            ProducerRecord<String, byte[]> newRecord = new ProducerRecord<>
                    (PrivateMessageTopic, Record.key().toString(), swap.SwapDataSerialization(swapData));

            SendRecordToGateway(newRecord);*/
            System.out.println("Record Received");

        /*} catch (IOException e) {
            logger.error("Could not read record value", e);*/
        }catch (Exception ex){
            logger.error("Error processing Record Received", ex);
        }

    }

    private void SendRecordToGateway(ProducerRecord Record){

        logger.info("Sending Record To Gateway");

        try {
            threadPool.execute(ckProducer.SendTopic(Record));
        }catch (Exception e){
            logger.error("Error sending topic", e);
        }
    }

    /**
     * Get Topics from file properties.xml file
     *
     * @pre
     * @post
     */
    private List<String> GetListOfTopicsFromFile() throws Exception {
        try {

            logger.info("Get Topics to Consumer from file");

            String rootPath = Thread.currentThread().getContextClassLoader().getResource("").getPath();
            String mnConfigPath = rootPath + "properties.xml";
            Properties mnProps = new Properties();
            mnProps.loadFromXML(new FileInputStream(mnConfigPath));

            List<String> listOfTopics = Arrays.asList(mnProps.getProperty("topics").split(","));

            return listOfTopics;

        } catch (Exception e){

            logger.error("Error loading properties file", e);
            //throw e;

            List<String> listOfTopics = Arrays.asList("temperature");

            return listOfTopics;
        }
    }
}
