package ckafka.connection;

import org.apache.kafka.clients.consumer.ConsumerRecord;

public interface ConsumerListener {

    void RecordReceived(ConsumerRecord<String, String> Record);
}
