package ckafka.connection;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.util.List;
import java.util.Properties;

public class CKConsumerProperties {


    /**
     *  Logger
     */
    final Logger logger = LoggerFactory.getLogger(CKConsumer.class);

    private Properties fileProperties;
    private Properties consumerProperties;

    private List topics;

    /**
     * Consumer Keys
     *
     */
    private String bootstrap_servers_config;
    private String key_deserializer_class_config;
    private String value_deserializer_class_config;
    private String group_id_config;
    private String auto_offset_reset_config;

    public CKConsumerProperties(String KeyDeserializer, String ValueDeserializer){

        bootstrap_servers_config = "";
        key_deserializer_class_config = "";
        value_deserializer_class_config = "";
        group_id_config = "";
        auto_offset_reset_config = "";

        try{

            SetConsumerProperties(KeyDeserializer, ValueDeserializer);

        }catch (Exception e){
            // throw error to notify the user that the consumer is not working
            logger.error("Could not initiate Kafka Properties");
            throw e;
        }

    }

    /**
     * Set Producer Properties
     * If the properties.xml file could not be load set default producer properties
     *
     * @pre must receive key and value serialize
     * @post Set Producer Properties
     */
    private void SetConsumerProperties(String KeySerializer, String ValueSerializer){
        try{
            logger.info("Getting Producer Properties");

            fileProperties = GetConsumerKeysFromFile();
            SetConsumerKeysFromFile(KeySerializer, ValueSerializer);

            consumerProperties = new Properties();
            FillConsumerProperties();

        }catch (Exception e){
            logger.error("Error on Producer Properties", e);
            consumerProperties = null;
        }

    }

    /**
     * Get Properties from file
     * If the properties.xml file could not be load set default producer properties
     *
     * @pre must exists a properties.xml file
     * @post Properties from properties.xml file
     */
    private Properties GetConsumerKeysFromFile(){
        try {

            logger.info("Get Properties to Producer from file");

            String rootPath = Thread.currentThread().getContextClassLoader().getResource("").getPath();
            String mnConfigPath = rootPath + "properties.xml";
            Properties mnProps = new Properties();
            mnProps.loadFromXML(new FileInputStream(mnConfigPath));

            return mnProps;

        } catch (Exception e){

            logger.error("Error loading properties file", e);
            return null;
        }
    }

    /**
     * Set values for Producer Keys from File
     * If the properties.xml file could not be load set default values
     *
     * @pre
     * @post
     */
    private void SetConsumerKeysFromFile(String KeySerializer, String ValueSerializer){

        key_deserializer_class_config = KeySerializer;
        value_deserializer_class_config = ValueSerializer;

        SetDefaultConsumerKeys();

        try {
            if(fileProperties != null){

                if(fileProperties.getProperty("BOOTSTRAP_SERVERS_CONFIG") != null){
                    bootstrap_servers_config = fileProperties.getProperty("BOOTSTRAP_SERVERS_CONFIG");
                }

                if(fileProperties.getProperty("GROUP_ID_CONFIG") != null){
                    group_id_config = fileProperties.getProperty("GROUP_ID_CONFIG");
                }

                if(fileProperties.getProperty("AUTO_OFFSET_RESET_CONFIG") != null){
                    auto_offset_reset_config = fileProperties.getProperty("AUTO_OFFSET_RESET_CONFIG");
                }

            }
        }catch (Exception e){
            logger.error("Error setting Properties - Default Properties loaded", e);
            SetDefaultConsumerKeys();
        }

    }

    /**
     * Set default values for Producer Keys
     *
     * @pre
     * @post
     */
    private void SetDefaultConsumerKeys(){
        bootstrap_servers_config = "192.168.0.102:9092"; //"127.0.0.1:9092"
        group_id_config = "gw-consumer";
        auto_offset_reset_config = "latest";
    }

    /**
     * Fill Producer Properties
     *
     * @pre
     * @post
     */
    private void FillConsumerProperties(){

        consumerProperties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrap_servers_config);
        consumerProperties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, key_deserializer_class_config);
        consumerProperties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, value_deserializer_class_config);
        consumerProperties.setProperty(ConsumerConfig.GROUP_ID_CONFIG, group_id_config);
        consumerProperties.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, auto_offset_reset_config);

    }

    public Properties GetConsumerProperties(){
        return consumerProperties;
    }
}
