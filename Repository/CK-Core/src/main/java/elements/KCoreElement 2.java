package elements;

import kafka.consumer.ConsumerListener;
import kafka.consumer.KConsumer;
import kafka.producer.KProducer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.ByteArrayDeserializer;
import org.apache.kafka.common.serialization.ByteArraySerializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import properties.EnvironmentVariables;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ScheduledThreadPoolExecutor;

public class KCoreElement implements ConsumerListener {

    /**
     *  Logger
     */
    private final Logger logger = LoggerFactory.getLogger(KCoreElement.class);

    private KProducer kProducer;

    private KConsumer kConsumer;

    /**
     *  ThreadPoll
     */
    protected final ScheduledThreadPoolExecutor threadPool;

    private EnvironmentVariables environmentVariables;

    /**
     *  Override it to delivery topics internally to the Consumer
     */
    public List<String> GetInternalListOfStrings(){
        return new ArrayList<>();
    }

    public KCoreElement(){

        List<String> internalListOfStrings = GetInternalListOfStrings();

        String serializeKey = StringSerializer.class.getName();
        String deserializeKey = StringDeserializer.class.getName();
        String serializeValue = ByteArraySerializer.class.getName();
        String deserializeValue = ByteArrayDeserializer.class.getName();

        environmentVariables = new EnvironmentVariables();

        threadPool = new ScheduledThreadPoolExecutor(1000);

        try{
            logger.info("Starting Producer");

            kProducer = new KProducer(serializeKey, serializeValue, environmentVariables, "app.producer");

        }catch (Exception e){
            logger.error("Error starting producer", e);
            System.exit(1);
        }

        try {
            logger.info("Starting Consumer");

            kConsumer = new KConsumer(deserializeKey, deserializeValue, environmentVariables, "app.consumer", internalListOfStrings);
            kConsumer.addConsumerListener(this);
            kConsumer.start();

        }catch (Exception e){
            logger.error("Error starting consumer", e);
            System.exit(1);
        }

    }

    @Override
    public void RecordReceived(ConsumerRecord Record) {
        logger.info("Record Received " + Record.value().toString());
    }

    public void SendRecord(ProducerRecord Record){
        this.threadPool.execute(this.kProducer.SendTopic(Record));
    }

    public void SendMultipleRecords(List<ProducerRecord> Records){
        this.threadPool.execute(this.kProducer.SendMultipleTopics(Records));
    }

    protected ProducerRecord CreateRecord(String Topic, String Key, byte[] Value){
        logger.info("Producing Record");
        try {

            ProducerRecord<String, byte[]> PRecord = new ProducerRecord<>(Topic, Key, Value);
            return PRecord;

        }catch (Exception e){
            logger.error("Error Producing Record", e);
            return null;
        }
    }

    protected String GetEnvironmentVariable(String VariableKey){
        logger.info("Getting Environment Variable");
        try{
            return this.environmentVariables.GetEnvironmentVariableValue(VariableKey);
        }catch (Exception e){
            logger.error("Error Getting Environment Variable", e);
            return null;
        }
    }

    protected Map<String, String> GetEnvironmentVariables(List<String> VariablesKeys){
        logger.info("Getting Environment Variable");
        try{
            return this.environmentVariables.GetEnvironmentVariablesValues(VariablesKeys);
        }catch (Exception e){
            logger.error("Error Getting Environment Variable", e);
            return null;
        }
    }
}
