package application;

import ckafka.data.Swap;
import ckafka.data.SwapData;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;


public class OutboundApp extends ModelApplication{

    final Logger logger = LoggerFactory.getLogger(InboundApp.class);

    private FileWriter writer;
    private static String file = "data.txt";        // app.outbound.file >> CHANGE
    public Integer qtdMessages = 100;
    public Integer beginRegister = 1200000;

    private ConcurrentHashMap<String, Map<Integer, Instant>> SendTimes;
    private ConcurrentHashMap<String, Map<Integer, Instant>> ReceiveTimes;
    private ConcurrentHashMap<String, HashSet<Integer>> IndexMap;
    private ConcurrentHashMap<String, Integer> Indexes;

    /**
     * Set Data Handler
     */
    private Swap swap;
    private ObjectMapper objectMapper;

    public boolean Register = false;

    public static void main(String[] args) {
        new OutboundApp();
    }

    private OutboundApp() {
        super("app.outbound.producer",
                "app.outbound.consumer");

        SendTimes = new ConcurrentHashMap<>();
        ReceiveTimes = new ConcurrentHashMap<>();
        IndexMap = new ConcurrentHashMap<>();
        Indexes = new ConcurrentHashMap<>();

        file = environmentVariables.GetEnvironmentVariableValue("app.outbound.file");
        qtdMessages = new Integer(environmentVariables.GetEnvironmentVariableValue("app.outbound.qtdMsg"));
        beginRegister = new Integer(environmentVariables.GetEnvironmentVariableValue("app.outbound.beginReg"));

        // Data Swap
        this.objectMapper = new ObjectMapper();
        this.swap = new Swap(objectMapper);

        AppRegisterTask register = new AppRegisterTask(this);
        threadPool.execute(register);
        logger.info("SetUp Inbound End");
    }

    @Override
    public List<String> GetInternalListOfStrings(){
        List<String> topics = new ArrayList<>();
        topics.add("Teste");
        return topics;
    }

    @Override
    public void RecordReceived(ConsumerRecord Record) {
        try {
            Instant inst = Instant.now();
            String mnId = Record.key().toString();
            if(Indexes.containsKey(mnId)){
                RegisterReturn(Record, inst);
            }else {
                RegisterNewMN(mnId);
            }
        }catch (Exception e){
            logger.error("Error Record Received", e);
        }
    }

    private void RegisterReturn(ConsumerRecord record, Instant inst) {
        try {
            SwapData data = swap.SwapDataDeserialization((byte[]) record.value());
            int counter = getCounterAsInteger(data.getMessage());
            if(counter > 0){
                String mnId = record.key().toString();
                if(ReceiveTimes.containsKey(mnId)){
                    ReceiveTimes.get(mnId).putIfAbsent(counter, inst);
                }else {
                    Map<Integer, Instant> map = new ConcurrentHashMap<>();
                    map.put(counter, inst);
                    ReceiveTimes.put(mnId, map);
                }
            }
        } catch (Exception e) {
            logger.error("Error Register Return", e);
        }
    }

    private void RegisterNewMN(String mnId) {
        Indexes.put(mnId, 1);

        HashSet<Integer> sent = new HashSet<Integer>();
        IndexMap.put(mnId, sent);

        OutboundAppTask outTask = new OutboundAppTask(this, mnId);
        threadPool.execute(outTask);
    }

    public void RegisterSend(String mnId, Integer count){
        Instant sendTime = Instant.now();
        if(SendTimes.containsKey(mnId)){
            SendTimes.get(mnId).put(count, sendTime);
        }else {
            Map<Integer, Instant> map = new ConcurrentHashMap<>();
            map.put(count, sendTime);
            SendTimes.put(mnId, map);
        }
    }


    // Record Creation
    public ProducerRecord NewRecord(String Mn, int MessageCounter) throws Exception {
        SwapData swapContent = CreateContent(MessageCounter);
        return CreateRecord(
                "PrivateMessageTopic", Mn, swap.SwapDataSerialization(swapContent)
        );
    }

    private SwapData CreateContent(Integer MessageCounter) {
        byte[] messageContent = getCounterAsByte(MessageCounter);

        SwapData content = new SwapData();
        content.setMessage(messageContent);
        content.setTopic("PrivateMessageTopic");
        content.setDuration(60);
        return content;
    }


    private byte[] getCounterAsByte(Integer counter){
        return counter.toString().getBytes(StandardCharsets.UTF_8);
    }

    private Integer getCounterAsInteger(byte[] counter){
        return new Integer (new String(counter, StandardCharsets.UTF_8));
    }

    private void SendPrivateMessage(ConsumerRecord Record) {
        try{
            SendRecord(
                    CreateRecord(
                            "PrivateMessageTopic",
                            Record.key().toString(),
                            (byte[])Record.value()
                    )
            );
        }catch (Exception e)
        {
            logger.error("Error SendPrivateMessage", e);
        }
    }

    // Register
    public void CreateFile()  {
        try {
            writer = new FileWriter(file, true);
        } catch (IOException e) {
            logger.error("Unable to Create File", e);
        }
    }

    public void CloseFile(){
        try {
            writer.close();
        } catch (IOException e) {
            logger.error("Unable to Close File", e);
        }
    }

    public void Register() {
        logger.info("Register");
        CreateFile();
        for (String nodeId: Indexes.keySet()) {
            if(SendTimes.containsKey(nodeId) &&
                    ReceiveTimes.containsKey(nodeId)){
                Map<Integer, Instant> sendMap = SendTimes.get(nodeId);
                Map<Integer, Instant> receiveMap = ReceiveTimes.get(nodeId);
                HashSet<Integer> registerMap = IndexMap.get(nodeId);
                Integer UnansweredMessages = sendMap.keySet().size() - receiveMap.keySet().size();

                Map<Integer, Long> durationMap = new HashMap<>();

                sendMap.keySet().forEach(
                        (Integer count) -> {
                            if(!registerMap.contains(count)){
                                if(receiveMap.containsKey(count)){
                                    Long roundTrip =
                                            Duration.between(sendMap.get(count), receiveMap.get(count)).toMillis();
                                    durationMap.put(count, roundTrip);
                                    registerMap.add(count);
                                }
                            }
                        }
                );
                String line = nodeId + ";" + UnansweredMessages+ ";"
                        + sendMap.keySet().size() + ";" + durationMap.toString();
                Write(line);

            }

        }

        CloseFile();
    }

    public void Write(String line){
        try {
            writer.write(line);
            writer.write('\n');
        } catch (IOException e) {
            logger.error("Unable to Write File", e);
        }
    }
}