package application;

import ckafka.data.Swap;
import ckafka.data.SwapData;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;


public class GroupApp extends ModelApplication{

    final Logger logger = LoggerFactory.getLogger(GroupApp.class);

    private FileWriter writer;
    private static String file = "data.txt";        // app.outbound.file >> CHANGE
    public Integer qtdMessages = 100;
    public Integer beginRegister = 1200000;
    private String topicWrite;

    private ConcurrentHashMap<Integer, Instant> SendTimes;
    private ConcurrentHashMap<String, Map<Integer, Instant>> ReceiveTimes;
    private ConcurrentHashMap<String, HashSet<Integer>> IndexMap;

    /**
     * Set Data Handler
     */
    private Swap swap;
    private ObjectMapper objectMapper;

    public boolean Register = false;

    private List<Integer> Groups;

    public static void main(String[] args) {
        new GroupApp();
    }

    private GroupApp() {
        super("app.group.producer",
                "app.group.consumer");

        SendTimes = new ConcurrentHashMap<>();
        ReceiveTimes = new ConcurrentHashMap<>();
        IndexMap = new ConcurrentHashMap<>();

        file = environmentVariables.GetEnvironmentVariableValue("app.group.file");
        qtdMessages = new Integer(environmentVariables.GetEnvironmentVariableValue("app.group.qtdMsg"));
        beginRegister = new Integer(environmentVariables.GetEnvironmentVariableValue("app.group.beginReg"));
        topicWrite = environmentVariables.GetEnvironmentVariableValue("app.group.topicWrite");


        // Data Swap
        this.objectMapper = new ObjectMapper();
        this.swap = new Swap(objectMapper);

        GetGroups();
        StartGroups();

        AppGrpRegisterTask register = new AppGrpRegisterTask(this);
        threadPool.execute(register);
        logger.info("SetUp Inbound End");
    }

    private void GetGroups(){
        Groups = new ArrayList<>();
        String file = environmentVariables.GetEnvironmentVariableValue("app.group.groups");
        String[] groupsSplit = file.split(",");
        for(String group:  groupsSplit){
            Groups.add(new Integer(group.trim()));
        }
    }

    private void StartGroups(){
        int interval = 15000;
        int msg_interval = 15000 * Groups.size();
        for(int groupId: Groups){
            try {
                AppGrpSenderTask groupSend = new AppGrpSenderTask(this, groupId, msg_interval);
                threadPool.execute(groupSend);
                Thread.sleep(interval);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public List<String> GetInternalListOfStrings(){
        List<String> topics = new ArrayList<>();
        //topics.add("Teste");
        return topics;
    }

    @Override
    public void RecordReceived(ConsumerRecord Record) {
        try {
            Instant inst = Instant.now();
            String mnId = Record.key().toString();
            RegisterReturn(Record, inst);
        }catch (Exception e){
            logger.error("Error Record Received", e);
        }
    }

    private void RegisterReturn(ConsumerRecord record, Instant inst) {
        try {
            SwapData data = swap.SwapDataDeserialization((byte[]) record.value());
            Integer counter = getCounterAsInteger(data.getMessage());
            if(counter > 0){
                String mnId = record.key().toString();
                if(ReceiveTimes.containsKey(mnId)){
                    ReceiveTimes.get(mnId).putIfAbsent(counter, inst);
                }else {
                    Map<Integer, Instant> map = new ConcurrentHashMap<>();
                    map.put(counter, inst);
                    ReceiveTimes.put(mnId, map);

                    HashSet<Integer> setRegistered = new HashSet<>();
                    IndexMap.put(mnId, setRegistered);
                }
            }
        } catch (Exception e) {
            logger.error("Error Register Return", e);
        }
    }


    public void RegisterSend(int MessageCounter){
        Instant sendTime = Instant.now();
        SendTimes.putIfAbsent(MessageCounter, sendTime);
    }


    // Record Creation
    public ProducerRecord NewRecord(Integer group, int MessageCounter) throws Exception {
        SwapData swapContent = CreateContent(MessageCounter);
        return CreateRecord(
                "GroupMessageTopic", group.toString(), swap.SwapDataSerialization(swapContent)
        );
    }

    private SwapData CreateContent(Integer MessageCounter) {
        byte[] messageContent = getCounterAsByte(MessageCounter);

        SwapData content = new SwapData();
        content.setMessage(messageContent);
        //content.setTopic("GroupMessageTopic");
        content.setTopic(topicWrite);
        content.setDuration(60);
        return content;
    }


    private byte[] getCounterAsByte(Integer counter){
        return counter.toString().getBytes(StandardCharsets.UTF_8);
    }

    private Integer getCounterAsInteger(byte[] counter){
        return new Integer (new String(counter, StandardCharsets.UTF_8));
    }

    private void SendPrivateMessage(ConsumerRecord Record) {
        try{
            SendRecord(
                    CreateRecord(
                            "PrivateMessageTopic",
                            Record.key().toString(),
                            (byte[])Record.value()
                    )
            );
        }catch (Exception e)
        {
            logger.error("Error SendPrivateMessage", e);
        }
    }

    // Register
    public void CreateFile()  {
        try {
            writer = new FileWriter(file, true);
        } catch (IOException e) {
            logger.error("Unable to Create File", e);
        }
    }

    public void CloseFile(){
        try {
            writer.close();
        } catch (IOException e) {
            logger.error("Unable to Close File", e);
        }
    }

    public void Register() {
        logger.info("Register");
        CreateFile();
        for (String nodeId: ReceiveTimes.keySet()) {

            Map<Integer, Instant> receiveMap = ReceiveTimes.get(nodeId);

            Map<Integer, Long> durationMap = new HashMap<>();

            SendTimes.keySet().forEach(
                    (Integer count) -> {
                        if(!IndexMap.get(nodeId).contains(count)){
                            if(receiveMap.containsKey(count)){
                                Long roundTrip =
                                        Duration.between(SendTimes.get(count), receiveMap.get(count)).toMillis();
                                durationMap.put(count, roundTrip);
                                IndexMap.get(nodeId).add(count);
                            }
                        }
                    }
            );

            String line = nodeId + ";" + durationMap.toString();
            Write(line);

        }

        CloseFile();
    }

    public void Write(String line){
        try {
            writer.write(line);
            writer.write('\n');
        } catch (IOException e) {
            logger.error("Unable to Write File", e);
        }
    }
}