package application;


public class AppRegisterTask implements Runnable {

    private OutboundApp app;
    public AppRegisterTask(OutboundApp App){
        app = App;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(app.beginRegister);
            app.Register = true;
            while (true){
                Thread.sleep(60000);
                app.Register();
            }
        } catch (InterruptedException e) {
            app.logger.error("Error AppRegisterTask", e);
        }
    }
}