package application;


import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OutboundAppTask implements Runnable{

    final Logger logger = LoggerFactory.getLogger(OutboundAppTask.class);

    private Integer msgCount;
    private String MN;
    private OutboundApp Out;
    private boolean send = true;

    public OutboundAppTask(OutboundApp out, String mn){
        Out = out;
        MN = mn;
        msgCount = 0;
    }

    @Override
    public void run() {
        try {
            while (send){
                Thread.sleep(15000);
                if(Out.Register){
                    msgCount++;
                }
                ProducerRecord record= Out.NewRecord(MN, msgCount);
                if(Out.Register){
                    Out.RegisterSend(MN, msgCount);
                }
                Out.kProducer.SendTopic(record).start();

                if(msgCount > Out.qtdMessages){
                    send = false;
                }
            }
        } catch (Exception e) {
            logger.error("Outbound App Task Error", e);
        }
    }
}
