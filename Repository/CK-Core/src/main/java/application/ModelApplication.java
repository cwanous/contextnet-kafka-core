package application;

import kafka.consumer.ConsumerListener;
import kafka.consumer.KConsumer;
import kafka.producer.KProducer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import properties.EnvironmentVariables;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ScheduledThreadPoolExecutor;

public class ModelApplication implements ConsumerListener {

    /**
     *  Logger
     */
    final Logger logger = LoggerFactory.getLogger(ModelApplication.class);

    protected KProducer kProducer;

    protected KConsumer kConsumer;

    /**
     *  ThreadPoll
     */
    protected final ScheduledThreadPoolExecutor threadPool;

    private String serializeKey;
    private String deserializeKey;
    private String serializeValue;
    private String deserializeValue;

    public EnvironmentVariables environmentVariables;

    /**
     *  Override it to delivery topics internally to the Consumer
     */
    public List<String> GetInternalListOfStrings(){
        return new ArrayList<>();
    }


    public ModelApplication(){

        List<String> internalListOfStrings = GetInternalListOfStrings();

        serializeKey = StringSerializer.class.getName();
        deserializeKey = StringDeserializer.class.getName();
        serializeValue = ByteArraySerializer.class.getName();
        deserializeValue = ByteArrayDeserializer.class.getName();


        environmentVariables = new EnvironmentVariables();

        threadPool = new ScheduledThreadPoolExecutor(2000);

        try{
            logger.info("Starting Producer");

            kProducer = new KProducer(serializeKey, serializeValue, environmentVariables, "app.producer");

        }catch (Exception e){
            logger.error("Error starting producer", e);
            System.exit(1);
        }

        try {
            logger.info("Starting Consumer");

            kConsumer = new KConsumer(deserializeKey, deserializeValue, environmentVariables, "app.consumer", internalListOfStrings);
            kConsumer.addConsumerListener(this);
            kConsumer.start();

        }catch (Exception e){
            logger.error("Error starting consumer", e);
            System.exit(1);
        }

    }


    public ModelApplication(String producerId, String consumerId){

        List<String> internalListOfStrings = GetInternalListOfStrings();

        serializeKey = StringSerializer.class.getName();
        deserializeKey = StringDeserializer.class.getName();
        serializeValue = ByteArraySerializer.class.getName();
        deserializeValue = ByteArrayDeserializer.class.getName();


        environmentVariables = new EnvironmentVariables();

        threadPool = new ScheduledThreadPoolExecutor(8000);

        try{
            logger.info("Starting Producer");

            kProducer = new KProducer(serializeKey, serializeValue, environmentVariables, producerId);

        }catch (Exception e){
            logger.error("Error starting producer", e);
            System.exit(1);
        }

        try {
            logger.info("Starting Consumer");

            kConsumer = new KConsumer(deserializeKey, deserializeValue, environmentVariables, consumerId, internalListOfStrings);
            kConsumer.addConsumerListener(this);
            kConsumer.start();

        }catch (Exception e){
            logger.error("Error starting consumer", e);
            System.exit(1);
        }

    }


    public ModelApplication(String producerId, String consumerId, List<String> InternalTopics){

        serializeKey = StringSerializer.class.getName();
        deserializeKey = StringDeserializer.class.getName();
        serializeValue = ByteArraySerializer.class.getName();
        deserializeValue = ByteArrayDeserializer.class.getName();


        environmentVariables = new EnvironmentVariables();

        threadPool = new ScheduledThreadPoolExecutor(8000);

        try{
            logger.info("Starting Producer");

            kProducer = new KProducer(serializeKey, serializeValue, environmentVariables, producerId);

        }catch (Exception e){
            logger.error("Error starting producer", e);
            System.exit(1);
        }

        try {
            logger.info("Starting Consumer");

            kConsumer = new KConsumer(deserializeKey, deserializeValue, environmentVariables, consumerId, InternalTopics);
            kConsumer.addConsumerListener(this);
            kConsumer.start();

        }catch (Exception e){
            logger.error("Error starting consumer", e);
            System.exit(1);
        }

    }

    @Override
    public void RecordReceived(ConsumerRecord Record) {
        logger.info("Record Received " + Record.value().toString());


    }


    public void SendRecord(ProducerRecord Record){
        this.threadPool.execute(this.kProducer.SendTopic(Record));
    }


    public void SendMultipleRecords(List<ProducerRecord> Records){
        this.threadPool.execute(this.kProducer.SendMultipleTopics(Records));
    }


    public ProducerRecord CreateRecord(String Topic, String NodeID, byte[] Content){
        try {
            ProducerRecord<String, byte[]> record
                    = new ProducerRecord<>(Topic, NodeID, Content);
            return record;
        }catch (Exception e){
            logger.error("Error converting Swap Data to Context Producer-Record", e);
            return null;
        }
    }

}
