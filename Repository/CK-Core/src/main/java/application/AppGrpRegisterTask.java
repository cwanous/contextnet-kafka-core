package application;


public class AppGrpRegisterTask implements Runnable {

    private GroupApp app;

    public AppGrpRegisterTask(GroupApp App){
        app = App;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(app.beginRegister);
            app.Register = true;
            while (true){
                Thread.sleep(60000);
                app.Register();
            }
        } catch (InterruptedException e) {
            app.logger.error("Error AppRegisterTask", e);
        }
    }
}