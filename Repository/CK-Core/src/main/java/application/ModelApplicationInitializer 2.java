package application;

import ckafka.data.Swap;
import ckafka.data.SwapData;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.nio.charset.StandardCharsets;
import java.util.UUID;

public class ModelApplicationInitializer {

    private ModelApplication model;

    private Swap swap;
    private ObjectMapper objectMapper;

    public static void main(String[] args) {
        new ModelApplicationInitializer();
    }


    private ModelApplicationInitializer() {
        this.objectMapper = new ObjectMapper();
        this.swap = new Swap(objectMapper);

        model = new ModelApplication();

        SendPrivateMessage();
    }


    private void SendPrivateMessage() {
        try{

            UUID MNId = UUID.fromString("5b221977-1216-426f-b8a5-e2a718cf9eef");
            model.SendRecord(model.CreateRecord("PrivateMessageTopic", MNId.toString(), swap.SwapDataSerialization(GetSwapData())));
        }catch (Exception e)
        {
            model.logger.error("Error SendPrivateMessage", e);
        }
    }


    private void SendGroupMessage(){
        try{

            int GroupID = 1;
            model.SendRecord(model.CreateRecord("GroupMessageTopic", Integer.toString(GroupID), swap.SwapDataSerialization(GetSwapData())));
        }catch (Exception e)
        {
            model.logger.error("Error SendPrivateMessage", e);
        }
    }


    public SwapData GetSwapData()
    {
        byte[] content = "Model Application Test".getBytes(StandardCharsets.UTF_8);
        SwapData serializableData = new SwapData();
        serializableData.setMessage(content);
        serializableData.setDuration(60);
        return serializableData;
    }
}
