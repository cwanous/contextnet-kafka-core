package application;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AppGrpSenderTask implements Runnable{

    final Logger logger = LoggerFactory.getLogger(OutboundAppTask.class);
    private GroupApp app;
    private Integer Group;
    private Integer Interval = 15000;
    private Integer msgCount = 0;
    private boolean send = true;

    public AppGrpSenderTask(GroupApp App, int group){
        app = App;
        Group = group;
    }

    public AppGrpSenderTask(GroupApp App, int group, int interval){
        app = App;
        Group = group;
        Interval = interval;
    }

    @Override
    public void run() {
        try {
            while (send){
                Thread.sleep(Interval);
                if(app.Register){
                    msgCount++;
                }
                int msgId = msgCount*10 + Group;
                ProducerRecord record= app.NewRecord(Group, msgId);
                if(app.Register){
                    app.RegisterSend(msgId);
                }
                app.kProducer.SendTopic(record).start();

                if(msgCount >= app.qtdMessages){
                    send = false;
                }
            }
        } catch (Exception e) {
            logger.error("Error on AppGrpSenderTask", e);
        }

    }
}
