package application;

import ckafka.data.Swap;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InboundApp extends ModelApplication{

    final Logger logger = LoggerFactory.getLogger(InboundApp.class);

    /**
     * Set Data Handler
     */
    private Swap swap;
    private ObjectMapper objectMapper;

    public static void main(String[] args) {
        new InboundApp();
    }


    private InboundApp() {
        super("app.inbound.producer",
                "app.inbound.consumer");
        // Data Swap
        this.objectMapper = new ObjectMapper();
        this.swap = new Swap(objectMapper);
    }

    @Override
    public void RecordReceived(ConsumerRecord Record) {
        logger.info("Record Received");
        try{
            SendPrivateMessage(Record);
            /*
            if(Record.topic().equals("LoadReport")){
                LoadReport report = (LoadReport) swap.DataDeserialization((byte[]) Record.value(), LoadReport.class);
                System.out.println(report.getRateOfAvailableJVMMemory());
            }else {
                SendPrivateMessage(Record);
            }
            */
        }catch (Exception e)
        {
            logger.error("Error DataDeserialization", e);
        }
    }

    private void SendPrivateMessage(ConsumerRecord Record) {
        try{
            SendRecord(
                    CreateRecord(
                            "PrivateMessageTopic",
                            Record.key().toString(),
                            (byte[])Record.value()
                    )
            );
        }catch (Exception e)
        {
            logger.error("Error SendPrivateMessage", e);
        }
    }
}
