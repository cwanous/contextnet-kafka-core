package kafka.consumer;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.errors.WakeupException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import properties.EnvironmentVariables;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

public class KConsumer extends Thread{

    /**
     *  Logger
     */
    private final Logger logger = LoggerFactory.getLogger(KConsumer.class);

    /**
     *  Consumer Properties
     */
    private Properties consumerProperties;
    private ConsumerProperties ckConsumerProperties;

    /**
     *  List of Listeners
     */
    protected static List<ConsumerListener> Listeners;

    /**
     *  List of subscribed topics
     */
    private List<String> topics;


    public KConsumer(String KeyDeserialize,
                     String ValueSerialize,
                     EnvironmentVariables EnvironmentVariables,
                     String ApplicationPrefix,
                     List<String> InternalTopics) throws Exception {

        Listeners = new ArrayList<ConsumerListener>();

        try{
             this.ckConsumerProperties = new ConsumerProperties(KeyDeserialize,
                    ValueSerialize,
                    EnvironmentVariables,
                    ApplicationPrefix);

            consumerProperties =  this.ckConsumerProperties.GetConsumerProperties();

            topics = GetTopics(InternalTopics);

        }catch (Exception e){
            logger.error("Error initializing KConsumer");
            throw e;
        }

    }

    private List<String> GetTopics(List<String> InternalTopics) throws Exception {
        List<String> topicsList;
        topicsList = InternalTopics;

        List<String> externalList = this.ckConsumerProperties.GetTopics();

        if(externalList != null){
            topicsList.addAll(externalList);
        }

        if(topicsList.size() > 0){
            return topicsList;
        }else {
            throw new Exception("There are no consumer topics");
        }
    }


    public void addConsumerListener(ConsumerListener lis){
        Listeners.add(lis);
    }

    @Override
    public void run(){

        //latch for dealing whit multiple threads
        CountDownLatch latch = new CountDownLatch(1);

        // Creating the Consumer
        ConsumerRunnable myConsumerRunnable = new ConsumerRunnable(topics, consumerProperties, latch);

        // Starting the Thread
        Thread myThread = new Thread(myConsumerRunnable);
        myThread.start();

        // add a Shutdown hook
        Runtime.getRuntime().addShutdownHook(new Thread( () -> {
            logger.warn("Caught shutdown hook");
            (myConsumerRunnable).shutdown();

            try{
                latch.await();
            } catch (InterruptedException e){
                logger.error("Application Interrupted", e);
            }
            logger.warn("Application has exited");
        }
        ));

        try{
            latch.await();
        } catch (InterruptedException e){
            logger.warn("Application got interrupted", e);
        }finally {
            logger.warn("Application is closing");
        }


    }


    public static class ConsumerRunnable implements Runnable {

        private CountDownLatch latch;
        private KafkaConsumer<String, String> consumer;

        public ConsumerRunnable(List Topics,
                                Properties ConsumerProperties,
                                CountDownLatch latch){
            this.latch = latch;

            // create the Consumer
            consumer = new KafkaConsumer<String, String>(ConsumerProperties);

            // subscribe Consumer to our topics
            consumer.subscribe(Topics);
        }

        @Override
        public void run() {

            try {
                // poll for new data
                while (true) {
                    // Polling records
                    ConsumerRecords<String, String> records = consumer.poll(1000); // <<<<<<<<<<<<<<<<<<<<<<<<<<

                    // Notifying records polled
                    for (ConsumerRecord<String, String> record : records){
                        for (ConsumerListener  lis : Listeners) {
                            lis.RecordReceived(record);
                        }
                    }

                }
            } catch (WakeupException e){
                // Received shutdown sing
            } finally {
                consumer.close();
                // tell our main code we are done with the consumer (single thread count from 1 only)
                latch.countDown();
            }
        }

        // when called shutdown calls consumer.wakeup() that will raise an error
        public void shutdown() {
            // the wakeup() is a special method to interrupt consumer.poll
            // it will throw the exception "WakeUpException"
            consumer.wakeup();
        }
    }
}
