package kafka.consumer;

import kafka.element.KafkaElement;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import properties.EnvironmentVariables;

import java.util.List;
import java.util.Properties;

public class ConsumerProperties extends KafkaElement {

    /**
     *  Logger
     */
    final Logger logger = LoggerFactory.getLogger(ConsumerProperties.class);

    public ConsumerProperties(String KeyDeserializer,
                              String ValueDeserializer,
                              EnvironmentVariables EnvironmentVariables,
                              String ApplicationPrefix){

        super(EnvironmentVariables, ApplicationPrefix);

        try{

            LoadConsumerProperties();

            LoadProperties(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, KeyDeserializer,
                    ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, ValueDeserializer);


        }catch (Exception e){
            // throw error to notify the user that the consumer is not working
            logger.error("Could not initiate Kafka Properties");
            throw e;
        }

    }

    List<String> GetTopics(){
        logger.info("Getting Consumer Topics");
        try {

            return GetTopicsFromEnvironmentVariables();

        }catch (Exception e){
            logger.info("There are no consumer topics on environment variables", e);
            return null;
        }
    }

    public Properties GetConsumerProperties(){
        return properties;
    }

    /**
     * List Consumer Properties and its Default Values
     *
     *
     */
    private void LoadConsumerProperties(){
        logger.info("Load Producer Properties: Default values and Identifications");
        try{

            mapOfProperties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092");

            mapOfProperties.put(ConsumerConfig.GROUP_ID_CONFIG, "gw-consumer");

            mapOfProperties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest");

        }catch (Exception e){
            logger.error("Error listing producer properties");
            throw e;
        }
    }

}
