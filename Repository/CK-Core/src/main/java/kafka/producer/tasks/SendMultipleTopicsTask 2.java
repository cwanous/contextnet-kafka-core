package kafka.producer.tasks;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class SendMultipleTopicsTask extends Thread{

    /**
     *  Logger
     */
    private final Logger logger = LoggerFactory.getLogger(SendMultipleTopicsTask.class);

    private KafkaProducer producer;
    private List<ProducerRecord> records;

    public SendMultipleTopicsTask(KafkaProducer Producer, List<ProducerRecord> Records){
        this.producer = Producer;
        this.records = Records;
    }

    // Send Topic
    @Override
    public void run() {
        try {
            for(ProducerRecord record : records){
                producer.send(record);
            }
            logger.info("Send Multiple Topics Task Complete");

        }catch (Exception e){
            logger.error("Error running Send Multiple Topics Task", e);
        }

    }
}
