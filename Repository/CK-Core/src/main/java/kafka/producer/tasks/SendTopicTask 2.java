package kafka.producer.tasks;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SendTopicTask extends Thread{

    /**
     *  Logger
     */
    private final Logger logger = LoggerFactory.getLogger(SendTopicTask.class);

    private KafkaProducer producer;
    private ProducerRecord record;

    public SendTopicTask(KafkaProducer Producer,
                         ProducerRecord Record){
        this.producer = Producer;
        this.record = Record;
    }

    // Send Topic
    @Override
    public void run() {
        try{
            producer.send(record);

            logger.info("Send Topic Task Complete");
        }catch (Exception e){
            logger.error("Error running Send Topic Task", e);
        }

    }
}
