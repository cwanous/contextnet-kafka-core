package ckafka.data;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.io.Serializable;
import java.time.Instant;
import java.util.List;
import java.util.Set;

public class ConnectionReport implements Serializable {

    @JsonCreator
    public ConnectionReport(@JsonProperty("pingBeginTime") Instant PingBeginTime,
                            @JsonProperty("listReturnTimes") List<Instant> ListReturnTimes,
                            @JsonProperty("listNodesConnected") Set<String> ListNodesConnected){

        this.pingBeginTime = PingBeginTime;
        this.listReturnTimes = ListReturnTimes;
        this.listNodesConnected = ListNodesConnected;
    }

    @JsonProperty("pingBeginTime")
    private Instant pingBeginTime;
    public Instant getPingBeginTime() {
        return pingBeginTime;
    }
    public void setPingBeginTime(Instant PingBeginTime) { this.pingBeginTime = PingBeginTime; }

    @JsonProperty("listReturnTimes")
    //@JsonDeserialize(using = ListReturnTimes.class)
    private List<Instant> listReturnTimes;
    public List<Instant> getListReturnTimes() {
        return listReturnTimes;
    }
    public void setListReturnTimes(List<Instant> ListReturnTimes) { this.listReturnTimes = ListReturnTimes; }

    @JsonProperty("listNodesConnected")
    //@JsonDeserialize(using = ListNodesConnected.class)
    private Set<String> listNodesConnected;
    public Set<String> getListNodesConnected() {
        return listNodesConnected;
    }
    public void setListNodesConnected(Set<String> ListNodesConnected) {  this.listNodesConnected = ListNodesConnected; }

    static class ListReturnTimes extends JsonDeserializer<List<Instant>>{

        @Override
        public List<Instant> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
                throws IOException, JsonProcessingException {
            return null;
        }
    }

    static class ListNodesConnected extends JsonDeserializer<List<String>>{

        @Override
        public List<String> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
                throws IOException, JsonProcessingException {
            return null;
        }
    }

}
