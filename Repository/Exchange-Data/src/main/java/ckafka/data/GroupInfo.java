package ckafka.data;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.io.Serializable;
import java.util.Set;


public class GroupInfo implements Serializable {

    @JsonCreator
    public GroupInfo(@JsonProperty("gwID") String GwID,
                     @JsonProperty("listGroups") Set<Integer> ListGroups){

        this.gwID = GwID;
        this.listGroups = ListGroups;
    }

    @JsonProperty("gwID")
    private String gwID;
    public String getGwID(){
        return gwID;
    }
    public void SetGdID(String GwID) { this.gwID = GwID; }

    @JsonProperty("listGroups")
    private Set<Integer> listGroups;
    public Set<Integer> getListGroups() {
        return listGroups;
    }
    public void setListGroups(Set<Integer> ListGroups) {  this.listGroups = ListGroups; }

    static class ListOfGroups extends JsonDeserializer<Set<Integer>> {

        @Override
        public Set<Integer> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
                throws IOException, JsonProcessingException {
            return null;
        }
    }

}
