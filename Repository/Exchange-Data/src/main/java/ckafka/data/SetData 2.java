package ckafka.data;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Set;

public class SetData implements Serializable {

    private Set set;

    @JsonProperty("set")
    public Set getSet() {
        return set;
    }

    public void setSet(Set Set) {
        this.set = Set;
    }


}
