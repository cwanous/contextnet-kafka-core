package ckafka.data;

import lac.cnclib.sddl.message.ClientLibProtocol;
import lac.cnclib.sddl.message.Message;

import java.io.Serializable;
import java.time.Instant;
import java.util.List;
import java.util.UUID;

public class SwapMessage implements Message {

    private UUID senderID;

    private UUID senderGatewayID;

    private UUID recipientID;

    private UUID recipientGatewayID;

    private long gatewayLogicalTime;


    /**
     *  Exclusive Attributes
     */

    private byte[] contextContent;

    private byte[] messageContent;

    private Instant creationTime;

    private String topic;

    private Integer duration;

    private ClientLibProtocol.MSGType msgType;

    private ClientLibProtocol.PayloadSerialization payloadType;


    public SwapMessage(){
        //super();
        this.msgType = ClientLibProtocol.MSGType.APPLICATION;
        this.payloadType = ClientLibProtocol.PayloadSerialization.JAVA;
    }


    public byte[] getContextContent(){
        return this.contextContent;
    }

    public void setContextContent(byte[] ContextContent){
        this.contextContent = ContextContent;
    }


    public byte[] getMessageContent(){
        return this.messageContent;
    }

    public void setMessageContent(byte[] MessageContent){
        this.messageContent = MessageContent;
    }


    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }


    public Instant getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Instant creationTime) {
        this.creationTime = creationTime;
    }


    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer Duration) {
        this.duration = Duration;
    }


    @Override
    public UUID getSenderID() {
        return this.senderID;
    }
    @Override
    public void setSenderID(UUID uuid) {
        this.senderID = uuid;
    }

    @Override
    public UUID getSenderGatewayID() {
        return this.senderGatewayID;
    }
    @Override
    public void setSenderGatewayID(UUID uuid) {
        this.senderGatewayID = uuid;
    }

    @Override
    public UUID getRecipientID() {
        return this.recipientID;
    }
    @Override
    public void setRecipientID(UUID uuid) {
        this.recipientID = uuid;
    }

    @Override
    public UUID getRecipientGatewayID() {
        return this.recipientGatewayID;
    }
    @Override
    public void setRecipientGatewayID(UUID uuid) {
        this.recipientGatewayID = uuid;
    }

    @Override
    public long getGatewayLogicalTime() {
        return this.gatewayLogicalTime;
    }
    @Override
    public void setGatewayLogicalTime(long l) {
        this.gatewayLogicalTime = l;
    }

    @Override
    public ClientLibProtocol.MSGType getType() {
        return msgType;
    }

    /**
     *  Not Used
     */

    @Override
    public void setType(ClientLibProtocol.MSGType msgType) {

    }
    @Override
    public ClientLibProtocol.PayloadSerialization getPayloadSerialization() {
        return null;
    }
    @Override
    public void setPayloadType(ClientLibProtocol.PayloadSerialization payloadSerialization) {

    }
    @Override
    public void addTag(String s) {

    }
    @Override
    public void removeTag(String s) {

    }
    @Override
    public boolean hasTag(String s) {
        return false;
    }
    @Override
    public Serializable getContentObject() {
        return null;
    }
    @Override
    public byte[] getContent() {
        return new byte[0];
    }
    @Override
    public void setContentObject(Serializable serializable) {

    }
    @Override
    public void setContent(byte[] bytes) {

    }
    @Override
    public List<String> getTagList() {
        return null;
    }
    @Override
    public void setTagList(List<String> list) {

    }

}
