package ckafka.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.Serializable;
import java.time.Instant;

public class SwapData implements Serializable {


    public SwapData(){
        this.creationTime = Instant.now();
    }

    @JsonProperty("topic")
    private String topic;
    public String getTopic() {
        if(this.topic != null){
            return topic;
        }else {
            return "";
        }
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    @JsonProperty("creationTime")
    private Instant creationTime;
    public Instant getCreationTime() {
        return creationTime;
    }

    @JsonProperty("duration")
    private Integer duration;
    public Integer getDuration() {
        return duration;
    }
    public void setDuration(Integer Duration) {
        this.duration = Duration;
    }

    @JsonProperty("message")
    private byte[] message;
    public byte[] getMessage() {
        return message;
    }
    public void setMessage(byte[] message) {
        this.message = message;
    }

    @JsonProperty("context")
    private ObjectNode context;
    public ObjectNode getContext() {
        return context;
    }
    public void setContext(ObjectNode Context){
        context = Context;
    }

}
