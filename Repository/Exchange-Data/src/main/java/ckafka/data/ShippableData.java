package ckafka.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;
import java.time.Instant;
import java.util.UUID;

/**
 * ShippableData is the class exchanged between the mobile components and the Gateway.
 * It has a context attribute that represents the context information's about the mobile node.
 *
 * @author camilawanous
 */


public class ShippableData {

    private ObjectNode context;
    private ObjectNode message;
    private Instant creationTime;
    private String topic;
    private UUID senderId;
    private Integer duration;

    public ShippableData(ObjectNode context, ObjectNode message, String topic, UUID SenderId) {

        this.context = context;
        this.message = message;
        this.topic = topic;
        this.senderId = SenderId;

        Instant now = Instant.now();
        this.creationTime = now;

    }


    public ShippableData(String applicationMsg, String topic, UUID SenderId) {
        try {
            this.message = new ObjectMapper().readValue(applicationMsg, ObjectNode.class);
        } catch (IOException e) {
            this.message = null;
        }
        this.senderId = SenderId;
        this.topic = topic;
        Instant now = Instant.now();
        this.creationTime = now;
    }

    /**
     * ShippableData constructor from String
     */
    public ShippableData(String ShippableDataAsString) throws Exception {
        try {
            ObjectNode shippableDataAsObjectNode = new ObjectMapper().readValue(ShippableDataAsString, ObjectNode.class);

            if(shippableDataAsObjectNode.has("context")){
                this.context = new ObjectMapper().readValue(shippableDataAsObjectNode.get("context").asText(), ObjectNode.class);
            }

            if(shippableDataAsObjectNode.has("message")){
                this.message = new ObjectMapper().readValue(shippableDataAsObjectNode.get("message").asText(), ObjectNode.class);
            }

            this.topic = shippableDataAsObjectNode.get("topic").asText();

            this.senderId = UUID.fromString(shippableDataAsObjectNode.get("senderId").asText());

            this.creationTime = Instant.now();

        } catch (Exception e) {
            throw e;
        }

    }

    /**
    * Override toString method
     */
    @Override
    public String toString() {
        ObjectNode objectNodeFromShippableData = JsonNodeFactory.instance.objectNode();

        if(this.context != null){
            objectNodeFromShippableData.put("context", this.context.toString());
        }

        if(this.message != null){
            objectNodeFromShippableData.put("message", this.message.toString());
        }

        objectNodeFromShippableData.put("creationTime", creationTime.toString());

        objectNodeFromShippableData.put("topic", topic);

        objectNodeFromShippableData.put("senderId", senderId.toString());

        return objectNodeFromShippableData.toString();
    }


    public ObjectNode getMessage() {
        return message;
    }

    public void setMessage(ObjectNode message) {
        this.message = message;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public ObjectNode getContext() {
        return context;
    }

    public void setContext(ObjectNode context) {
        this.context = context;
    }

    public Instant getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Instant creationTime) {
        this.creationTime = creationTime;
    }

    public UUID getSenderId() {
        return senderId;
    }

    public void setSenderId(UUID senderId) {
        this.senderId = senderId;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer Duration) {
        this.duration = Duration;
    }
}
