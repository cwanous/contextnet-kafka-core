package ckafka.data;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

public class LoadReport {

    @JsonCreator
    public LoadReport(@JsonProperty("activePoolUtilization") Double ActivePoolUtilization,
                      @JsonProperty("cpuLoad") Double CpuLoad,
                      @JsonProperty("gatewayID") UUID GatewayID,
                      @JsonProperty("hardwareTotalMemory") Long HardwareTotalMemory,
                      @JsonProperty("jvmFTotalMemory") Long JVMFTotalMemory,
                      @JsonProperty("poolQueueDimension") Double PoolQueueDimension,
                      @JsonProperty("rateOfAvailableHardwareMemory") Double RateOfAvailableHardwareMemory,
                      @JsonProperty("rateOfAvailableJVMMemory") Double RateOfAvailableJVMMemory,
                      @JsonProperty("totalMN") Double TotalMN,
                      @JsonProperty("totalLostMN") Double TotalLostMN,
                      @JsonProperty("unsentConnections") Double UnsentConnections){

        this.activePoolUtilization = ActivePoolUtilization;
        this.cpuLoad = CpuLoad;
        this.gatewayID = GatewayID;
        this.hardwareTotalMemory = HardwareTotalMemory;
        this.jvmFTotalMemory = JVMFTotalMemory;
        this.poolQueueDimension = PoolQueueDimension;
        this.rateOfAvailableHardwareMemory = RateOfAvailableHardwareMemory;
        this.rateOfAvailableJVMMemory = RateOfAvailableJVMMemory;
        this.totalMN = TotalMN;
        this.totalLostMN = TotalLostMN;
        this.unsentConnections = UnsentConnections;
    }

    @JsonProperty("activePoolUtilization")
    private Double activePoolUtilization;
    public Double getActivePoolUtilization() { return activePoolUtilization; }

    @JsonProperty("cpuLoad")
    private Double cpuLoad;
    public Double getCpuLoad() {
        return cpuLoad;
    }

    @JsonProperty("gatewayID")
    private UUID gatewayID;
    public UUID getGatewayID(){
        return gatewayID;
    }

    @JsonProperty("hardwareTotalMemory")
    private Long hardwareTotalMemory;
    public Long getHardwareTotalMemory() {
        return hardwareTotalMemory;
    }

    @JsonProperty("jvmFTotalMemory")
    private Long jvmFTotalMemory;
    public Long getJvmFTotalMemory() {
        return jvmFTotalMemory;
    }

    @JsonProperty("poolQueueDimension")
    private Double poolQueueDimension;
    public Double getPoolQueueDimension() {return poolQueueDimension; }

    @JsonProperty("rateOfAvailableHardwareMemory")
    private Double rateOfAvailableHardwareMemory;
    public Double getRateOfAvailableHardwareMemory(){ return rateOfAvailableHardwareMemory;}

    @JsonProperty("rateOfAvailableJVMMemory")
    private Double rateOfAvailableJVMMemory;
    public Double getRateOfAvailableJVMMemory(){ return rateOfAvailableJVMMemory;}

    @JsonProperty("totalMN")
    private Double totalMN;
    public Double getTotalMN(){ return totalMN;}

    @JsonProperty("totalLostMN")
    private Double totalLostMN;
    public Double getTotalLostMN(){ return totalLostMN;}

    @JsonProperty("unsentConnections")
    private Double unsentConnections;
    public Double getUnsentConnections(){ return unsentConnections;}

}
