package ckafka.data;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.io.Serializable;
import java.util.Set;

public class GroupAdvertisement implements Serializable {

    @JsonCreator
    public GroupAdvertisement(@JsonProperty("nodeID") String NodeID,
                              @JsonProperty("gdID") String GdID,
                              @JsonProperty("listGroups") Set<Integer> ListGroups){

        this.nodeID = NodeID;
        this.gdID = GdID;
        this.listGroups = ListGroups;
    }

    @JsonProperty("nodeID")
    private String nodeID;
    public String getNodeID(){
        return nodeID;
    }
    public void SetNodeID(String NodeID) { this.nodeID = NodeID; }

    @JsonProperty("gdID")
    private String gdID;
    public String getGdID(){
        return gdID;
    }
    public void SetGdID(String GdID) { this.gdID = GdID; }

    @JsonProperty("listGroups")
    private Set<Integer> listGroups;
    public Set<Integer> getListGroups() {
        return listGroups;
    }
    public void setListGroups(Set<Integer> ListGroups) {  this.listGroups = ListGroups; }

    static class ListOfGroups extends JsonDeserializer<Set<Integer>> {

        @Override
        public Set<Integer> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
                throws IOException, JsonProcessingException {
            return null;
        }
    }

}
