package ckafka.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

public class Swap {

    public ObjectMapper mapper;


    public Swap(ObjectMapper Mapper){
        this.mapper = Mapper;

        JavaTimeModule module = new JavaTimeModule();
        this.mapper.registerModule(module);
    }


    public byte[] SwapDataSerialization(SwapData swap) throws Exception {
        try{

            return mapper.writeValueAsBytes(swap);

        }catch (Exception e){
            throw e;
        }
    }


    public SwapData SwapDataDeserialization(byte[] swapDataByte) throws Exception {
        try{

            return mapper.readValue(swapDataByte, SwapData.class);

        }catch (Exception e){
            throw e;
        }
    }


    public byte[] DataSerialization(Object Data) throws Exception {
        try{

            return mapper.writeValueAsBytes(Data);

        }catch (Exception e){
            throw e;
        }
    }


    public Object DataDeserialization(byte[] Data, Class dataClass) throws Exception {
        try{

            return mapper.readValue(Data, dataClass);

        }catch (Exception e){
            throw e;
        }
    }


    public ConnectionReport DataConnectionReportDeserialization(byte[] Data) throws Exception {
        try{

            return mapper.readValue(Data, ConnectionReport.class);

        }catch (Exception e){
            throw e;
        }
    }


    /*public byte[] getContextSerialized(SwapData swap) throws Exception {
        try{

            return mapper.writeValueAsBytes(swap.getContext());

        }catch (Exception e){
            throw e;
        }
    }


    public byte[] getMessageSerialized(SwapData swap) throws Exception {
        try{

            return mapper.writeValueAsBytes(swap.getMessage());

        }catch (Exception e){
            throw e;
        }
    }


    public ObjectNode getObjectNodeFromBytes(byte[] bytes) {
        try{
            if(bytes != null){
                return (ObjectNode) this.mapper.readTree(bytes);
            }else{
                return null;
            }
        }catch (Exception e){
            return null;
        }

    }*/


}
