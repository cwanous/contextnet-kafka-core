package ckafka.data;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class PingConfig implements Serializable {

    @JsonCreator
    public PingConfig(@JsonProperty("frequency") Integer Frequency,
                      @JsonProperty("window") Integer Window){
        this.frequency = Frequency;
        this.window = Window;
    }

    @JsonProperty("frequency")
    private Integer frequency;
    public Integer getFrequency() {
        return frequency;
    }

    @JsonProperty("window")
    private Integer window;
    public Integer getWindow() {
        return window;
    }

}
