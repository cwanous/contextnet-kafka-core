package ckafka.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class SwapTest {

    public static void main(String[] args) {
        // Only one instance needed
        ObjectMapper objectMapper = new ObjectMapper();


        ObjectNode objectContext = objectMapper.createObjectNode();
        objectContext.put("c1",1);

        ObjectNode objectMessage = objectMapper.createObjectNode();
        objectMessage.put("m1", 2);

        SwapData swapTest = new SwapData();

        swapTest.setDuration(30);
        swapTest.setTopic("topicTest");
        //swapTest.setContext(objectContext);

        Swap swap = new Swap(objectMapper);

        try {
            byte[] serializedSwapTest = swap.SwapDataSerialization(swapTest);

            SwapData deserializeSwapTest = swap.SwapDataDeserialization(serializedSwapTest);


        } catch (Exception e) {
            e.printStackTrace();
        }

        // Test case with null fields

    }
}
