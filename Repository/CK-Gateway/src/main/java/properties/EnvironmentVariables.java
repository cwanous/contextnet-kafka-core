package properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EnvironmentVariables {

    /**
     *  Logger
     */
    final Logger logger = LoggerFactory.getLogger(EnvironmentVariables.class);


    private Map<String,String> environmentVariables;


    public EnvironmentVariables(){
        GetEnvironmentVariables();
    }


    private void GetEnvironmentVariables(){
        logger.info("Getting Environment Variables");
        try{
            environmentVariables = System.getenv();
        }catch (Exception e){
            environmentVariables = null;
            logger.error("Error getting Environment Variables", e);
        }
    }


    public Map<String, String> GetEnvironmentVariablesValues(List<String> listOfVariables, String Prefix){
        logger.info("Getting Environment Variables values");
        try {

            Map<String, String> mapOfVariablesValues = new HashMap<>();

            for (String variable : listOfVariables){
                try{

                    String value = environmentVariables.get(Prefix + "." + variable);

                    if(value != null){
                        mapOfVariablesValues.put(variable, value);
                    }else {
                        logger.info("Could not get variable " + variable);
                    }

                }catch (Exception e){
                    logger.info("Could not get variable " + variable, e);
                }
            }

            return mapOfVariablesValues;

        }catch (Exception e){
            logger.error("Error getting Environment Variables values", e);
            return null;
        }
    }


    public String GetEnvironmentVariableValue(String prefix, String variable){
        logger.info("Getting Environment Variable");
        try{
            return environmentVariables.get(prefix + "." + variable);

        }catch (Exception e){
            logger.error("Error getting Environment Variables values" + variable, e);
            return null;
        }
    }


}