package ckafka.core;

import java.util.UUID;

public class CKGatewayServer {

    private static CKGateway gw;

    public static void main(String[] args) {

        UUID id = UUID.randomUUID();
        //UUID id = UUID.fromString("a61ee41e-32d7-48fe-a98c-f8905b7997b6");

        try {
            System.out.println("##### GW Starting #####");

            gw = new CKGateway(id);

            System.out.println("##### GW Started  ##### " + id.toString());

        }catch (Exception e){
            e.printStackTrace();
            gw.close();
        }

    }
}
