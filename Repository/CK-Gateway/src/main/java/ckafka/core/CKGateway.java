package ckafka.core;

import ckafka.core.consumer.ConsumerListener;
import ckafka.core.consumer.KConsumer;
import ckafka.core.data.LoadReportExtractor;
import ckafka.core.handler.GWRejectedExecutionHandler;
import ckafka.core.producer.KProducer;
import ckafka.core.tasks.*;
import ckafka.data.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import lac.cnclib.net.NodeConnection;
import lac.cnclib.net.NodeConnectionListener;
import lac.cnclib.net.NodeConnectionServerListener;
import lac.cnclib.net.extension.ExtendedMessageListener;
import lac.cnclib.net.mrudp.MrUdpNodeConnectionServer;
import lac.cnclib.sddl.message.ApplicationMessage;
import lac.cnclib.sddl.message.Message;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.ByteArrayDeserializer;
import org.apache.kafka.common.serialization.ByteArraySerializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import properties.EnvironmentVariables;

import java.io.IOException;
import java.net.SocketAddress;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;

public class CKGateway implements NodeConnectionServerListener,
        NodeConnectionListener, ExtendedMessageListener, ConsumerListener {

    /**
     *  Logger
     */
    final Logger logger = LoggerFactory.getLogger(CKGateway.class);

    /**
     *  Variables to locate the gateway, its IP address and port number **/
    private static String gatewayIP = "127.0.0.1";
    private static int gatewayPort = 5500;

    /**
     * MR-UDP Server to communicate with nodes
     */
    private MrUdpNodeConnectionServer connectionServer;

    /**
     * Gateways ID used for identify a gateway
     */
    private final UUID gatewayId;

    /**
     * Gateway ThreadPoll
     */
    private int threadPoolCoreSize;
    private GWRejectedExecutionHandler handler;
    private final ScheduledThreadPoolExecutor threadPool;
    private static final int maximumPoolSize = Runtime.getRuntime().availableProcessors() * 2;

    /**
     * Gateway Producer
     */
    private KProducer ckProducer;

    /**
     * Kafka Consumer Server
     */
    private KConsumer ckConsumer;

    private EnvironmentVariables environmentVariables;

    /**
     * Index that stores nodes Connection indexed by Id
     */
    public Map<String, NodeConnection> nodeById;
    public Map<String, Integer> disconnectedNodeById;
    public Map<String, NodeConnection> mtdNodeById;
    public Map<UUID, String> nodeConnectionById;
    private Integer lostMN = 0;

    /**
     * Group Definer Group - Node - GD
     */
    private Table<Integer, String, String> groupTable;

    /**
     * MTD
     */
    public Integer mtdTest = 0;
    private Map<NodeConnection, ConcurrentLinkedQueue<Message>> lastMessagesByNodeConnection;
    private boolean atLeastOnce;
    private Map<NodeConnection, Boolean>  unsentConnections;

    /**
     * Swap Data Handler
     */
    private Swap swap;
    private ObjectMapper objectMapper;
    private String serializeKey;
    private String deserializeKey;
    private String serializeValue;
    private String deserializeValue;

    /**
    * Ping */
    private volatile ScheduledFuture<?> pingTaskFuture;
    private List<Instant> pingList;
    private final Integer pingLock = 1;


    /**
     * Gateways Constructor
     */
    public CKGateway(UUID gatewayId) throws IOException{

        // Serializer and Deserializer
        serializeKey = StringSerializer.class.getName();
        deserializeKey = StringDeserializer.class.getName();
        serializeValue = ByteArraySerializer.class.getName();
        deserializeValue = ByteArrayDeserializer.class.getName();

        environmentVariables = new EnvironmentVariables();

        // Port --- From Env
        try {
            gatewayPort = Integer.parseInt(
                    environmentVariables.GetEnvironmentVariableValue("gw", "port"));
            logger.info("Port " + String.valueOf(gatewayPort));
        }catch (Exception e){
            logger.error("Error getting MR-UDP Port", e);
            System.exit(1);
        }

        // ID
        this.gatewayId = gatewayId;

        // Nodes Data
        this.nodeById = new ConcurrentHashMap<String, NodeConnection>();
        this.disconnectedNodeById = new ConcurrentHashMap<>();
        this.mtdNodeById = new ConcurrentHashMap<String, NodeConnection>();
        this.nodeConnectionById = new ConcurrentHashMap<UUID, String>();
        this.pingList = new ArrayList<Instant>();

        // Group Message
        this.groupTable = HashBasedTable.create();

        // Scheduled Thread Pool
        GetEnvVariables();
        this.handler = new GWRejectedExecutionHandler(this);
        //this.threadPool = new ScheduledThreadPoolExecutor(maximumPoolSize);
        this.threadPool = new ScheduledThreadPoolExecutor(threadPoolCoreSize);
        this.threadPool.setRejectedExecutionHandler(handler);
        this.threadPool.prestartAllCoreThreads();

        // MR-UDP --- Node Connection
        try {
            logger.info("Starting MR-UDP Server");

            this.connectionServer = new MrUdpNodeConnectionServer(gatewayPort);
            this.connectionServer.addListener(this);
            this.connectionServer.start();

        }catch (Exception e){
            logger.error("Error starting MR-UDP Connection", e);
            System.exit(1);
        }


        // Producer
        try{
            logger.info("Starting Producer");
            ckProducer = new KProducer(serializeKey, serializeValue, environmentVariables, "gw.producer");

        }catch (Exception e){
            logger.error("Error starting producer", e);
            System.exit(1);
        }

        // Consumer
        try {
            logger.info("Starting Consumer");

            ckConsumer = new KConsumer(deserializeKey, deserializeValue, environmentVariables, "gw.consumer", this.gatewayId.toString());
            ckConsumer.addConsumerListener(this);
            ckConsumer.start();

        }catch (Exception e){
            logger.error("Error starting consumer", e);
            System.exit(1);
        }

        // MTD Test
        //MTDSendMessageTask mtdTask = new MTDSendMessageTask(this);
        //this.threadPool.scheduleWithFixedDelay(mtdTask, 10000, 10000, TimeUnit.MILLISECONDS);

        // MTD (At Least Once)
        this.lastMessagesByNodeConnection = new ConcurrentHashMap<NodeConnection, ConcurrentLinkedQueue<Message>>();
        this.unsentConnections = new ConcurrentHashMap<NodeConnection, Boolean>();

        // Data Swap
        this.objectMapper = new ObjectMapper();
        this.swap = new Swap(objectMapper);

        // Schedule Send Node's Set Task
        SendNodesSetTask sendNodesSetTask = new SendNodesSetTask(this);
        this.threadPool.scheduleWithFixedDelay(sendNodesSetTask, 3000, 10000, TimeUnit.MILLISECONDS);

        // Schedule Load Report Task
        CreateLoadReportTask createLoadReportTask = new CreateLoadReportTask(this);
        this.threadPool.scheduleWithFixedDelay(createLoadReportTask, 3000, 10000, TimeUnit.MILLISECONDS);

        // Send Hello PoA
        SendHelloPoA();

        // Initial Ping
        // PingConfig initial = new PingConfig(60000, 30000);
        // StartPing(initial);
    }

    private void GetEnvVariables() {
        threadPoolCoreSize = 2000;
        this.atLeastOnce = false;
        try {
            Map<String,String> envVar = System.getenv();
            threadPoolCoreSize = new Integer(envVar.get("gw.poolsize"));

            Integer atLeast = new Integer(envVar.get("gw.atLeast"));
            int one = 1;
            if (atLeast.equals(one)) {
                this.atLeastOnce = true;
            }
        }catch (Exception e){
            logger.info("No Pool Size Informed");
        }
    }


    public void close() {
        logger.info("Closing Kafka Gateway");
        this.connectionServer.removeListener(this);
        this.ckProducer.getProducer().close();
    }


    /**
     * Swap Data
     *
     * **/

    private Message FromSwapToMessage(SwapData Data){
        try {
            ApplicationMessage message = new ApplicationMessage();
            message.setContentObject(Data);
            return message;
        }catch (Exception e){
            logger.error("Error converting Swap Data to Message", e);
            return null;
        }
    }

    public Message FromSwapToMessageWithRecipient(SwapData Data, String NodeID){
        try {
            ApplicationMessage message = new ApplicationMessage();
            message.setRecipientID(UUID.fromString(NodeID));
            message.setContentObject(Data);
            return message;
        }catch (Exception e){
            logger.error("Error converting Swap Data to Message", e);
            return null;
        }
    }

    private SwapData FromRecordToSwapData(ConsumerRecord Record){
        try {
            return swap.SwapDataDeserialization((byte[]) Record.value());
        }catch (Exception e){
            logger.error("Error converting Record to Swap Data", e);
            return null;
        }
    }

    private SwapData FromRecordToSwapData(ProducerRecord Record){
        try {
            return swap.SwapDataDeserialization((byte[]) Record.value());
        }catch (Exception e){
            logger.error("Error converting Record to Swap Data", e);
            return null;
        }
    }

    private SwapData FromMessageToSwapData(Message Message){
        try {
            return (SwapData) Message.getContentObject();
        }catch (Exception e){
            logger.error("Error getting Swap Data from Message", e);
            return null;
        }
    }

    private ProducerRecord FromSwapDataToContextRecord(SwapData Data, String nodeID){
        try {
            return new ProducerRecord<>("GroupReportTopic", nodeID, swap.SwapDataSerialization(Data));
        }catch (Exception e){
            logger.error("Error converting Swap Data to Context Producer-Record", e);
            return null;
        }
    }

    private ProducerRecord FromSwapDataToApplicationRecord(SwapData Data, String nodeID){
        try {
            Data.setContext(null);
            return new ProducerRecord<>(Data.getTopic(), nodeID, swap.SwapDataSerialization(Data));
        }catch (Exception e){
            logger.error("Error converting SwapData to Application Producer-Record", e);
            return null;
        }
    }

    private ProducerRecord FromSwapDataToMTDRecord(SwapData Data, String nodeID){
        try {
            return new ProducerRecord<>("Unsent", nodeID, swap.SwapDataSerialization(Data));
        }catch (Exception e){
            logger.error("Error converting Shippable to MTD Producer-Record", e);
            return null;
        }
    }

    private ProducerRecord GetHelloPoARecord(SwapData Data){
        try {
            return new ProducerRecord<>("HelloPoA", this.gatewayId.toString(), swap.SwapDataSerialization(Data));
        }catch (Exception e){
            logger.error("Error converting Shippable to MTD Producer-Record", e);
            return null;
        }
    }

    private ProducerRecord GetConnectionReportRecord(ConnectionReport Data){
        try {
            return new ProducerRecord<>("ConnectionReport", this.gatewayId.toString(), swap.DataSerialization(Data));
        }catch (Exception e){
            logger.error("Error converting Shippable to ConnectionReport Producer-Record", e);
            return null;
        }
    }

    private ProducerRecord GetLoadReportRecord(LoadReport Data){
        try {
            return new ProducerRecord<>("LoadReport", this.gatewayId.toString(), swap.DataSerialization(Data));
        }catch (Exception e){
            logger.error("Error converting Shippable to Load Report Producer-Record", e);
            return null;
        }
    }

    public ProducerRecord GetNodesSetRecord(){
        try {
            SetData setDataFromNodesSet = CreateSetDataFromNodesSet();

            if(setDataFromNodesSet != null){
                return new ProducerRecord<>("NodesSet", this.gatewayId.toString(), swap.DataSerialization(setDataFromNodesSet));
            }else{
                return null;
            }
        }catch (Exception e){
            logger.error("Error converting Shippable to MTD Producer-Record", e);
            return null;
        }
    }



    /**
     * Send Message from Record to Node
     *
     * **/
    private void sendPrivateMessage(ConsumerRecord record, NodeConnection node) {

        //logger.info("Sending message from Record to Node");

        SwapData swapData = FromRecordToSwapData(record);

        try{
            threadPool.execute(new SendMessageTask(this, node, FromSwapToMessageWithRecipient(swapData, record.key().toString())));
        }catch (Exception e){
            logger.error("Error sending message to Node", e);
        }

    }

    /**
     * Send Message to Node
     *
     * **/
    public void sendPrivateMessage(Message message, NodeConnection node) {

        //logger.info("Sending message to Node");

        try{
            threadPool.execute(new SendMessageTask(this, node, message));
        }catch (Exception e){
            logger.error("Error sending message to Node", e);
        }

    }

    /**
     * Send Message to Group of nodes
     *
     * **/
    private void sendGroupMessage(Message message, Integer group) {
        //logger.info("Sending message to group of nodes");
        try{
            // Getting Nodes (Group - Node - GD)
            Set<String> groupNodes = this.groupTable.row(group).keySet();

            List<NodeConnection> nodeConnectionList = new ArrayList<>();
            for(String nodeId: groupNodes){
                try {
                    NodeConnection connection = this.nodeById.get(nodeId);
                    if(connection != null){
                        nodeConnectionList.add(connection);
                    }
                }catch (Exception e){
                    logger.error("Error getting NodeConnection", e);
                }
            }

            threadPool.execute(new SendMultipleMessagesTask(this, nodeConnectionList, message));
        }catch (Exception e){
            logger.error("Error sending group message", e);
        }
    }

    /**
     * Send Ping Message to nodes
     *
     * **/
    public void SendPingToNodes(){
        //logger.info("Sending Ping to Nodes");
        try{
            SwapData swap = new SwapData();
            swap.setTopic("Ping");
            sendBroadcastMessage(FromSwapToMessage(swap));
        }catch (Exception e){
            logger.error("Error sending Ping to Nodes", e);
        }
    }

    /**
     * Send Broadcast to nodes
     *
     * **/
    private void sendBroadcastMessage(Message message){
        //logger.info("Broadcasting Message to Nodes");
        try{
            this.threadPool.execute(new SendMultipleMessagesTask(this, GetNodesConnectionList(), message));
        }catch (Exception e){
            logger.error("Error broadcasting Message to Nodes ");
        }
    }

    /**
     * Handle Error Sending Message
     *
     * **/
    public void ErrorSendingMessageToNode(Message message, NodeConnection nodeConnection, Exception ex) {
        logger.info("Error sending message to Node " + message.getContentObject().toString(), ex);
        NodeDisconnected(nodeConnection);
        SendUnsentMessageToMTD(message, nodeConnection);
    }

    public void ErrorSendingMessageToNode(Message message, NodeConnection nodeConnection) {
        logger.info("Error sending message to Node " + message.getContentObject().toString());
        NodeDisconnected(nodeConnection);
        SendUnsentMessageToMTD(message, nodeConnection);
    }



    /**
     * NodeConnectionServerListener
     *
     *
     */
    @Override
    public void newNodeConnection(NodeConnection nodeConnection) {

        //logger.info("New Node Connected");
        // add a ConnectionListener
        try {
            nodeConnection.addNodeConnectionListener(this);
        }catch (Exception e){
            logger.error("NodeConnection could not be registered", e);
        }
    }

    @Override
    public void disconnected(NodeConnection nodeConnection) {
        //logger.info("Node disconnected");
        lostMN +=1;
        NodeDisconnected(nodeConnection);

    }

    private void NodeDisconnected(NodeConnection nodeConnection){
        String nodeUnsent = this.nodeConnectionById.get(nodeConnection.getUuid());
        if(nodeUnsent != null){
            this.nodeById.remove(nodeUnsent);
            this.disconnectedNodeById.put(nodeUnsent, 0);
        }
        this.nodeConnectionById.remove(nodeConnection.getUuid());
    }

    /**
     * Message received from a MN
     *
     * {@inheritDoc}
     */
    @Override
    public void newMessageReceived(NodeConnection nodeConnection, Message message) {
        //logger.info("New Message Received: " + Instant.now().toString());
        try {

            String nodeID = message.getSenderID().toString();

            /*
             * If is the first time, I have to insert the keys on indexes
             */
            this.nodeById.put(nodeID, nodeConnection);
            this.mtdNodeById.put(nodeID, nodeConnection);
            this.nodeConnectionById.put(nodeConnection.getUuid(), nodeID);

            SwapData swapData = FromMessageToSwapData(message);

            if(swapData != null){

                if(swapData.getTopic().equals("Ping")){

                    synchronized (pingLock){
                        pingList.add(Instant.now());
                    }

                }else{

                    if(swapData.getContext() != null){
                        ProducerRecord ContextRecord = FromSwapDataToContextRecord(swapData, nodeID);
                        if(ContextRecord != null){
                            //logger.info("Send Context Info");
                            SendRecordToCore(ContextRecord);
                        }
                    }

                    if(swapData.getMessage() != null){
                        ProducerRecord ApplicationRecord = FromSwapDataToApplicationRecord(swapData, nodeID);
                        if(ApplicationRecord != null){
                            //logger.info("Send Application Content");
                            SendRecordToCore(ApplicationRecord);
                        }
                    }

                }

            }

        } catch (Exception e) {
            logger.error("Error processing new message", e);
        }

    }

    /**
     * Called when connection drops and there is unacked sent messages.
     *
     *
     */
    @Override
    public void unsentMessages(NodeConnection nodeConnection, List<Message> list) {
        try{
            //logger.info("Unsent Message caught");
            this.unsentConnections.put(nodeConnection, true);

            if(list.size() > 0){
                // Send to MTD
                for(Message msg: list){
                    ErrorSendingMessageToNode(msg, nodeConnection);
                }
            }else if(AtLeastOnceOn()){
                // Send to MTD Last Messages
                SendLastMessagesToMTD(nodeConnection);
            }

        }catch (Exception e){
            logger.error("Error handling unsent messages", e);
        }
    }

    public boolean AtLeastOnceOn(){
        return this.atLeastOnce;
    }



    /**
     * Send Record To Core
     *
     */
    public void SendRecordToCore(ProducerRecord Record){
        //logger.info("Sending Record to Core");
        try{
            this.threadPool.execute(this.ckProducer.SendTopic(Record));
        }catch (Exception e){
            logger.error("Error sending record to Core", e);
        }
    }

    private void SendMultipleRecordsToCore(List<ProducerRecord> Records){

        //logger.info("Sending Multiple Records to Core");

        try{
            this.threadPool.execute(this.ckProducer.SendMultipleTopics(Records));
        }catch (Exception e){
            logger.error("Error sending multiple record to Core", e);
        }
    }

    private void SendUnsentMessageToMTD(Message message, NodeConnection nodeConnection){
        logger.info("Sending Unsent Message To MTD");
        try {
            //String nodeID = this.nodeConnectionById.get(nodeConnection.getUuid());
            String nodeID = message.getRecipientID().toString();

            if(nodeID != null){
                SwapData swap = FromMessageToSwapData(message);

                SendRecordToCore(FromSwapDataToMTDRecord(swap, nodeID));
            }
        }catch (Exception e){
            //logger.info("Error sending Unsent Message to MTD");
        }
    }

    private void SendHelloPoA(){
        //logger.info("Sending Hello PoA");
        try {
            SwapData helloData = new SwapData();
            SendRecordToCore(GetHelloPoARecord(helloData));
        }catch (Exception e){
            logger.error("Error sending Hello PoA topic", e);
        }
    }

    public void SendConnectionReport(ConnectionReport connectionReport){
        //logger.info("Send Connection Report");
        try{
            ProducerRecord connectionReportRecord = GetConnectionReportRecord(connectionReport);
            SendRecordToCore(connectionReportRecord);
        }catch (Exception e){
            logger.error("Error sending Connection Report", e);
        }
    }

    public void SendLoadReport(LoadReport loadReport){
        //logger.info("Send Load Report");
        try{
            ProducerRecord loadReportRecord = GetLoadReportRecord(loadReport);
            SendRecordToCore(loadReportRecord);
        }catch (Exception e){
            logger.error("Error sending Load Report", e);
        }
    }

    private void SendLastMessagesToMTD(NodeConnection nodeConnection){
        try{
            //logger.info("Send Last Messages to MTD");

            ConcurrentLinkedQueue<Message> messagesQueue = this.lastMessagesByNodeConnection.get(nodeConnection);

            //String nodeID = this.nodeConnectionById.get(nodeConnection.getUuid());

            if(messagesQueue != null){

                List<ProducerRecord> records = new ArrayList<>();

                for(Message message: messagesQueue){
                    String nodeID = message.getRecipientID().toString();
                    SwapData swap = FromMessageToSwapData(message);
                    records.add(FromSwapDataToMTDRecord(swap, nodeID));
                }

                SendMultipleRecordsToCore(records);
            }

        }catch (Exception e){
            logger.error("Error sending last messages to MTD", e);
        }
    }



    /**
     * Beta Kafka Consumer Listener
     *
     * {@inheritDoc}
     */
    @Override
    public void RecordReceived(ConsumerRecord record) {
        //logger.info("Record Received" + record.value().toString());
        try {

            if(record.topic().equals("PrivateMessageTopic") | record.topic().equals("UniCast")){
                HandlePrivateMessageTopic(record, record.key().toString());

            }else if(record.topic().equals("GroupAdvertisement")){
                HandleGroupAdvertisementTopic(record);

            }else if(record.topic().equals("GroupMessageTopic")){
                HandleGroupMessageTopic(record);

            }else if(record.topic().equals("PingConfig")){
                HandlePingConfigTopic(record);

            }else if(record.topic().equals("BroadcastMessageTopic")){
                HandleBroadcastMessageTopic(record);

            }

        }catch (Exception e){
            logger.error("Error receiving Record", e);
        }

    }

    private void HandleGroupMessageTopic(ConsumerRecord Record) {
        //logger.info("Handle Group Message");
        try {
            SwapData swap = FromRecordToSwapData(Record);

            sendGroupMessage(FromSwapToMessage(swap), (Integer.parseInt(Record.key().toString())));

        }catch (Exception e){
         logger.error("Error handling Group Message", e);
        }
    }

    private void HandlePrivateMessageTopic(ConsumerRecord Record, String NodeIn){
        //logger.info("Handle Private Message");
        try {
            NodeConnection node = this.nodeById.get(NodeIn);

            // Send Record as Private Message to Node
            if (node != null){
                sendPrivateMessage(Record, node);
            }else {
                if(this.disconnectedNodeById.containsKey(NodeIn)){
                    SendRecordToCore(FromSwapDataToMTDRecord(FromRecordToSwapData(Record), NodeIn));
                }
            }
        }catch (Exception e){
            logger.error("Error handling Private Message", e);
        }

    }

    private void HandleGroupAdvertisementTopic(ConsumerRecord Record){
        //logger.info("Handle Group Advertisement");
        try {
            String NodeId = Record.key().toString();
            // Verifies if this node is connected to this Gateway
            if(this.nodeById.containsKey(NodeId)){

                GroupAdvertisement advertisement = (GroupAdvertisement)
                        swap.DataDeserialization((byte[]) Record.value(), GroupAdvertisement.class);
                ProcessGroupAdvertisement(advertisement);

            }else{
                //logger.info("Node not connected to GW.");
            }
        } catch (Exception e) {
            logger.error("Error handling Group Advertisement", e);
        }
    }

    private void ProcessGroupAdvertisement(GroupAdvertisement GroupAdv){
        //logger.info("Process Group Advertisement");
        try{
            // Group - Node - GD
            String gdID = GroupAdv.getGdID();
            String nodeID = GroupAdv.getNodeID();
            Set<Integer> newGroups = GroupAdv.getListGroups();

            if(gdID != null && nodeID != null){
                Set<Table.Cell<Integer, String, String>>
                        toDelete = groupTable.cellSet().stream()
                        .filter(cell -> cell.getValue().equals(gdID))
                        .filter(cell -> cell.getColumnKey().equals(nodeID))
                        .collect(Collectors.toSet());

                // Remove
                for(Table.Cell<Integer, String, String> entry : toDelete){
                    Integer oldGroup = entry.getRowKey();

                    // Check if oldGroup is in newGroups
                    if(newGroups.contains(oldGroup)){
                        // if oldGroup is in newGroups remove to the Groups to be added
                        newGroups.remove(oldGroup);
                    }else {
                        // if oldGroup is NOT in newGroups remove from the table
                        synchronized (this){
                            groupTable.remove(oldGroup, entry.getColumnKey());
                        }
                    }
                }

                // Add: Group - Node - GD
                synchronized (this){
                    for(Integer newGroup: newGroups) {
                        groupTable.put(newGroup, nodeID, gdID);
                    }
                }

            }else {
                //logger.info("Empty Group Advertisement");
            }
        }catch (Exception e){
            logger.error("Error Process Group Queue", e);
        }
    }

    private void HandlePingConfigTopic(ConsumerRecord record) {
        //logger.info("Handling Ping Config Topic");
        try {
            if(record.key() != null){
                if(!record.key().toString().equals(this.gatewayId.toString())){
                    //logger.info("Ping Config not to this GW");
                    return;
                }
            }

            PingConfig config = (PingConfig) swap.DataDeserialization((byte[]) record.value(), PingConfig.class);

            StartPing(config);

        }catch (Exception e){
            logger.error("Error Handling Ping Config Topic", e);
        }
    }

    private void StartPing(PingConfig config){
        if(pingTaskFuture != null){
            pingTaskFuture.cancel(true);
        }

        synchronized (pingLock){
            this.pingList.clear();
        }

        PingTask pingTask = new PingTask(this, config.getWindow());
        pingTaskFuture = this.threadPool.scheduleWithFixedDelay(pingTask, 60000, config.getFrequency(), TimeUnit.MILLISECONDS);
    }

    private void HandleBroadcastMessageTopic(ConsumerRecord Record) {
        //logger.info("Handle Broadcast Message Topic");
        try {
            SwapData swap = FromRecordToSwapData(Record);

            sendBroadcastMessage(FromSwapToMessage(swap));

        }catch (Exception e){
            logger.error("Error handling Broadcast Message", e);
        }
    }


    /**
     * Ping Task
     *
     *
     */
    public List<Instant> GetPingList(){
        //logger.info("Getting Ping List");
        try{
            List<Instant> pingsSoFar;

            synchronized (pingLock){
                pingsSoFar = new ArrayList<>(this.pingList);
                this.pingList.clear();
            }

            return pingsSoFar;
        }catch (Exception e){
            logger.error("Error getting Ping List", e);
            return null;
        }
    }

    public Set GetNodesSet(){
        //logger.info("Getting Nodes Set");
        try {
            return nodeById.keySet();

        }catch (Exception e){
            logger.error("Error getting nodes set", e);
            return null;
        }
    }


    /**
     * Load Report Task
     *
     *
     */
    public LoadReport GetLoadReport() {
        //logger.info("Getting Load Report");
        try{

            LoadReportExtractor extractor = new LoadReportExtractor(gatewayId,
                    // Returns the core number of threads.
                    threadPool.getCorePoolSize(),
                    // Returns the approximate number of threads that are actively executing tasks
                    threadPool.getActiveCount(),
                    // Returns Thread Pool Queue Size
                    threadPool.getQueue().size(),
                    this.nodeById.size(),
                    lostMN,
                    this.unsentConnections.size()
            );

            return new LoadReport(
                    extractor.getActivePoolUtilization(),
                    extractor.getCpuLoad(),
                    extractor.getGatewayID(),
                    extractor.getHardwareTotalMemory(),
                    extractor.getJVMFTotalMemory(),
                    extractor.getPoolQueueDimension(),
                    extractor.getPercentageOfAvailableHardwareMemory(),
                    extractor.getPercentageOfAvailableJVMMemory(),
                    extractor.getTotalMN(),
                    extractor.getLostMN(),
                    extractor.getUnsentConn()
            );

        }catch (Exception e){
            logger.error("Error getting Load Report", e);
            return null;
        }
    }


    /**
     * Used to Update the two Last Messages sent by a Node Connection
     *
     * Must be "synchronized" because two thread can not modify the messages-lists simultaneously
     */
    public void UpdateLastMessagesByNodeConnection(NodeConnection nodeConnection, Message message){
        //logger.info("Updating Last Messages Sent");
        try {
            synchronized (nodeConnection){

                ConcurrentLinkedQueue<Message> messagesQueue = this.lastMessagesByNodeConnection.get(nodeConnection);

                if(messagesQueue == null){
                    ConcurrentLinkedQueue<Message> newMessagesQueue = new ConcurrentLinkedQueue<Message>();
                    newMessagesQueue.add(message);
                    this.lastMessagesByNodeConnection.put(nodeConnection, newMessagesQueue);
                }else {
                    messagesQueue.add(message);

                    if(messagesQueue.size() > 2){
                        messagesQueue.poll();
                    }
                    this.lastMessagesByNodeConnection.put(nodeConnection, messagesQueue);

                }

            }
        }catch (Exception e){
            logger.error("Problem Updating Last Messages", e);
        }
    }


    /**
     * Helpers
     *
     *
     */
    private List GetNodesConnectionList(){
        //logger.info("Getting Nodes Set");
        try {
            ArrayList arrayList = new ArrayList(nodeById.values());
            return arrayList;

        }catch (Exception e){
            logger.error("Error getting nodes set", e);
            return null;
        }
    }

    private SetData CreateSetDataFromNodesSet(){
        try {
            //logger.info("Creating SetData from nodes set");

            SetData set = new SetData();

            Set gatewayNodesSet = GetNodesSet();

            if(gatewayNodesSet != null){
                set.setSet(gatewayNodesSet);
                return set;
            }else{
                return null;
            }

        }catch (Exception e){
            logger.error("Error getting nodes set", e);
            return null;
        }
    }






    /**
     * Used only on client side
     *
     * {@inheritDoc}
     */
    @Override
    public void connected(NodeConnection nodeConnection) {

    }
    @Override
    public void reconnected(NodeConnection nodeConnection, SocketAddress socketAddress, boolean b, boolean b1) {

    }
    @Override
    public void internalException(NodeConnection nodeConnection, Exception e) {

    }
    @Override
    public void newProtocolMessageReceived(NodeConnection nodeConnection, Message message) {

    }

}
