package ckafka.core.tasks;

import ckafka.core.CKGateway;
import lac.cnclib.net.NodeConnection;
import lac.cnclib.sddl.message.Message;

public class SendMessageTask implements Runnable {

    /**
     * The Gateways instance
     */
    private final CKGateway gateway;

    /**
     * Vehicle to send the rawMessage
     */
    private final NodeConnection nodeConnection;

    /**
     * A Message to send to the node
     */
    private final Message message;

    /**
     * Constructs the task from Gateways instance, the node and its rawMessage
     *
     * @param gateway Gateways instance used in case of sending error
     * @param nodeConnection The node that should receive the rawMessage
     * @param Message The Message itself
     */
    public SendMessageTask(CKGateway gateway, NodeConnection nodeConnection, Message Message) {
        this.gateway = gateway;
        this.nodeConnection = nodeConnection;
        this.message = Message;
    }

    /**
     * Sends the rawMessage to the node
     *
     *
     * {@inheritDoc}
     */
    @Override
    public void run(){

        try {
            this.nodeConnection.sendMessage(this.message);
        } catch (Exception e) {
            this.gateway.ErrorSendingMessageToNode(this.message, this.nodeConnection, e);
            return;
        }

        if(this.gateway.AtLeastOnceOn()){
            this.gateway.UpdateLastMessagesByNodeConnection(this.nodeConnection, this.message);
            return;
        }else{
            return;
        }

    }

}
