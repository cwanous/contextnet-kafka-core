package ckafka.core.tasks;

import ckafka.core.CKGateway;
import ckafka.data.SwapData;
import lac.cnclib.net.NodeConnection;
import lac.cnclib.sddl.message.Message;

import java.util.Map;

public class MTDSendMessageTask implements Runnable {

    /**
     * The Gateways instance
     */
    private final CKGateway gateway;

    /**
     * Constructs the task from Gateways instance
     *
     * @param gateway Gateways instance used in case of sending error
     */
    public MTDSendMessageTask(CKGateway gateway) {
        this.gateway = gateway;
    }

    /**
     * Sends the MTD message to the node
     *
     *
     * {@inheritDoc}
     */
    @Override
    public void run(){
        this.gateway.mtdTest++;

        SwapData swp = new SwapData();
        swp.setDuration(60);
        swp.setTopic("UniCast");
        byte[] arr = new byte[1];
        arr[0] = this.gateway.mtdTest.byteValue();
        swp.setMessage(arr);

        System.out.println("Sending MTD " + this.gateway.mtdTest.toString());

        for (Map.Entry<String, NodeConnection> entry : gateway.mtdNodeById.entrySet()) {
            Message msg = this.gateway.FromSwapToMessageWithRecipient(swp, entry.getKey());
            NodeConnection v = entry.getValue();
            this.gateway.sendPrivateMessage(msg, v);
        }

    }
}
