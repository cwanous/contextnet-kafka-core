package ckafka.core.tasks;

import ckafka.core.CKGateway;
import lac.cnclib.net.NodeConnection;
import lac.cnclib.sddl.message.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class SendMultipleMessagesTask implements Runnable {

    /**
     *  Logger
     */
    final Logger logger = LoggerFactory.getLogger(SendMultipleMessagesTask.class);

    /**
     * The Gateways instance
     */
    private final CKGateway gateway;

    /**
     * Vehicle to send the rawMessage
     */
    private final List<NodeConnection> nodeConnectionList;

    /**
     * A Message to send to the node
     */
    private final Message message;

    /**
     * Constructs the task from Gateways instance, the node and its rawMessage
     *
     * @param gateway Gateways instance used in case of sending error
     * @param nodeConnectionList The node list that should receive the rawMessage
     * @param Message The Message itself
     */
    public SendMultipleMessagesTask(CKGateway gateway, List<NodeConnection> nodeConnectionList, Message Message) {
        this.gateway = gateway;
        this.nodeConnectionList = nodeConnectionList;
        this.message = Message;
    }

    /**
     * Sends the rawMessage to the node
     *
     *
     * {@inheritDoc}
     */
    @Override
    public void run(){
        logger.info("Sending multiple Messages to Nodes");
        try {

            for(NodeConnection connection: nodeConnectionList){
                connection.sendMessage(message);
            }
            return;

        } catch (Exception e) {
            logger.error("Error sending multiple Messages to Nodes");
            return;
        }

    }

}