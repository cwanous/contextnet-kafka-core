package ckafka.core.tasks;

import ckafka.core.CKGateway;
import ckafka.data.ConnectionReport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.util.List;
import java.util.Set;

public class PingTask implements Runnable {

    /**
     *  Logger
     */
    final Logger logger = LoggerFactory.getLogger(PingTask.class);

    private CKGateway gateway;

    private Integer window;

    private Instant pingBeginTime;

    public PingTask(CKGateway Gateway, Integer Window){
        this.gateway = Gateway;
        this.window = Window;
        this.pingBeginTime = Instant.now();
    }

    @Override
    public void run() {
        logger.info("Starting Ping Task");
        try{

            this.gateway.SendPingToNodes();

            Thread.sleep(this.window);

            List<Instant> pingsSoFar = this.gateway.GetPingList();

            Set nodesSet = this.gateway.GetNodesSet();

            ConnectionReport connectionReport = new ConnectionReport(pingBeginTime, pingsSoFar, nodesSet);

            this.gateway.SendConnectionReport(connectionReport);

            return;

        }catch (Exception e){
            logger.error("Error executing Ping Task");
            return;
        }
    }
}
