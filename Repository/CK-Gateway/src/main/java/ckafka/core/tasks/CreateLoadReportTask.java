package ckafka.core.tasks;

import ckafka.core.CKGateway;
import ckafka.data.LoadReport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CreateLoadReportTask implements Runnable{

    /**
     *  Logger
     */
    final Logger logger = LoggerFactory.getLogger(CreateLoadReportTask.class);

    private CKGateway gateway;

    public CreateLoadReportTask(CKGateway Gateway){
        this.gateway = Gateway;
    }

    @Override
    public void run() {

        LoadReport LoadReport = this.gateway.GetLoadReport();

        if(LoadReport != null){

            this.gateway.SendLoadReport(LoadReport);

        }else{
            logger.info("Could not create Load Report");
            return;
        }

    }
}
