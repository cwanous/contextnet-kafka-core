package ckafka.core.tasks;

import ckafka.core.CKGateway;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GroupNotificationTask implements Runnable{

    /**
     *  Logger
     */
    final Logger logger = LoggerFactory.getLogger(GroupNotificationTask.class);

    private CKGateway gateway;

    public GroupNotificationTask(CKGateway Gateway){
        this.gateway = Gateway;
    }

    @Override
    public void run() {

    }
}
