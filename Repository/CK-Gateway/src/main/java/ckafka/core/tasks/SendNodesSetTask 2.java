package ckafka.core.tasks;

import ckafka.core.CKGateway;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SendNodesSetTask implements Runnable{

    /**
     *  Logger
     */
    final Logger logger = LoggerFactory.getLogger(SendNodesSetTask.class);

    /**
     * The Gateways instance
     */
    private final CKGateway gateway;

    public SendNodesSetTask(CKGateway gateway){

        this.gateway = gateway;
    }

    @Override
    public void run() {
        logger.info("Starting Send Nodes Set Task");

        try{

            ProducerRecord record = this.gateway.CreateNodesSetRecord();

            if(record != null){
                this.gateway.SendRecordToCore(record);
            }

            return;

        }catch (Exception e){
            this.gateway.ErrorSendingNodesSetRecord(e);
            return;
        }

    }
}
