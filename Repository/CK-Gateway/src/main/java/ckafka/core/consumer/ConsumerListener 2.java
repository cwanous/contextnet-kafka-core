package ckafka.core.consumer;

import org.apache.kafka.clients.consumer.ConsumerRecord;

public interface ConsumerListener {

    void RecordReceived(ConsumerRecord Record);
}
