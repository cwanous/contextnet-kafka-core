package ckafka.core.element;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import properties.EnvironmentVariables;

import java.util.*;

public class KafkaElement {

    /**
     *  Logger
     */
    final Logger logger = LoggerFactory.getLogger(KafkaElement.class);

    /**
     *  Map Producer Properties Identifications X Env. Variables (or default)
     */
    protected Map<String, String> mapOfProperties;

    /**
     * Environment Variables
     *
     */

    protected EnvironmentVariables environmentVariables;


    protected Properties properties;


    protected String prefix;


    protected void SetSerializer(String KeyConfig, String KeySerializer,
                                 String ValueConfig, String ValueSerializer){
        logger.info("Setting Serializer");
        try{
            mapOfProperties.put(KeyConfig, KeySerializer);

            mapOfProperties.put(ValueConfig, ValueSerializer);

        }catch (Exception e){
            logger.error("Error Setting Serializer");
            throw e;
        }
    }


    protected void SetPropertiesFromEnvironmentVariables(){

        logger.info("Getting Producer Properties from Environment Variables");

        try{

            // Get Environment Variables Values for Producer Properties (producerVariables)
            List<String> listVariables = new ArrayList<String>();
            listVariables.addAll(this.mapOfProperties.keySet());

            Map<String, String> mapOfEnvironmentVariables =
                    this.environmentVariables.GetEnvironmentVariablesValues(listVariables, this.prefix);

            // Replace Default values of Producer Properties
            if(mapOfEnvironmentVariables != null){
                ReplaceProperties(mapOfEnvironmentVariables);
            }

        }catch (Exception e){
            logger.error("Error getting Producer Properties", e);
        }

    }

    /**
     * Replace Producer Properties Values
     *
     * @pre
     * @post
     */
    protected void ReplaceProperties(Map<String, String> mapOfEnvironmentVariables) {
        logger.info("Replacing Default Producer Properties");
        try{
            for(Map.Entry<String, String> environmentVariable: mapOfEnvironmentVariables.entrySet()){
                try {
                    this.mapOfProperties.put(environmentVariable.getKey(), environmentVariable.getValue());
                }catch (Exception e){
                    logger.error("Error setting property " + environmentVariable.getKey() +
                            "from Environment Variables", e);
                }
            }
        }catch (Exception e){
            logger.info("Error replacing Default Producer Properties", e);
        }
    }


    /**
     * Fill Producer Properties
     *
     *
     */
    protected void FillProperties(){
        logger.info("Filling Producer Properties");
        try{
            for(Map.Entry<String, String> property: this.mapOfProperties.entrySet()){
                try {
                    properties.setProperty(property.getKey(), property.getValue());
                }catch (Exception e){
                    logger.error("Error setting property " + property.getKey(), e);
                }
            }
        }catch (Exception e){
            logger.error("Error filling Producer Properties", e);
            throw e;
        }
    }


    protected void LoadProperties(String KeyConfig, String KeySerializer,
                                  String ValueConfig, String ValueSerializer){
        try{

            SetSerializer(KeyConfig, KeySerializer, ValueConfig, ValueSerializer);

            SetPropertiesFromEnvironmentVariables();

            properties = new Properties();
            FillProperties();

        }catch (Exception e){
            throw e;
        }
    }

    protected List<String> GetTopicsFromEnvironmentVariables(){
        logger.info("Getting Consumer Topics");
        try {

            String stringTopics = this.environmentVariables.GetEnvironmentVariableValue(prefix, "topics");

            String[] stringTopicsSplit = stringTopics.split(",");

            List<String> list = Arrays.asList(stringTopicsSplit);

            return list;

        }catch (Exception e){
            logger.error("Error getting consumer topics", e);
            throw e;
        }
    }


    public KafkaElement(EnvironmentVariables EnvironmentVariables, String Prefix){
        try{

            this.mapOfProperties = new HashMap<>();
            this.environmentVariables = EnvironmentVariables;
            this.prefix = Prefix;

        }catch (Exception e){
            logger.error("Error initiating Kafka Element", e);
            throw e;
        }
    }
}
