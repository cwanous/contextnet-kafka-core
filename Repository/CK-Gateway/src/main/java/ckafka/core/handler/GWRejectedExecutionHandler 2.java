package ckafka.core.handler;

import ckafka.core.CKGateway;

import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;

public class GWRejectedExecutionHandler implements RejectedExecutionHandler {

    private CKGateway gateway;

    public GWRejectedExecutionHandler(CKGateway Gateway){
        this.gateway = Gateway;
    }

    @Override
    public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
        System.out.println("Rejected");
    }
}
