package ckafka.core;

import ckafka.core.consumer.ConsumerListener;
import ckafka.core.consumer.KConsumer;
import ckafka.core.data.LoadReportExtractor;
import ckafka.core.handler.GWRejectedExecutionHandler;
import ckafka.core.producer.KProducer;
import ckafka.core.tasks.*;
import ckafka.data.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import lac.cnclib.net.NodeConnection;
import lac.cnclib.net.NodeConnectionListener;
import lac.cnclib.net.NodeConnectionServerListener;
import lac.cnclib.net.extension.ExtendedMessageListener;
import lac.cnclib.net.mrudp.MrUdpNodeConnectionServer;
import lac.cnclib.sddl.message.ApplicationMessage;
import lac.cnclib.sddl.message.Message;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.ByteArrayDeserializer;
import org.apache.kafka.common.serialization.ByteArraySerializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import properties.EnvironmentVariables;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.SocketAddress;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.*;

public class CKGateway implements NodeConnectionServerListener,
        NodeConnectionListener, ExtendedMessageListener, ConsumerListener {

    /**
     *  Logger
     */
    final Logger logger = LoggerFactory.getLogger(CKGateway.class);

    /**
     *  Variables to locate the gateway, its IP address and port number **/
    private static String gatewayIP = "127.0.0.1";
    private static int gatewayPort = 5500;

    /**
     * MR-UDP Server to communicate with nodes
     */
    private MrUdpNodeConnectionServer connectionServer;

    /**
     * Gateways ID used for identify a gateway
     */
    private final UUID gatewayId;

    /**
     * Gateway ThreadPoll
     */
    private GWRejectedExecutionHandler handler;
    private final ScheduledThreadPoolExecutor threadPool;

    /**
     * Gateway Producer
     */
    private KProducer ckProducer;

    /**
     * Kafka Consumer Server
     */
    private KConsumer ckConsumer;

    private EnvironmentVariables environmentVariables;

    /**
     * Index that stores nodes Connection indexed by Id
     */
    public Map<String, NodeConnection> nodeById;
    public Map<String, NodeConnection> mtdNodeById;
    public Map<UUID, String> nodeConnectionById;

    /**
     * Group Definer GD - Group - Nodes
     */
    private Table<String, Integer, Set<String>> groupsTable;

    /**
     * MTD
     */
    public Integer mtdTest = 0;
    private Map<NodeConnection, ConcurrentLinkedQueue<Message>> lastMessagesByNodeConnection;
    private boolean atLeastOnce;

    /**
     * Swap Data Handler
     */
    private Swap swap;
    private ObjectMapper objectMapper;
    private String serializeKey;
    private String deserializeKey;
    private String serializeValue;
    private String deserializeValue;

    /**
    * Ping */
    private volatile ScheduledFuture<?> pingTaskFuture;
    private List<Instant> pingList;


    /**
     * Gateways Constructor Called from the outside (initialization)
     */
    public CKGateway(UUID gatewayId, String gatewayIP, int gatewayPort) throws IOException {

        this(gatewayId, gatewayIP, gatewayPort, new MrUdpNodeConnectionServer(gatewayPort));

    }

    /**
     * Gateways Constructor
     */
    public CKGateway(UUID gatewayId,
                     String externalRUDPIp,
                     int serverPort,
                     MrUdpNodeConnectionServer mrUdpNodeConnectionServer) {

        // Serializer and Deserializer
        serializeKey = StringSerializer.class.getName();
        deserializeKey = StringDeserializer.class.getName();
        serializeValue = ByteArraySerializer.class.getName();
        deserializeValue = ByteArrayDeserializer.class.getName();


        environmentVariables = new EnvironmentVariables();

        // ID
        this.gatewayId = gatewayId;

        // Nodes Data
        this.nodeById = new HashMap<String, NodeConnection>();
        this.mtdNodeById = new HashMap<String, NodeConnection>();
        this.nodeConnectionById = new HashMap<UUID, String>();
        this.pingList = new ArrayList<Instant>();

        this.groupsTable = HashBasedTable.create();

        // Scheduled Thread Pool
        this.handler = new GWRejectedExecutionHandler(this);
        this.threadPool = new ScheduledThreadPoolExecutor(4);
        this.threadPool.setRejectedExecutionHandler(handler);

        // MR-UDP --- Node Connection
        try {
            logger.info("Starting MR-UDP Server");

            this.connectionServer = mrUdpNodeConnectionServer;
            this.connectionServer.addListener(this);
            this.connectionServer.start();

        }catch (Exception e){
            logger.error("Error starting MR-UDP Connection", e);
            System.exit(1);
        }


        // Producer
        try{
            logger.info("Starting Producer");
            ckProducer = new KProducer(serializeKey, serializeValue, environmentVariables, "gw.producer");

        }catch (Exception e){
            logger.error("Error starting producer", e);
            System.exit(1);
        }

        // Consumer
        try {
            logger.info("Starting Consumer");

            ckConsumer = new KConsumer(deserializeKey, deserializeValue, environmentVariables, "gw.consumer");
            ckConsumer.addConsumerListener(this);
            ckConsumer.start();

        }catch (Exception e){
            logger.error("Error starting consumer", e);
            System.exit(1);
        }

        // MTD Test
        //MTDSendMessageTask mtdTask = new MTDSendMessageTask(this);
        //this.threadPool.scheduleWithFixedDelay(mtdTask, 10000, 10000, TimeUnit.MILLISECONDS);

        // MTD (At Least Once)
        this.lastMessagesByNodeConnection = new ConcurrentHashMap<NodeConnection, ConcurrentLinkedQueue<Message>>();
        this.atLeastOnce = false;

        // Data Swap
        this.objectMapper = new ObjectMapper();
        this.swap = new Swap(objectMapper);

        // Schedule Send Node's Set Task
        SendNodesSetTask sendNodesSetTask = new SendNodesSetTask(this);
        this.threadPool.scheduleWithFixedDelay(sendNodesSetTask, 5000, 20000, TimeUnit.MILLISECONDS);

        // Schedule Load Report Task
        CreateLoadReportTask createLoadReportTask = new CreateLoadReportTask(this);
        this.threadPool.scheduleWithFixedDelay(createLoadReportTask, 10000, 60000, TimeUnit.MILLISECONDS);

        // Send Hello PoA
        SendHelloPoA();
    }


    public void close() {
        logger.info("Closing Kafka Gateway");
        this.connectionServer.removeListener(this);
        this.ckProducer.getProducer().close();
    }

    /**
     * Swap Data
     *
     * **/
    private SwapData FromMessageToSwapData(Message Message){
        try {
            SwapData data = (SwapData) Message.getContentObject();
            return data;
        }catch (Exception e){
            logger.error("Error getting Swap Data from Message", e);
            return null;
        }
    }


    private Message FromSwapToMessage(SwapData Data){
        try {
            ApplicationMessage message = new ApplicationMessage();
            message.setContentObject(Data);
            return message;
        }catch (Exception e){
            logger.error("Error converting Swap Data to Message", e);
            return null;
        }
    }


    public Message FromSwapToMessageWithRecipient(SwapData Data, String NodeID){
        try {
            ApplicationMessage message = new ApplicationMessage();
            message.setRecipientID(UUID.fromString(NodeID));
            message.setContentObject(Data);
            return message;
        }catch (Exception e){
            logger.error("Error converting Swap Data to Message", e);
            return null;
        }
    }


    private SwapData FromRecordToSwapData(ConsumerRecord Record){
        try {
            SwapData swapData = swap.SwapDataDeserialization((byte[]) Record.value());
            return swapData;
        }catch (Exception e){
            logger.error("Error converting Record to Swap Data", e);
            return null;
        }
    }


    private ProducerRecord FromSwapDataToContextRecord(SwapData Data, String nodeID){
        try {
            ProducerRecord<String, byte[]> record
                    = new ProducerRecord<>("GroupReportTopic", nodeID, swap.SwapDataSerialization(Data));
            return record;
        }catch (Exception e){
            logger.error("Error converting Swap Data to Context Producer-Record", e);
            return null;
        }
    }


    private ProducerRecord FromSwapDataToApplicationRecord(SwapData Data, String nodeID){
        try {
            Data.setContext(null);
            ProducerRecord<String, byte[]> record
                    = new ProducerRecord<>(Data.getTopic(), nodeID, swap.SwapDataSerialization(Data));
            return record;
        }catch (Exception e){
            logger.error("Error converting SwapData to Application Producer-Record", e);
            return null;
        }
    }


    private ProducerRecord FromSwapDataToMTDRecord(SwapData Data, String nodeID){
        try {
            ProducerRecord<String, byte[]> record
                    = new ProducerRecord<>("Unsent", nodeID, swap.SwapDataSerialization(Data));
            return record;
        }catch (Exception e){
            logger.error("Error converting Shippable to MTD Producer-Record", e);
            return null;
        }
    }


    private ProducerRecord GetHelloPoARecord(SwapData Data){
        try {
            ProducerRecord<String, byte[]> record
                    = new ProducerRecord<>("HelloPoA", this.gatewayId.toString(), swap.SwapDataSerialization(Data));
            return record;
        }catch (Exception e){
            logger.error("Error converting Shippable to MTD Producer-Record", e);
            return null;
        }
    }


    private ProducerRecord GetConnectionReportRecord(ConnectionReport Data){
        try {
            ProducerRecord<String, byte[]> record
                    = new ProducerRecord<>("ConnectionReport", this.gatewayId.toString(), swap.DataSerialization(Data));
            return record;
        }catch (Exception e){
            logger.error("Error converting Shippable to ConnectionReport Producer-Record", e);
            return null;
        }
    }


    private ProducerRecord GetLoadReportRecord(LoadReport Data){
        try {
            ProducerRecord<String, byte[]> record
                    = new ProducerRecord<>("LoadReport", this.gatewayId.toString(), swap.DataSerialization(Data));
            return record;
        }catch (Exception e){
            logger.error("Error converting Shippable to Load Report Producer-Record", e);
            return null;
        }
    }


    /**
     * Node Connection Server Listener
     *
     * **/
    @Override
    public void newNodeConnection(NodeConnection nodeConnection) {

        logger.info("New Node Connected");
        // add a ConnectionListener
        try {
            nodeConnection.addNodeConnectionListener(this);
        }catch (Exception e){
         logger.error("NodeConnection could not be registered", e);
        }
    }

    /**
     * Send Message from Record to Node
     *
     * **/
    private void sendPrivateMessage(ConsumerRecord record, NodeConnection node) {

        logger.info("Sending message from Record to Node");

        SwapData swapData = FromRecordToSwapData(record);

        try{
            threadPool.execute(new SendMessageTask(this, node, FromSwapToMessageWithRecipient(swapData, record.key().toString())));
        }catch (Exception e){
            logger.error("Error sending message to Node", e);
        }

    }

    /**
     * Send Message to Node
     *
     * **/
    public void sendPrivateMessage(Message message, NodeConnection node) {

        logger.info("Sending message to Node");

        try{
            threadPool.execute(new SendMessageTask(this, node, message));
        }catch (Exception e){
            logger.error("Error sending message to Node", e);
        }

    }

    /**
     * Send Message to Group of nodes
     *
     * **/
    private void sendGroupMessage(Message message, Integer group) {

        logger.info("Sending message to group of nodes");

        // Getting Nodes
        Collection<Set<String>> groups = groupsTable.column(group).values();

        for(Set<String> setOfNodes: groups){
            for(String nodeId: setOfNodes){
                try{
                    message.setRecipientID(UUID.fromString(nodeId));
                    threadPool.execute(new SendMessageTask(this, nodeById.get(nodeId), message));
                }catch (Exception e){
                    logger.error("Error sending message to a specific Node", e);
                }
            }
        }

    }

    /**
     * Handle Error Sending Message
     *
     * **/
    public void ErrorSendingMessageToNode(Message message, NodeConnection nodeConnection, Exception ex) {

        logger.info("Error sending message to Node " + message.getContentObject().toString(), ex);

        SendUnsentMessageToMTD(message, nodeConnection);

    }


    public void SendUnsentMessageToMTD(Message message, NodeConnection nodeConnection){
        try {

            logger.info("Sending Unsent Message To MTD");

            //String nodeID = this.nodeConnectionById.get(nodeConnection.getUuid());
            String nodeID = message.getRecipientID().toString();

            if(nodeID != null){

                SwapData swap = FromMessageToSwapData(message);

                SendRecordToCore(FromSwapDataToMTDRecord(swap, nodeID));

            }

        }catch (Exception e){
            logger.info("Error sending Unsent Message to MTD");
        }
    }

    /**
     * Handle Error Sending Node's Set Record
     *
     * **/
    public void ErrorSendingNodesSetRecord(Exception e) {
        logger.error("Error sending Nodes Set Record", e);
    }

    @Override
    public void disconnected(NodeConnection nodeConnection) {

        logger.info("Node disconnected");

        String nodeUnsent = this.nodeConnectionById.get(nodeConnection.getUuid());

        // Removing Nodes Connection References
        //this.nodeConnectionById.remove(nodeConnection.getUuid());

        NodeConnection nodeConnectionUnsent = this.nodeById.get(nodeUnsent);
        if(nodeConnectionUnsent != null){
            this.nodeById.remove(nodeUnsent);
        }
    }

    /**
     * Message received from a MN
     *
     * {@inheritDoc}
     */
    @Override
    public void newMessageReceived(NodeConnection nodeConnection, Message message) {

        logger.info("New Message Received");

        try {

            String nodeID = message.getSenderID().toString();

            /*
             * If is the first time, I have to insert the keys on indexes
             */
            this.nodeById.put(nodeID, nodeConnection);
            this.mtdNodeById.put(nodeID, nodeConnection);
            this.nodeConnectionById.put(nodeConnection.getUuid(), nodeID);

            SwapData swapData = FromMessageToSwapData(message);

            if(swapData != null){

                if(swapData.getTopic().equals("Ping")){

                    synchronized (this.pingList){
                        pingList.add(Instant.now());
                    }

                }else{

                    if(swapData.getContext() != null){
                        ProducerRecord ContextRecord = FromSwapDataToContextRecord(swapData, nodeID);
                        if(ContextRecord != null){
                            logger.info("Send Context Info");
                            SendRecordToCore(ContextRecord);
                        }
                    }

                    if(swapData.getMessage() != null){
                        ProducerRecord ApplicationRecord = FromSwapDataToApplicationRecord(swapData, nodeID);
                        if(ApplicationRecord != null){
                            logger.info("Send Application Content");
                            SendRecordToCore(ApplicationRecord);
                        }
                    }

                }

            }

        } catch (Exception e) {
            logger.error("Error processing new message", e);
        }

    }

    /**
     * Send Record To Core
     *
     */
    public void SendRecordToCore(ProducerRecord Record){

        logger.info("Sending Record to Core");

        try{
            this.threadPool.execute(this.ckProducer.SendTopic(Record));
        }catch (Exception e){
            logger.error("Error sending record to Core", e);
        }
    }


    public void SendMultipleRecordsToCore(List<ProducerRecord> Records){

        logger.info("Sending Multiple Records to Core");

        try{
            this.threadPool.execute(this.ckProducer.SendMultipleTopics(Records));
        }catch (Exception e){
            logger.error("Error sending multiple record to Core", e);
        }
    }

    /**
     * Called when connection drops and there is unacked sent messages.
     *
     *
     */
    @Override
    public void unsentMessages(NodeConnection nodeConnection, List<Message> list) {
        try{
            logger.info("Unsent Message caught");

            if(list.size() > 0){
                // Send to MTD

            }else if(AtLeastOnceOn()){
                // Send to MTD Last Messages
                SendLastMessagesToMTD(nodeConnection);
            }

        }catch (Exception e){
            logger.error("Error handling unsent messages", e);
        }

    }

    /**
     * Beta Kafka Consumer Listener
     *
     * {@inheritDoc}
     */
    @Override
    public void RecordReceived(ConsumerRecord record) {

        logger.info("Record Received", record.value().toString());

        try {

            if(record.topic().equals("PrivateMessageTopic") | record.topic().equals("UniCast")){
                NodeConnection node = this.nodeById.get(record.key().toString());
                HandlePrivateMessageTopic(record, node);

            }else if(record.topic().equals("GroupAdvertisement")){
                HandleGroupAdvertisementTopic(record);

            }else if(record.topic().equals("GroupMessageTopic")){
                HandleGroupMessageTopic(record);

            }else if(record.topic().equals("PingConfig")){
                HandlePingConfigTopic(record);

            }

        }catch (Exception e){
            logger.error("Error receiving Record", e);
        }

    }


    private void HandleGroupMessageTopic(ConsumerRecord Record) {

        logger.info("Handle Group Message");

        try {
            SwapData swap = FromRecordToSwapData(Record);

            sendGroupMessage(FromSwapToMessage(swap), (Integer) Record.key());

        }catch (Exception e){
         logger.error("Error handling Group Message", e);
        }
    }


    private void HandlePrivateMessageTopic(ConsumerRecord Record, NodeConnection Node){

        logger.info("Handle Private Message");

        try {
            /**
             *  Send Record as Private Message to Node*/
            if (Node != null){
                sendPrivateMessage(Record, Node);
            }else {
                logger.info("Node note connected to GW.");
            }
        }catch (Exception e){
            logger.error("Error handling Private Message", e);
        }

    }


    private void HandleGroupAdvertisementTopic(ConsumerRecord Record){
        logger.info("Handle Group Advertisement");

        try {
            String NodeId = Record.key().toString();
            // Verifies if this node is connected to this Gateway
            if(this.nodeById.containsKey(NodeId)){

                // Get the data from the Group Definer
                GroupAdvertisement advertisement = (GroupAdvertisement) swap.DataDeserialization((byte[]) Record.value(), GroupAdvertisement.class);
                String GDId = advertisement.getGdID();

                if(groupsTable.containsRow(GDId)){
                    Map<Integer, Set<String>> groupDefinerGroups = groupsTable.row(GDId);

                    Set<Integer> setOfGDGroups = groupDefinerGroups.keySet();
                    Set<Integer> setOfNodeNewGroups = advertisement.getListGroups();

                    //setOfGDGroups.removeAll(setOfNodeNewGroups); // add

                    // Remove
                    if(setOfGDGroups.size() > 0){
                        for(Integer groupId: setOfGDGroups){
                            if(groupsTable.contains(GDId, groupId)){
                                Set<String> oldSet = groupsTable.get(GDId, groupId);
                                oldSet.remove(NodeId);
                                groupsTable.put(GDId, groupId, oldSet);
                            }
                        }
                    }


                    // Add
                    if(setOfNodeNewGroups.size() > 0){
                        for(Integer groupId: setOfNodeNewGroups){
                            if(groupsTable.contains(GDId, groupId)){
                                Set<String> oldSet = groupsTable.get(GDId, groupId);
                                oldSet.add(NodeId);
                                groupsTable.put(GDId, groupId, oldSet);
                            }else {
                                HashSet<String> setOfGroupNodes = new HashSet<String>();
                                setOfGroupNodes.add(NodeId);
                                groupsTable.put(GDId, groupId, setOfGroupNodes);
                            }
                        }
                    }

                }else {
                    for(Integer groupId: advertisement.getListGroups()){
                        HashSet<String> setOfGroupNodes = new HashSet<String>();
                        setOfGroupNodes.add(NodeId);
                        groupsTable.put(GDId, groupId, setOfGroupNodes);
                    }
                }

            }
        } catch (Exception e) {
            logger.error("Error handling Group Advertisement", e);
        }

    }

    /**
     * Get Topics from file properties.xml file
     *
     * @pre
     * @post
     */
    private List<String> GetListOfTopicsFromFile() throws Exception {
        try {

            logger.info("Get Topics to Consumer from file");

            String rootPath = Thread.currentThread().getContextClassLoader().getResource("").getPath();
            String mnConfigPath = rootPath + "properties.xml";
            Properties mnProps = new Properties();
            mnProps.loadFromXML(new FileInputStream(mnConfigPath));

            List<String> listOfTopics = Arrays.asList(mnProps.getProperty("topics").split(","));

            return listOfTopics;

        } catch (Exception e){

            logger.error("Error loading properties file", e);
            throw e;
        }
    }

    /**
     * Used only on client side
     *
     * {@inheritDoc}
     */
    @Override
    public void connected(NodeConnection nodeConnection) {

    }

    @Override
    public void reconnected(NodeConnection nodeConnection, SocketAddress socketAddress, boolean b, boolean b1) {

    }

    @Override
    public void internalException(NodeConnection nodeConnection, Exception e) {

    }

    @Override
    public void newProtocolMessageReceived(NodeConnection nodeConnection, Message message) {

    }

    /**
     * Used to Update the two Last Messages sent by a Node Connection
     *
     * Must be "synchronized" because two thread can not modify the messages-lists simultaneously
     */
    public void UpdateLastMessagesByNodeConnection(NodeConnection nodeConnection, Message message){

        logger.info("Updating Last Messages Sent");

        try {

            synchronized (nodeConnection){

                ConcurrentLinkedQueue<Message> messagesQueue = this.lastMessagesByNodeConnection.get(nodeConnection);

                if(messagesQueue == null){
                    ConcurrentLinkedQueue<Message> newMessagesQueue = new ConcurrentLinkedQueue<Message>();
                    newMessagesQueue.add(message);
                    this.lastMessagesByNodeConnection.put(nodeConnection, newMessagesQueue);
                }else {
                    messagesQueue.add(message);

                    if(messagesQueue.size() > 2){
                        messagesQueue.poll();
                    }
                    this.lastMessagesByNodeConnection.put(nodeConnection, messagesQueue);

                }

            }

        }catch (Exception e){
            logger.error("Problem Updating Last Messages", e);
        }

    }


    public boolean AtLeastOnceOn(){
        return this.atLeastOnce;
    }


    private void SendLastMessagesToMTD(NodeConnection nodeConnection){
        try{
            logger.info("Send Last Messages to MTD");

            ConcurrentLinkedQueue<Message> messagesQueue = this.lastMessagesByNodeConnection.get(nodeConnection);

            //String nodeID = this.nodeConnectionById.get(nodeConnection.getUuid());

            if(messagesQueue != null){

                List<ProducerRecord> records = new ArrayList<>();

                for(Message message: messagesQueue){
                    String nodeID = message.getRecipientID().toString();
                    SwapData swap = FromMessageToSwapData(message);
                    records.add(FromSwapDataToMTDRecord(swap, nodeID));
                }

                SendMultipleRecordsToCore(records);
            }

        }catch (Exception e){
            logger.error("Error sending last messages to MTD", e);
        }
    }


    public Set GetNodesSet(){
        try {
            logger.info("Getting Nodes Set");

            return nodeById.keySet();

        }catch (Exception e){
            logger.error("Error getting nodes set", e);
            return null;
        }
    }


    public List GetNodesConnectionList(){
        try {
            logger.info("Getting Nodes Set");

            List<NodeConnection> nodesList = new ArrayList(nodeById.values());

            return nodesList;

        }catch (Exception e){
            logger.error("Error getting nodes set", e);
            return null;
        }
    }


    private SetData CreateSetDataFromNodesSet(){
        try {
            logger.info("Creating SetData from nodes set");

            SetData set = new SetData();

            Set gatewayNodesSet = GetNodesSet();

            if(gatewayNodesSet != null){
                set.setSet(gatewayNodesSet);
                return set;
            }else{
                return null;
            }

        }catch (Exception e){
            logger.error("Error getting nodes set", e);
            return null;
        }
    }


    public ProducerRecord CreateNodesSetRecord(){
        try {

            SetData setDataFromNodesSet = CreateSetDataFromNodesSet();

            if(setDataFromNodesSet != null){
                ProducerRecord<String, byte[]> record
                        = new ProducerRecord<>("NodesSet", this.gatewayId.toString(), swap.DataSerialization(setDataFromNodesSet));
                return record;
            }else{
                return null;
            }

        }catch (Exception e){
            logger.error("Error converting Shippable to MTD Producer-Record", e);
            return null;
        }
    }


    private void SendHelloPoA(){
        logger.info("Sending Hello PoA");

        try {
            SwapData helloData = new SwapData();

            SendRecordToCore(GetHelloPoARecord(helloData));

        }catch (Exception e){
            logger.error("Error sending Hello PoA topic", e);
        }
    }


    private void HandlePingConfigTopic(ConsumerRecord record) {
        logger.info("Handling Ping Config Topic");

        try {
            PingConfig config = (PingConfig) swap.DataDeserialization((byte[]) record.value(), PingConfig.class);

            if(pingTaskFuture != null){
                pingTaskFuture.cancel(true);
            }

            synchronized (this.pingList){
                this.pingList.clear();
            }

            PingTask pingTask = new PingTask(this, config.getWindow());
            pingTaskFuture = this.threadPool.scheduleWithFixedDelay(pingTask, 20000, config.getFrequency(), TimeUnit.MILLISECONDS);

        }catch (Exception e){
            logger.error("Error Handling Ping Config Topic", e);
        }
    }


    public void SendConnectionReport(ConnectionReport connectionReport){
        logger.info("Send Connection Report");
        try{

            ProducerRecord connectionReportRecord = GetConnectionReportRecord(connectionReport);

            SendRecordToCore(connectionReportRecord);

        }catch (Exception e){
            logger.error("Error sending Connection Report", e);
        }
    }


    public void SendLoadReport(LoadReport loadReport){
        logger.info("Send Load Report");
        try{

            ProducerRecord loadReportRecord = GetLoadReportRecord(loadReport);

            SendRecordToCore(loadReportRecord);

        }catch (Exception e){
            logger.error("Error sending Load Report", e);
        }
    }


    public List<Instant> GetPingList(){
        logger.info("Getting Ping List");
        try{

            List<Instant> pingsSoFar = new ArrayList<>();

            synchronized (this.pingList){
                pingsSoFar.addAll(this.pingList);
                this.pingList.clear();
            }

            return pingsSoFar;

        }catch (Exception e){
            logger.error("Error getting Ping List", e);
            return null;
        }
    }


    public void SendPingToNodes(){
        logger.info("Sending Ping to Nodes");
        try{

            SwapData swap = new SwapData();
            swap.setTopic("Ping");

            BroadcastMessageToNodes(FromSwapToMessage(swap));

        }catch (Exception e){
            logger.error("Error sending Ping to Nodes", e);
        }
    }


    public void BroadcastMessageToNodes(Message message){
        logger.info("Broadcasting Message to Nodes");
        try{
            this.threadPool.execute(new SendMultipleMessagesTask(this, GetNodesConnectionList(), message));

        }catch (Exception e){
            logger.error("Error broadcasting Message to Nodes ");
        }
    }


    public LoadReport GetLoadReport() {
        logger.info("Getting Load Report");
        try{

            LoadReportExtractor extractor = new LoadReportExtractor(gatewayId,
                    // Returns the core number of threads.
                    threadPool.getCorePoolSize(),
                    // Returns the approximate number of threads that are actively executing tasks
                    threadPool.getActiveCount(),
                    // Returns Thread Pool Queue Size
                    threadPool.getQueue().size()
            );

            LoadReport loadReport = new LoadReport(
                    extractor.getActivePoolUtilization(),
                    extractor.getCpuLoad(),
                    extractor.getGatewayID(),
                    extractor.getHardwareTotalMemory(),
                    extractor.getJVMFTotalMemory(),
                    extractor.getPoolQueueDimension(),
                    extractor.getPercentageOfAvailableHardwareMemory(),
                    extractor.getPercentageOfAvailableJVMMemory()
            );

             return loadReport;

        }catch (Exception e){
            logger.error("Error getting Load Report", e);
            return null;
        }
    }

}
