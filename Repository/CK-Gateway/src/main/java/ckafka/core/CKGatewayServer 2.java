package ckafka.core;

import java.io.FileInputStream;
import java.util.Properties;
import java.util.UUID;

public class CKGatewayServer {

    private static CKGateway gw;

    /**
     *  Variables to locate the gateway, its IP address and port number **/
    private static String gatewayIP;
    private static int gatewayPort;

    public static void main(String[] args) {

        UUID id = UUID.randomUUID();

        try {
            System.out.println("##### GW Starting #####");

            GetMRUPDProperties();

            gw = new CKGateway(id, gatewayIP, gatewayPort);

            System.out.println("##### GW Started  ##### " + id.toString());

        }catch (Exception e){
            e.printStackTrace();
            gw.close();
        }

    }

    private static void GetMRUPDProperties() {
        try {
            String rootPath = Thread.currentThread().getContextClassLoader().getResource("").getPath();
            String mnConfigPath = rootPath + "properties.xml";
            Properties mnProps = new Properties();
            mnProps.loadFromXML(new FileInputStream(mnConfigPath));

            gatewayIP = mnProps.getProperty("gatewayIP");
            gatewayPort = new Integer(mnProps.getProperty("gatewayPort"));

        }catch (Exception e){
            gatewayIP = "127.0.0.1";
            gatewayPort = 5500;
        }
    }
}
