package ckafka.core.data;

import ckafka.data.LoadReport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import oshi.SystemInfo;
import oshi.hardware.CentralProcessor;
import oshi.hardware.HardwareAbstractionLayer;

import java.util.UUID;

public class LoadReportExtractor {

    final Logger logger = LoggerFactory.getLogger(LoadReport.class);

    private UUID gatewayID;

    private Long hardwareTotalMemory;

    private Long hardwareAvailableMemory;

    private Long JVMFreeMemory;

    private Long JVMFTotalMemory;

    private Double cpuLoad;

    // Returns the core number of threads.
    private Integer corePoolSize;

    // Returns the approximate number of threads that are actively executing tasks
    private Integer activePoolCount;

    // Returns Thread Pool Queue Size
    private Integer queueSize;


    // CPU utilization that occurred while executing at the user level (application).
    private Double user;
    // CPU utilization that occurred while executing at the user level with nice priority.
    private Double nice;
    // CPU utilization that occurred while executing at the system level (kernel).
    private Double sys;
    // Time that the CPU or CPUs were idle and the system did not have an outstanding disk I/O request.
    private Double idle;
    // Time that the CPU or CPUs were idle during which the system had an outstanding disk I/O request.
    private Double ioWait;
    // Time that the CPU used to service hardware IRQs
    private Double irq;
    // Time that the CPU used to service soft IRQs
    private Double softIrq;


    private Double totalMN;
    private Double totalLostMN;
    private Double unsentConn;

    public LoadReportExtractor(UUID GatewayID,
                      Integer CorePoolSize,
                      Integer ActivePoolCount,
                      Integer QueueSize,
                      Integer TotalMN,
                      Integer LostMN,
                      Integer UnsentConn){

        try{
            this.gatewayID = GatewayID;
            this.corePoolSize = CorePoolSize;
            this.activePoolCount = ActivePoolCount;
            this.queueSize = QueueSize;
            this.totalMN = Double.valueOf(TotalMN);
            this.totalLostMN = Double.valueOf(LostMN);
            this.unsentConn = Double.valueOf(UnsentConn);


            SystemInfo si = new SystemInfo();

            HardwareAbstractionLayer hardware = si.getHardware();

            CentralProcessor CPU = hardware.getProcessor();

            GetHardwareInformation(hardware);

            GetJVMInformation();

            GetCPUInformation(CPU);

            logger.info("Load Report Done");

        }catch (Exception e){
            logger.error("Error creating Load Report", e);
        }
    }

    private void GetHardwareInformation(HardwareAbstractionLayer hardware){
        try{
            hardwareTotalMemory = hardware.getMemory().getTotal(); // disk total memory
            hardwareAvailableMemory = hardware.getMemory().getAvailable(); // disk memory available

        }catch (Exception e){
            logger.error("Error getting Hardware Information", e);
            hardwareTotalMemory = null;
            hardwareAvailableMemory = null;
        }
    }


    private void GetCPUInformation(CentralProcessor CPU){
        try{
            long[] previousTicks = CPU.getSystemCpuLoadTicks();
            Thread.sleep(1000);
            long[] currentTicks = CPU.getSystemCpuLoadTicks();

            cpuLoad = CPU.getSystemCpuLoadBetweenTicks(previousTicks);

            long User = 0;
            long Nice = 0;
            long Sys = 0;
            long Idle = 0;
            long IoWait = 0;
            long Irq = 0;
            long SoftIrq = 0;
            long TotalCpu = 0;

            try{
                User = currentTicks[CentralProcessor.TickType.USER.getIndex()] -
                        previousTicks[CentralProcessor.TickType.USER.getIndex()];
            }catch (Exception e){}

            try{
                Nice = currentTicks[CentralProcessor.TickType.NICE.getIndex()] -
                        previousTicks[CentralProcessor.TickType.NICE.getIndex()];
            }catch (Exception e){}

            try{
                Sys = currentTicks[CentralProcessor.TickType.SYSTEM.getIndex()] -
                        previousTicks[CentralProcessor.TickType.SYSTEM.getIndex()];
            }catch (Exception e){}

            try{
                Idle = currentTicks[CentralProcessor.TickType.IDLE.getIndex()] -
                        previousTicks[CentralProcessor.TickType.IDLE.getIndex()];
            }catch (Exception e){}

            try{
                IoWait = currentTicks[CentralProcessor.TickType.IOWAIT.getIndex()] -
                        previousTicks[CentralProcessor.TickType.IOWAIT.getIndex()];
            }catch (Exception e){}

            try{
                Irq = currentTicks[CentralProcessor.TickType.IRQ.getIndex()] -
                        previousTicks[CentralProcessor.TickType.IRQ.getIndex()];
            }catch (Exception e){}

            try{
                SoftIrq = currentTicks[CentralProcessor.TickType.SOFTIRQ.getIndex()] -
                        previousTicks[CentralProcessor.TickType.SOFTIRQ.getIndex()];
            }catch (Exception e){}


            try{
                TotalCpu = User + Nice + Sys + Idle + IoWait + Irq + SoftIrq;
            }catch (Exception e){}


            try{
                user = (double) User/TotalCpu;
            }catch (Exception e){
                user = null;
            }

            try{
                nice = (double) Nice/TotalCpu;
            }catch (Exception e){
                nice = null;
            }

            try{
                sys = (double) Sys/TotalCpu;
            }catch (Exception e){
                sys = null;
            }

            try{
                idle = (double) Idle/TotalCpu;
            }catch (Exception e){
                idle = null;
            }

            try{
                ioWait = (double) IoWait/TotalCpu;
            }catch (Exception e){
                ioWait = null;
            }

            try{
                irq = (double) Irq/TotalCpu;
            }catch (Exception e){
                irq = null;
            }

            try{
                softIrq = (double) SoftIrq/TotalCpu;
            }catch (Exception e){
                softIrq = null;
            }


        }catch (Exception e){
            logger.error("Error getting CPU Information ",e);
        }
    }


    private void GetJVMInformation(){
        try{
            JVMFreeMemory = Runtime.getRuntime().freeMemory(); // JVM free Memory
            JVMFTotalMemory = Runtime.getRuntime().totalMemory();// JVM total Memory

        }catch (Exception e){
            JVMFreeMemory = null;
            JVMFTotalMemory = null;
            logger.error("",e);
        }
    }


    public Double getActivePoolUtilization() {
        try {
            return ((double) activePoolCount)/((double) corePoolSize);
        }catch (Exception e){
            return null;
        }
    }


    public Double getPoolQueueDimension() {
        try {
            return ((double) queueSize)/((double) corePoolSize);
        }catch (Exception e){
            return null;
        }
    }


    public Double getPercentageOfAvailableHardwareMemory(){
        try{
            return (((double) hardwareAvailableMemory)/((double) hardwareTotalMemory));
        }catch (Exception e){
            return null;
        }
    }


    public Double getPercentageOfAvailableJVMMemory(){
        try{
            return (((double) JVMFreeMemory)/((double) JVMFTotalMemory));
        }catch (Exception e){
            return null;
        }
    }


    public UUID getGatewayID(){
        return gatewayID;
    }


    public Long getHardwareTotalMemory() {
        return hardwareTotalMemory;
    }

    public Long getHardwareAvailableMemory() {
        return hardwareAvailableMemory;
    }

    public Long getJVMFreeMemory() {
        return JVMFreeMemory;
    }

    public Long getJVMFTotalMemory() {
        return JVMFTotalMemory;
    }

    public Double getCpuLoad() {
        return cpuLoad;
    }

    public Double getUserCPUPercentage() {
        return user;
    }

    public Double getNiceCPUPercentage() {
        return nice;
    }

    public Double getSysCPUPercentage() {
        return sys;
    }

    public Double getIdleCPUPercentage() {
        return idle;
    }

    public Double getIoWaitCPUPercentage() {
        return ioWait;
    }

    public Double getIrqCPUPercentage() {
        return irq;
    }

    public Double getSoftIrqCPUPercentage() {
        return softIrq;
    }

    public Double getTotalMN() {
        return totalMN;
    }

    public Double getLostMN() {
        return totalLostMN;
    }

    public Double getUnsentConn() {
        return unsentConn;
    }

}
