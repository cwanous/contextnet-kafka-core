package ckafka.core.producer;

import ckafka.core.element.KafkaElement;
import ckafka.core.producer.tasks.SendMultipleTopicsTask;
import ckafka.core.producer.tasks.SendTopicTask;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import properties.EnvironmentVariables;

import java.util.List;

public class KProducer extends KafkaElement {

    /**
     *  Logger
     */
    private final Logger logger = LoggerFactory.getLogger(KProducer.class);

    private KafkaProducer producer;


    public KProducer(String KeySerializer,
                     String ValueSerializer,
                     EnvironmentVariables EnvironmentVariables,
                     String ApplicationPrefix){

        super(EnvironmentVariables, ApplicationPrefix);

        try{

            LoadProducerProperties();

            LoadProperties(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, KeySerializer,
                    ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, ValueSerializer);

            producer = new KafkaProducer<>(properties);

        }catch (Exception e){
            // throw error to notify the user that the producer is not working
            logger.error("Could not initiate Kafka Producer");
            throw e;
        }

    }


    /**
     * List Producer Properties and its Default Values
     *
     *
     */
    private void LoadProducerProperties(){
        logger.info("Load Producer Properties: Default values and Identifications");
        try{

            mapOfProperties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "192.168.0.102:9092");

            mapOfProperties.put(ProducerConfig.ACKS_CONFIG, "all");

            mapOfProperties.put(ProducerConfig.RETRIES_CONFIG, "3");

            mapOfProperties.put(ProducerConfig.LINGER_MS_CONFIG, "1");

            mapOfProperties.put(ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG, "true");

        }catch (Exception e){
            logger.error("Error listing producer properties");
            throw e;
        }
    }


    public KafkaProducer getProducer() {
        return producer;
    }


    public SendTopicTask SendTopic(ProducerRecord Record){
        try {
            logger.info("Sending Topic");

            return new SendTopicTask(producer, Record);
        }catch (Exception e){
            logger.error("Error sending topic: " + Record.toString(), e);
            return null;
        }
    }


    public SendMultipleTopicsTask SendMultipleTopics(List<ProducerRecord> Records){
        try {
            logger.info("Sending Multiple Topics");

            return new SendMultipleTopicsTask(producer, Records);
        }catch (Exception e){
            logger.error("Error sending multiple topics: " + Records.toString(), e);
            return null;
        }
    }

}

