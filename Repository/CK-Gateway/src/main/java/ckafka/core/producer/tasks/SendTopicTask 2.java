package ckafka.core.producer.tasks;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

public class SendTopicTask extends Thread{

    KafkaProducer producer;
    ProducerRecord record;

    public SendTopicTask(KafkaProducer Producer, ProducerRecord Record){
        this.producer = Producer;
        this.record = Record;
    }

    // Send Topic
    @Override
    public void run() {

        try {
            producer.send(record);
            return;
        } catch (Exception e) {
            throw e;
        }
    }
}
