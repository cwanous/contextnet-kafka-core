package tasks;

import ckafka.core.MTD;

public class UnsentMapMaintenanceTask extends Thread {


    private MTD mtd;


    public UnsentMapMaintenanceTask(MTD Mtd){
        this.mtd = Mtd;
    }

    @Override
    public void run() {

        try{
            while (true){

                this.mtd.UpdateUnsentTable();

                this.mtd.DeleteExpiredRecordsFromUnsentMap();

                this.mtd.SendRecordsFromUnsentMap();

                Thread.sleep(5000);
            }
        }catch (Exception e){
            this.mtd.MaintenanceTaskError(e);
        }

        Runtime.getRuntime().addShutdownHook(new Thread( () -> {

        }));
    }


}
