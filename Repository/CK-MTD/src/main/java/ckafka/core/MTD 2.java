package ckafka.core;

import ckafka.connection.ConsumerListener;
import ckafka.data.SetData;
import ckafka.data.Swap;
import ckafka.data.SwapData;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tasks.UnsentMapMaintenanceTask;

import java.io.FileInputStream;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.stream.Collectors;

public class MTD extends CoreElement implements ConsumerListener {

    /**
     *  Logger
     */
    final Logger logger = LoggerFactory.getLogger(MTD.class);

    /**
     *  ThreadPoll
     */
    private final ScheduledThreadPoolExecutor threadPool;

    /**
    *  Message Queue
    */
    ConcurrentLinkedQueue<ConsumerRecord> unsentRecordsQueue;

    /**
     *  Gateway Queue
     */
    ConcurrentLinkedQueue<String> nodesQueue;

    /**
     *  Gateway Nodes Map
     */
    Map<String, Set>  gatewaysNodesMap;

    /**
     * Set Data Handler
     */
    private Swap swap;
    private ObjectMapper objectMapper;

    /**
     * Unsent Records Map
     */
    private Table<String, ConsumerRecord, Instant> unsentTable;

    /**
     * Default Settings
     */
    private Integer defaultDuration;
    private Boolean useOnlyDefaultDuration;


    public static void main(String[] args) {
        new MTD();
    }

    public MTD(){

        super();

        try{
            this.ckConsumer.addConsumerListener(this);
        }catch (Exception e){
            logger.error("Error adding consumer listener");
            System.exit(1);
        }

        threadPool = new ScheduledThreadPoolExecutor(1000);

        unsentRecordsQueue = new ConcurrentLinkedQueue<>();
        nodesQueue = new ConcurrentLinkedQueue<>();

        gatewaysNodesMap = new ConcurrentHashMap<String, Set>();

        unsentTable = HashBasedTable.create();

        // Data Swap
        this.objectMapper = new ObjectMapper();
        this.swap = new Swap(objectMapper);

        // UnsentMapMaintenanceTask
        UnsentMapMaintenanceTask unsentTask = new UnsentMapMaintenanceTask(this);
        unsentTask.start();

        GetDefaultPropertiesFromFile();

        logger.info("MTD Started");
    }

    @Override
    public void RecordReceived(ConsumerRecord<String, String> Record) {
        logger.info("Record Received " + Record.toString());

        try {

            if(Record.topic().equals("Unsent")){
                // Put the Record inside the Queue
                AddRecordToUnsentRecordsQueue(Record);
            }else if(Record.topic().equals("NodesSet")){
                ProcessGatewayNodesSetRecord(Record);
            }

        }catch (Exception ex){
            logger.error("Error processing Record Received", ex);
        }
    }

    /**
     * Record Receive Methods
     */
    public void AddRecordToUnsentRecordsQueue(ConsumerRecord Record){
        try {
            logger.info("Add Record to Unsent-Records-Queue");

            unsentRecordsQueue.add(Record);

        }catch (Exception e){
            logger.error("Error adding Record to Records Queue", e);
        }
    }


    public void ProcessGatewayNodesSetRecord(ConsumerRecord Record){
        try {
            logger.info("Process gateway nodes set record: " + Record.key().toString());

            SetData setData = (SetData) this.swap.DataDeserialization((byte[]) Record.value(), SetData.class);

            if(setData != null){
                Set set = setData.getSet();
                if(set != null){
                    UpdateGatewaysNodesMap(Record.key().toString(), set);
                }
            }


        }catch (Exception e){
            logger.error("Error processing Gateway Record Node Set", e);
        }
    }


    public void UpdateGatewaysNodesMap(String GatewayId, Set newSet){
        try {
            logger.info("Updating gateways nodes map");

            Set oldGatewaySet = gatewaysNodesMap.get(GatewayId);

            Set newNodes = new HashSet();

            newNodes.addAll(newSet);

            if(oldGatewaySet != null){
                newNodes.removeAll(oldGatewaySet);
                newNodes.retainAll(unsentTable.rowKeySet());
                AddNodesToNodesQueue(newNodes);
            }

            gatewaysNodesMap.put(GatewayId, newSet);

        }catch (Exception e){
            logger.error("Error Updating gateways nodes map", e );
        }
    }


    public void AddNodesToNodesQueue(Set newNodes){
        try {
            logger.info("Adding Nodes to Nodes Queue");

            if(newNodes.size() > 0){
                Iterator itr = newNodes.iterator();
                while(itr.hasNext()){
                    nodesQueue.add(itr.next().toString());
                }
            }

        }catch (Exception e){
            logger.error("Adding Nodes to Nodes Queue", e);
        }
    }

    /**
     * INSERT: Unsent Map Maintenance Methods
     */
    public void UpdateUnsentTable(){
        try{
            logger.info("Updating Unsent Map");

            for(int i=0; i<10 ; i++){

                ConsumerRecord record = unsentRecordsQueue.poll();

                if(record != null){

                    InsertRecord(record);

                }else {
                    break;
                }
            }

        }catch (Exception e){
            logger.error("Error updating unsent map", e);
        }
    }


    public void InsertRecord(ConsumerRecord record){
        try{

            logger.info("Inserting Record on Unsent Map");

            Instant finalInstant = GetFinalRecordInstant(record);

            if(finalInstant != null){
                InsertOnUnsentTable(record.key().toString(), record, finalInstant);
            }

        }catch (Exception e){
            logger.error("Error inserting record on Unsent Map", e);
        }
    }


    public void InsertOnUnsentTable(String NodeId, ConsumerRecord Record, Instant finalInstant){
        try{

            logger.info("Insert on Unsent Map");

            unsentTable.put(NodeId, Record, finalInstant);

        }catch (Exception e){
            logger.error("Error inserting on Unsent Map", e);
        }
    }


    public Instant GetFinalRecordInstant(ConsumerRecord record){
        try{

            logger.info("Getting Final Record Instant");

            Instant finalRecordInstant;

            if(!useOnlyDefaultDuration){
                SwapData recordData = FromRecordToSwapData(record);

                if(recordData.getDuration() != null){
                    finalRecordInstant = Instant.now().plusSeconds((long) recordData.getDuration());

                }else {
                    finalRecordInstant = Instant.now().plusSeconds((long) this.defaultDuration);

                }
            }else{
                finalRecordInstant = Instant.now().plusSeconds((long) 120);
            }

            return finalRecordInstant;

        }catch (Exception e){
            logger.error("Error getting final record Instant");
            return null;
        }
    }

    /**
     * DELETE: Unsent Map Maintenance Methods
     */
    public void DeleteExpiredRecordsFromUnsentMap(){
        try{
            logger.info("Deleting Expired Records from Unsent Map");

            Set<Table.Cell<String, ConsumerRecord, Instant>>
                    toDelete = unsentTable.cellSet().stream()
                    .filter(cell -> cell.getValue().isBefore(Instant.now()))
                    .collect(Collectors.toSet());

            DeleteRowsFromUnsentTable(toDelete);

        }catch (Exception e){
            logger.error("Error deleting expired Records from Unsent Map", e);
        }
    }


    public void DeleteRowsFromUnsentTable(Set<Table.Cell<String, ConsumerRecord, Instant>> toDelete){
        try{

            for(Table.Cell<String, ConsumerRecord, Instant> entry : toDelete){
                unsentTable.remove(entry.getRowKey(), entry.getColumnKey());
            }

        }catch (Exception e){
            logger.error("Error Deleting Rows From Unsent Table");
        }
    }


    /**
     * SEND: Unsent Map Maintenance Methods
     */
    public void SendRecordsFromUnsentMap(){
        try{
            logger.info("Sending Records from Unsent Map");

            for(int i=0; i<10 ; i++){

                String nodeID = nodesQueue.poll();

                if(nodeID != null){
                    SendNodeUnsentRecords(nodeID);

                }else {
                    break;
                }
            }

        }catch (Exception e){
            logger.error("Send Records from Unsent Map", e);
        }
    }


    public void SendNodeUnsentRecords(String nodeID){
        try{
            logger.info("Send Node Unsent Records: " + nodeID);

            Map<ConsumerRecord, Instant> nodesRecords = unsentTable.row(nodeID);

            if(nodesRecords != null){
                Set<ConsumerRecord> records = nodesRecords.keySet();

                if(records.size() > 0){
                    List<ProducerRecord> producerRecords = CreateListOfProducerRecords(records);

                    if(producerRecords != null){
                        SendRecordsToCore(producerRecords);
                    }
                }
            }

        }catch (Exception e){
            logger.error("Error sending Node Unsent Records");
        }
    }


    public List CreateListOfProducerRecords(Set<ConsumerRecord> SetOfRecords){
        try{

            List<ProducerRecord> listOfProducerRecords = new ArrayList<>();

            for(ConsumerRecord record : SetOfRecords){

                ProducerRecord producerRecord = FromConsumerToProducerRecord(record);

                if(producerRecord != null){
                    listOfProducerRecords.add(producerRecord);
                }
            }

            return listOfProducerRecords;

        }catch (Exception e){
            logger.error("", e);
            return null;
        }
    }


    public void SendRecordsToCore(List<ProducerRecord> Records){
        try{

            threadPool.execute(ckProducer.SendMultipleTopics(Records));

        }catch (Exception e){
            logger.error("Error Sending Multiple Topics to Core");
        }
    }


    private ProducerRecord FromConsumerToProducerRecord(ConsumerRecord CRecord){
        try {

            ProducerRecord<String, byte[]> PRecord
                    = new ProducerRecord<>("UniCast", (String) CRecord.key(), (byte[]) CRecord.value());
            return PRecord;

        }catch (Exception e){
            logger.error("Error converting Consumer to Producer Record", e);
            return null;
        }
    }


    public void MaintenanceTaskError(Exception e){

    }

    /**
     * Transforms a Record into a Swap Data
     *
     * @pre
     * @post
     */
    private SwapData FromRecordToSwapData(ConsumerRecord Record){
        try {
            SwapData swapData = swap.SwapDataDeserialization((byte[]) Record.value());
            return swapData;
        }catch (Exception e){
            logger.error("Error converting Record to Swap Data", e);
            return null;
        }
    }

    /**
     * Get Default Properties From File
     *
     * @pre
     * @post
     */
    private void GetDefaultPropertiesFromFile() {

        defaultDuration = 120;
        useOnlyDefaultDuration = false;

        try {
            logger.info("Getting Default Properties from File");

            Properties mnProps = GetClassProperties();

            if(mnProps != null){

                try{
                    defaultDuration = Integer.valueOf(mnProps.getProperty("defaultDuration"));
                }catch (Exception e){
                    logger.info("Default Duration not informed in Configuration File");
                }

                try{
                    useOnlyDefaultDuration = Boolean.parseBoolean(mnProps.getProperty("useOnlyDefaultDuration"));
                }catch (Exception e){
                    logger.info("Use Only Default Duration not informed in Configuration File");
                }

            }

        } catch (Exception e){
            logger.info("Error getting default properties from File", e);
        }
    }


    private Properties GetClassProperties(){
        try{
            String rootPath = Thread.currentThread().getContextClassLoader().getResource("").getPath();
            String mnConfigPath = rootPath + "properties.xml";
            Properties mnProps = new Properties();
            mnProps.loadFromXML(new FileInputStream(mnConfigPath));
            return mnProps;
        }catch (Exception e){
            return null;
        }
    }

}
