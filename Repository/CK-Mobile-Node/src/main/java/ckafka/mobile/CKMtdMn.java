package ckafka.mobile;

import ckafka.mobile.data.MessageRegister;
import ckafka.mobile.tasks.SendLocationTask;
import lac.cnclib.net.NodeConnection;
import lac.cnclib.net.mrudp.MrUdpNodeConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class CKMtdMn extends CKMobileNode {

    /**
     *  Variables to locate the gateway, its IP address and port number **/
    private static String gatewayIP = "127.0.0.1";
    private static int gatewayPort = 5500;
    private MrUdpNodeConnection connection;

    /**
     *  Mobile Node UUID
     */
    private final UUID MNId;

    /**
     *  ThreadPoll
     */
    private final ScheduledThreadPoolExecutor threadPool;
    private ScheduledFuture<?> scheduledFutureLocationTask;

    /**
     *  Message Register Map
     */
    public static Map<Integer, MessageRegister> messageRegisterMap;
    private Integer messageCount;

    /**
     *  Logger
     */
    final Logger logger = LoggerFactory.getLogger(CKMobileNode.class);

    public static void main(String[] args) {

        new CKMobileNode();

        // Calls close() to properly close MN method after shut down
        Runtime.getRuntime().addShutdownHook(new Thread( () -> {
            close();
        }));
    }

    /**
     *  Constructor
     */
    public CKMtdMn() {

        MNId = UUID.randomUUID();

        logger.info("Starting CKMobileNode " + MNId.toString());

        InetSocketAddress address = new InetSocketAddress(gatewayIP, gatewayPort);

        // initiates the Scheduled Thread Pool Executor
        this.threadPool = new ScheduledThreadPoolExecutor(20);
        this.threadPool.prestartAllCoreThreads();

        this.messageRegisterMap = new HashMap<Integer, MessageRegister>();
        this.messageCount = 0;

        try {
            // Create a connection object that encapsulate the details and protocol involved in the physical connection with GW
            connection = new MrUdpNodeConnection();
            // Add a listener to the connection object which provides methods to react to the node GW connection
            connection.addNodeConnectionListener(this);
            // Establish the connection
            connection.connect(address);

        } catch (IOException e) {
            logger.error("Unable to connect", e);
            System.exit(1);
        }
    }

    /**
     *  Method called when the mobile node connects with the Gateway
     *
     * @post send location task is scheduled
     */
    @Override
    public void connected(NodeConnection nodeConnection) {
        try{
            final SendLocationTask sendlocationtask = new SendLocationTask(this);
            this.scheduledFutureLocationTask =
                    this.threadPool.scheduleWithFixedDelay(sendlocationtask, 5000, 5000, TimeUnit.MILLISECONDS);

            Thread.sleep(30000);

            nodeConnection.disconnect();

        }catch (Exception e){
            logger.error("Error scheduling SendLocationTask", e);
        }
    }

}
