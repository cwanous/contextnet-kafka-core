package ckafka.mobile.tasks;

import ckafka.mobile.inbound.TestInbound;

public class RegisterTask implements Runnable {

    private TestInbound inbound;
    public RegisterTask(TestInbound Inbound){
        inbound = Inbound;
    }

    @Override
    public void run() {
        inbound.Register();
    }
}
