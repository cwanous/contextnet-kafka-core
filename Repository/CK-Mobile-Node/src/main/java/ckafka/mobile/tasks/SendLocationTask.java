package ckafka.mobile.tasks;

import ckafka.data.SwapData;
import ckafka.mobile.CKMobileNode;
import lac.cnclib.sddl.message.ApplicationMessage;
import lac.cnclib.sddl.message.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * SendLocationTask is a Task for sending messages to the Gateway.
 *
 *
 * @author camilawanous
 */

public class SendLocationTask implements Runnable{

    private CKMobileNode mn;
    private Integer messageCounter;

    final Logger logger = LoggerFactory.getLogger(CKMobileNode.class);

    public SendLocationTask (CKMobileNode MN){
        this.mn = MN;
    }

    @Override
    public void run() {

        logger.info("Run SendLocationTask");

        IncrementMessageCount();

        SwapData newLocation = getNewLocation();

        ApplicationMessage message = this.mn.createDefaultApplicationMessage();

        if(newLocation != null){

            if(message != null){

                message.setContentObject(newLocation);

                SendMessageToGateway(message);

            }else{
                logger.error("Location not sent: Application message could not be created");
            }

        }else{
            logger.warn("New location null");
        }

        return;

    }

    /**
     *  Increment Message Count
     *
     * @post messageCounter incremented
     *
     */
    private synchronized void IncrementMessageCount(){

        logger.info("Increment Message Count");

        try {
            this.messageCounter = this.mn.IncrementMessageCount();
        }catch (Exception e){
            logger.error("Error on message increment", e);
            this.messageCounter = -1;
        }
    }

    /**
     *  Get the Location from the CKMobileNode
     *
     * @post mobile node's location
     *
     */
    private SwapData getNewLocation(){
        try {
            return this.mn.newLocation(this.messageCounter);
        }catch (Exception e){
            logger.error("Error getting new location", e);
            return null;
        }
    }

    /**
     *  Uses Mobile Node method to send MR-UPD Message to Gateway
     *
     * @post message sent to the Gateway
     */
    private void SendMessageToGateway(Message message){
        try {
            this.mn.SendMessageToGateway(message);
        }catch (Exception e){
            logger.error("Error sending message", e);
        }
    }
}
