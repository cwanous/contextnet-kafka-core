package ckafka.mobile.tasks;

import ckafka.mobile.inbound.TestGroup;

public class RegisterOutTask implements Runnable {

    private TestGroup group;
    public RegisterOutTask(TestGroup Group){
        group = Group;
    }

    @Override
    public void run() {
        group.Register();
    }
}
