package ckafka.mobile;

import ckafka.data.ShippableData;
import ckafka.data.SwapData;
import ckafka.mobile.data.MessageRegister;
import ckafka.mobile.tasks.SendLocationTask;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lac.cnclib.net.NodeConnection;
import lac.cnclib.net.NodeConnectionListener;
import lac.cnclib.net.mrudp.MrUdpNodeConnection;
import lac.cnclib.sddl.message.ApplicationMessage;
import lac.cnclib.sddl.message.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

/**
 * CKMobileNode is a mobile node implementation used to test the Kafka-ContextNet core.
 * After shut down returns a register from all messages sent and received.
 *
 * @author camilawanous
 */


public class CKMobileNode implements NodeConnectionListener {


    /**
     *  Variables to locate the gateway, its IP address and port number **/
    private static String gatewayIP = "127.0.0.1";
    private static int gatewayPort = 5500;
    private MrUdpNodeConnection connection;

    /**
     *  Mobile Node UUID
     */
    private final UUID MNId;

    /**
     *  ThreadPoll
     */
    private final ScheduledThreadPoolExecutor threadPool;
    private ScheduledFuture<?> scheduledFutureLocationTask;

    /**
     *  Message Register Map
     */
    public static Map<Integer, MessageRegister> messageRegisterMap;
    private Integer messageCount;

    /**
     *  Logger
     */
    final Logger logger = LoggerFactory.getLogger(CKMobileNode.class);

    /**
     *  Handle JSON Data
     */
    private ObjectMapper objectMapper;

    public static void main(String[] args) {

        new CKMobileNode();

        // Calls close() to properly close MN method after shut down
        Runtime.getRuntime().addShutdownHook(new Thread( () -> {
            close();
        }));
    }

    /**
     *  Constructor
     */
    public CKMobileNode() {

        MNId = UUID.randomUUID();

        logger.info("Starting CKMobileNode " + MNId.toString());

        logger.info("Port: " + gatewayPort);
        InetSocketAddress address = new InetSocketAddress(gatewayIP, gatewayPort);

        // initiates the Scheduled Thread Pool Executor
        this.threadPool = new ScheduledThreadPoolExecutor(20);
        this.threadPool.prestartAllCoreThreads();

        this.messageRegisterMap = new HashMap<Integer, MessageRegister>();
        this.messageCount = 0;

        objectMapper = new ObjectMapper();

        try {
            // Create a connection object that encapsulate the details and protocol involved in the physical connection with GW
            connection = new MrUdpNodeConnection();
            // Add a listener to the connection object which provides methods to react to the node GW connection
            connection.addNodeConnectionListener(this);
            // Establish the connection
            connection.connect(address);

        } catch (IOException e) {
            logger.error("Unable to connect", e);
            System.exit(1);
        }
    }

    public CKMobileNode(String uuid) {

        MNId = UUID.fromString(uuid);

        logger.info("Starting CKMobileNode " + MNId.toString());

        logger.info("Port: " + gatewayPort);
        InetSocketAddress address = new InetSocketAddress(gatewayIP, gatewayPort);

        // initiates the Scheduled Thread Pool Executor
        this.threadPool = new ScheduledThreadPoolExecutor(20);
        this.threadPool.prestartAllCoreThreads();

        this.messageRegisterMap = new HashMap<Integer, MessageRegister>();
        this.messageCount = 0;

        objectMapper = new ObjectMapper();

        try {
            // Create a connection object that encapsulate the details and protocol involved in the physical connection with GW
            connection = new MrUdpNodeConnection();
            // Add a listener to the connection object which provides methods to react to the node GW connection
            connection.addNodeConnectionListener(this);
            // Establish the connection
            connection.connect(address);

        } catch (IOException e) {
            logger.error("Unable to connect", e);
            System.exit(1);
        }
    }


    /**
     *  Get the Location (in simulation it generates a new location)
     *
     * @pre MessageCounter
     * @post ShippableData containing location as Context information
     *
     */
    public SwapData newLocation(Integer MessageCounter) {

        logger.info("Getting new location");

        // creates an empty json {}
        ObjectNode location = objectMapper.createObjectNode();

        // 3 parameters that composes
        Float amountX = ThreadLocalRandom.current().nextFloat();
        Float amountY = ThreadLocalRandom.current().nextFloat();
        Float amountZ = ThreadLocalRandom.current().nextFloat();

        // we write the data to the json document
        location.put("ID", this.MNId.toString());
        location.put("messageCount", MessageCounter);
        location.put("positionX", amountX);
        location.put("positionY", amountY);
        location.put("positionZ", amountZ);

        byte[] messageContent = "Mobile Node Test".getBytes(StandardCharsets.UTF_8);

        try{

            SwapData locationData = new SwapData();
            locationData.setContext(location);
            locationData.setMessage(messageContent);
            locationData.setTopic("Teste");
            locationData.setDuration(60);
            return locationData;

        }catch (Exception e){
            logger.error("Location Swap Data could not be created", e);
            return null;
        }

    }

    /**
     *  Called to Send MR-UPD Message to Gateway
     *
     * @post message sent to the Gateway
     */
    public void SendMessageToGateway(Message message){
        try {
            logger.info("Send new message");
            this.connection.sendMessage(message);
        } catch (IOException e) {
            logger.error("Message could not be sent to Gateway", e);
        }
    }

    /**
     *  Increment the MessageCount by 1 and insert a new MessageRegister in the Register Map
     *
     *  Synchronized to prevent missing entry in Message Register Map
     *
     * @post (MessageCount + 1)
     * @post (RegisterMap with a new entry)
     */
    public synchronized Integer IncrementMessageCount(){

        logger.info("Increment Message Count");

        this.messageCount++;

        try {
            MessageRegister msgRegister = new MessageRegister();
            this.messageRegisterMap.put(this.messageCount, msgRegister);
        }catch (Exception e){
            logger.warn("MessageRegister could not be created or inserted in the Register Map", e);
        }
        return this.messageCount;
    }

    /**
     *  Called to print on terminal the Register Map after the MN shut down
     *
     * @post print Register Map
     */
    public static void close(){
        try {
            System.out.println(messageRegisterMap.toString());
        }catch (Exception e){}
    }

    /**
     *  Method to Register the return of the messages on the Register Map
     */
    private void RegisterMessageReceived(Message message) {
        try {

            logger.info("Register Message Received");

            // instantiate ShippableData from the message content object
            ShippableData data = new ShippableData(message.getContentObject().toString());

            // get the messageCount from the ShippableData context
            Integer messageCount = Integer.parseInt(data.getMessage().get("messageCount").asText());

            // update the MessageRegisterMap with the Register of the message received
            updateRecordOnMessageRegisterMap(messageCount);

        } catch (Exception e) {
            logger.error("Error registering Message Return", e);
        }

    }

    /**
     *  Method to update the Register Map
     *
     * @pre msgCount is a RegisterMap key
     * @post RegisterMap is updated
     */
    public void updateRecordOnMessageRegisterMap(Integer msgCount){
        try {

            logger.info("Update Register Map");

            MessageRegister messageRegister = this.messageRegisterMap.get(msgCount);

            if(messageRegister != null){
                messageRegister.MessageReceived();
                this.messageRegisterMap.put(msgCount, messageRegister);
            }
        }catch (Exception e){
            logger.error("Error updating RegisterMap", e);
        }
    }

    /**
     *  Method called when the mobile node connects with the Gateway
     *
     * @post send location task is scheduled
     */
    @Override
    public void connected(NodeConnection nodeConnection) {
        try{
            logger.info("Connected");
            final SendLocationTask sendlocationtask = new SendLocationTask(this);
            this.scheduledFutureLocationTask =
                    this.threadPool.scheduleWithFixedDelay(sendlocationtask, 5000, 60000, TimeUnit.MILLISECONDS);
        }catch (Exception e){
            logger.error("Error scheduling SendLocationTask", e);
        }
    }

    /**
     *  Method called when the mobile node receives a new message
     *
     * @pre received a valid message
     * @post Message Received is Register
     */
    @Override
    public void newMessageReceived(NodeConnection nodeConnection, Message message) {
        logger.info("New Message Received");
        try{

            SwapData swp = FromMessageToSwapData(message);

            if(swp.getTopic().equals("Ping")){
                message.setSenderID(this.MNId);
                SendMessageToGateway(message);
            }else{
                String str = new String(swp.getMessage(), StandardCharsets.UTF_8);
                logger.info("Message: " + str);
            }

        }catch (Exception e){
            logger.error("Error reading new message received");
        }
    }

    public ApplicationMessage createDefaultApplicationMessage(){
        try {
            ApplicationMessage message = new ApplicationMessage();
            message.setSenderID(this.MNId);
            return message;
        }catch (Exception e){
            logger.error("Error creating default Application Message", e);
            return null;
        }
    }

    @Override
    public void reconnected(NodeConnection nodeConnection, SocketAddress socketAddress, boolean b, boolean b1) {
    }

    @Override
    public void disconnected(NodeConnection nodeConnection) {
    }

    @Override
    public void unsentMessages(NodeConnection nodeConnection, List<Message> list) {
    }

    @Override
    public void internalException(NodeConnection nodeConnection, Exception e) {
    }

    /**
     * Swap Data
     *
     * **/
    private SwapData FromMessageToSwapData(Message Message){
        try {
            SwapData data = (SwapData) Message.getContentObject();
            return data;
        }catch (Exception e){
            logger.error("Error getting Swap Data from Message", e);
            return null;
        }
    }
}