package ckafka.mobile.data;

import java.time.Instant;

/**
 * MessageRegister is data structure made to register send and received messages time
 *
 *
 * @author camilawanous
 */

public class MessageRegister {

    private Instant sendInstant;
    private Instant receiveInstant;

    public MessageRegister(){
        Instant now = Instant.now();
        this.sendInstant = now;
    }

    public void MessageReceived(){
        Instant now = Instant.now();
        this.receiveInstant = now;
    }

    @Override
    public String toString(){
        if(this.receiveInstant != null){
            return "SEND: " + this.sendInstant.toString() + " - RECEIVED: " + this.receiveInstant.toString();
        }else{
            return "SEND: " + this.sendInstant.toString();
        }
    }
}
