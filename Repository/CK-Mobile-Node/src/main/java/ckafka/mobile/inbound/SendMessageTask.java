package ckafka.mobile.inbound;

import lac.cnclib.sddl.message.ApplicationMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SendMessageTask implements Runnable{

    private MNInbound mn;
    private MNInboundTask mnt;

    final Logger logger = LoggerFactory.getLogger(SendMessageTask.class);

    public SendMessageTask (MNInbound MN){
        this.mn = MN;
    }

    public SendMessageTask (MNInboundTask MN){
        this.mnt = MN;
    }

    @Override
    public void run() {
        try {
            int count = mnt.IncrementCount();

            ApplicationMessage msg = mnt.createMessage(count);

            if(msg != null){
                mnt.Register(count);
                mnt.SendMessageToGateway(msg);
            }
        }catch (Exception e){
            logger.error("Error Send Message Task", e);
        }
    }
}
