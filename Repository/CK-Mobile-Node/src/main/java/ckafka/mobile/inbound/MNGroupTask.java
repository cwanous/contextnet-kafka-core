package ckafka.mobile.inbound;

import ckafka.data.SwapData;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lac.cnclib.net.NodeConnection;
import lac.cnclib.net.NodeConnectionListener;
import lac.cnclib.net.mrudp.MrUdpNodeConnection;
import lac.cnclib.sddl.message.ApplicationMessage;
import lac.cnclib.sddl.message.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;


public class MNGroupTask implements NodeConnectionListener, Runnable {

    final Logger logger = LoggerFactory.getLogger(MNGroupTask.class);

    private final UUID Id;

    /**
     *  Variables to locate the gateway, its IP address and port number **/
    private String gatewayIP;
    private int gatewayPort;
    private MrUdpNodeConnection connection;

    private volatile ScheduledFuture<?> sendMessageTask;
    private int MessageInterval;
    private int MessageRate;
    private String AppTopic;

    private final ScheduledThreadPoolExecutor threadPool;

    /**
     *  Message Register Map
     */
    public Map<Integer, Instant> sendMsgMap;
    public Map<Integer, Instant> receiveMsgMap;

    private Integer messageCount;
    private Boolean register = false;

    /**
     *  Handle JSON Data
     */
    private ObjectMapper objectMapper;


    /**
     *  Report Data
     */
    public Map<Integer, Long> DurationMap;
    public Integer TotalMessages;
    public Integer UnansweredMessages;

    private Boolean run = true;

    private AtomicInteger groupValue;
    private String position = "position";
    private Integer ReceivedMessages = 0;
    private Integer ChangeInterval = 0;

    private Map<Integer, Integer> MsgReceived;
    private Map<Integer, Instant> TimeReceived;
    public HashSet<Integer> registred;

    public MNGroupTask(int port,
                       String ipAddress,
                       int msgInterval,
                       String appTopic,
                       int changeInterval) {

        Id = UUID.randomUUID();
        groupValue = new AtomicInteger();
        groupValue.set(0);
        ChangeInterval = changeInterval;

        MsgReceived = new ConcurrentHashMap<Integer, Integer>();
        TimeReceived = new ConcurrentHashMap<Integer, Instant>();
        registred = new HashSet<>();

        gatewayPort = port;
        gatewayIP = ipAddress;
        MessageInterval = msgInterval;
        AppTopic = appTopic;

        threadPool = new ScheduledThreadPoolExecutor(4);
        threadPool.prestartAllCoreThreads();

        objectMapper = new ObjectMapper();
    }

    public String BuildReport(){
        try {
            HashMap<Integer, Integer> ExposeMap
                    = new HashMap<Integer, Integer>();
            HashMap<Integer, Instant> Times = new HashMap<>();


            MsgReceived.keySet().forEach(
                    (Integer count) -> {
                        if(!registred.contains(count)){
                            ExposeMap.put(count, MsgReceived.get(count));
                            registred.add(count);
                            if(TimeReceived.containsKey(count)){
                                Times.put(count, TimeReceived.get(count));
                            }
                        }
                    }
            );
            return Id.toString() + ";" + ReceivedMessages.toString()+ ";" + ExposeMap.toString() + ";" + Times.toString();

        }catch (Exception ignore){ return Id.toString(); }
    }


    public void Register(){
        register = true;
    }

    // MR-UDP Methods
    @Override
    public void connected(NodeConnection nodeConnection) {
        logger.info("Connected");
        try{
            logger.info("Connected");
            final SendGDMessageTask sendTask = new SendGDMessageTask(this);
            sendMessageTask = this.threadPool.scheduleWithFixedDelay(sendTask, 1000, MessageInterval, TimeUnit.MILLISECONDS);
        }catch (Exception e){
            logger.error("Error scheduling SendLocationTask", e);
        }
    }

    @Override
    public void reconnected(NodeConnection nodeConnection, SocketAddress socketAddress, boolean b, boolean b1) {

    }

    @Override
    public void disconnected(NodeConnection nodeConnection) {

    }

    @Override
    public void newMessageReceived(NodeConnection nodeConnection, Message message) {
        try{
            Instant now = Instant.now();
            SwapData swp = (SwapData) message.getContentObject();
            String topic = swp.getTopic();
            if(topic.equals("Ping")){
                message.setSenderID(this.Id);
                SendMessageToGateway(message);
            }else {
                int groupNow = groupValue.get();
                final SendGDMessageTask sendTask = new SendGDMessageTask(this, createMessage(swp));
                this.threadPool.execute(sendTask);
                RegisterMsg(groupNow, swp, now);
            }

        }catch (Exception e){
            logger.error("Error reading new message received");
        }
    }

    private void RegisterMsg(int group, SwapData swp, Instant now){
        if(register){
            ReceivedMessages++;
            Integer msgId = getCounterAsInteger(swp.getMessage());
            MsgReceived.putIfAbsent(msgId, group);
            TimeReceived.putIfAbsent(msgId, now);
        }
    }


    private ApplicationMessage createMessage(SwapData swp){
        try {
            ApplicationMessage message = new ApplicationMessage();
            message.setContentObject(CreateContent(swp));
            message.setSenderID(Id);
            return message;
        }catch (Exception e){
            logger.error("Error creating message", e);
            return null;
        }
    }

    private SwapData CreateContent(SwapData swp) {
        SwapData content = new SwapData();
        content.setContext(GetContextObject());
        content.setMessage(swp.getMessage());
        content.setTopic(AppTopic);
        content.setDuration(60);
        return content;
    }

    ApplicationMessage createMessage(){
        try {
            ApplicationMessage message = new ApplicationMessage();
            message.setContentObject(CreateContent());
            message.setSenderID(Id);
            return message;
        }catch (Exception e){
            logger.error("Error creating message", e);
            return null;
        }
    }

    private SwapData CreateContent() {
        SwapData content = new SwapData();
        content.setContext(GetContextObject());
        content.setDuration(60);
        return content;
    }




    // Counter Manipulation
    private byte[] getCounterAsByte(Integer counter){
        return counter.toString().getBytes(StandardCharsets.UTF_8);
    }

    private Integer getCounterAsInteger(byte[] counter){
        return new Integer (new String(counter, StandardCharsets.UTF_8));
    }


    // Send MR-UPD Message to Gateway
    void SendMessageToGateway(Message message){
        try {
            //logger.info("Send new message");
            this.connection.sendMessage(message);
        } catch (Exception e) {
            logger.error("Message could not be sent to Gateway", e);
            run = false;
        }
    }


    private void Disconnect(){
        try {
            logger.info("Disconnect");
            this.connection.disconnect();
        } catch (Exception e) {
            logger.error("Could not Disconnect", e);
        }
    }



    @Override
    public void run() {

        InetSocketAddress address = new InetSocketAddress(gatewayIP, gatewayPort);
        try {
            // Create a connection object that encapsulate the details and protocol involved in the physical connection with GW
            connection = new MrUdpNodeConnection();
            // Add a listener to the connection object which provides methods to react to the node GW connection
            connection.addNodeConnectionListener(this);
            // Establish the connection
            connection.connect(address);
        } catch (IOException e) {
            logger.error("Unable to connect", e);
            logger.info("Stop");
            return;
        }

        while (run){
            try {
                if(ChangeInterval > 0){
                    Thread.sleep(ChangeInterval);
                    ChangeGroup();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        try {
            this.connection.disconnect();
            this.connection.getSocket().close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        threadPool.shutdown();
        logger.info("Stop");
    }


    private void ChangeGroup() {
        if(ChangeInterval > 0){
            int gvNow = groupValue.get();
            if(gvNow == 0){
                groupValue.set(1);
            }else {
                groupValue.set(0);
            }
        }
    }

    private void ChangeGroup2() {
        if(ChangeInterval > 0){
            int gvNow = groupValue.get();
            if(gvNow == 0){
                groupValue.set(1);
            }else if(gvNow == 1) {
                groupValue.set(2);
            }else {
                groupValue.set(0);
            }
        }
    }


    private ObjectNode GetContextObject(){
        ObjectNode context = objectMapper.createObjectNode();
        context.put(position, groupValue.get());
        return context;
    }

    @Override
    public void unsentMessages(NodeConnection nodeConnection, List<Message> list) {

    }
    @Override
    public void internalException(NodeConnection nodeConnection, Exception e) {

    }

}

