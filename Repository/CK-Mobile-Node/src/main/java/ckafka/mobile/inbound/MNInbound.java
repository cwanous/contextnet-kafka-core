package ckafka.mobile.inbound;

import ckafka.data.SwapData;
import ckafka.mobile.data.MessageRegister;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lac.cnclib.net.NodeConnection;
import lac.cnclib.net.NodeConnectionListener;
import lac.cnclib.net.mrudp.MrUdpNodeConnection;
import lac.cnclib.sddl.message.ApplicationMessage;
import lac.cnclib.sddl.message.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class MNInbound implements NodeConnectionListener {

    final Logger logger = LoggerFactory.getLogger(MNInbound.class);

    private final UUID Id;

    /**
     *  Variables to locate the gateway, its IP address and port number **/
    private String gatewayIP;
    private int gatewayPort;
    private MrUdpNodeConnection connection;

    private int MessageInterval;
    private String AppTopic;

    private final ScheduledThreadPoolExecutor threadPool;

    /**
     *  Message Register Map
     */
    public Map<Integer, Instant> sendMsgMap;
    public Map<Integer, Instant> receiveMsgMap;
    public HashSet<Integer> registred;
    private Integer messageCount;

    /**
     *  Handle JSON Data
     */
    private ObjectMapper objectMapper;


    /**
     *  Report Data
     */
    public Map<Integer, Long> DurationMap;
    public Integer TotalMessages;
    public Integer UnansweredMessages;

    MNInbound(int port,
              String ipAddress,
              int msgInterval,
              String appTopic) {

        Id = UUID.randomUUID();
        //logger.info("Starting MNInbound " + Id.toString());

        gatewayPort = port;
        gatewayIP = ipAddress;
        MessageInterval = msgInterval;
        AppTopic = appTopic;

        InetSocketAddress address = new InetSocketAddress(gatewayIP, gatewayPort);

        threadPool = new ScheduledThreadPoolExecutor(5);
        threadPool.prestartAllCoreThreads();

        sendMsgMap = new ConcurrentHashMap<>();
        receiveMsgMap = new ConcurrentHashMap<>();
        registred = new HashSet<>();
        messageCount = 0;
        UnansweredMessages = 0;

        objectMapper = new ObjectMapper();

        try {
            // Create a connection object that encapsulate the details and protocol involved in the physical connection with GW
            connection = new MrUdpNodeConnection();
            // Add a listener to the connection object which provides methods to react to the node GW connection
            connection.addNodeConnectionListener(this);
            // Establish the connection
            connection.connect(address);

        } catch (IOException e) {
            logger.error("Unable to connect", e);
        }
    }


    public String BuildReport(){
        try {
            DurationMap = new HashMap<>();

            TotalMessages = sendMsgMap.keySet().size();

            UnansweredMessages = TotalMessages - receiveMsgMap.keySet().size();

            sendMsgMap.keySet().forEach(
                    (Integer count) -> {
                        if(!registred.contains(count)){
                            if(receiveMsgMap.containsKey(count)){
                                Long roundTrip =
                                        Duration.between(sendMsgMap.get(count), receiveMsgMap.get(count)).toMillis();
                                DurationMap.put(count, roundTrip);
                                registred.add(count);
                            }
                        }
                    }
            );

            return Id.toString() + ";" + UnansweredMessages + ";" + DurationMap.toString();

        }catch (Exception ignore){ return Id.toString(); }
    }

    public MNData CleanRegister(){
        try {
            MNData data = new MNData(Id, sendMsgMap, receiveMsgMap);
            sendMsgMap.clear();
            receiveMsgMap.clear();
            return data;
        }catch (Exception e){
            logger.error("Clould not Clean Register", e);
        }
        return null;
    }



    // MR-UDP Methods
    @Override
    public void connected(NodeConnection nodeConnection) {
        logger.info("Connected");
        try{
            logger.info("Connected");
            final SendMessageTask sendTask = new SendMessageTask(this);
            this.threadPool.scheduleWithFixedDelay(sendTask, 1000, MessageInterval, TimeUnit.MILLISECONDS);
        }catch (Exception e){
            logger.error("Error scheduling SendLocationTask", e);
        }
    }

    @Override
    public void reconnected(NodeConnection nodeConnection, SocketAddress socketAddress, boolean b, boolean b1) {

    }

    @Override
    public void disconnected(NodeConnection nodeConnection) {

    }

    @Override
    public void newMessageReceived(NodeConnection nodeConnection, Message message) {
        //logger.info("New Message Received");
        try{

            SwapData swp = (SwapData) message.getContentObject();
            String topic = swp.getTopic();

            if(topic.equals(AppTopic)){
                RegisterMessage(swp);
            }

        }catch (Exception e){
            logger.error("Error reading new message received");
        }
    }


    // Message Send Task
    ApplicationMessage createMessage(){
        //logger.info("Create Message");
        try {
            int count = IncrementCount();

            ApplicationMessage message = new ApplicationMessage();
            message.setSenderID(Id);

            message.setContentObject(CreateContent(count));

            //Register
            if(sendMsgMap.containsKey(count)){
                // Register already used
                return null;
            }
            MessageRegister msgRegister = new MessageRegister();
            sendMsgMap.put(count, Instant.now());

            return message;
        }catch (Exception e){
            logger.error("Error creating message", e);
            return null;
        }
    }

    private synchronized int IncrementCount(){
        messageCount++;
        return messageCount;
    }

    private SwapData CreateContent(Integer MessageCounter) {
        ObjectNode context = objectMapper.createObjectNode();

        byte[] messageContent = getCounterAsByte(MessageCounter);

        SwapData content = new SwapData();
        content.setContext(context);
        content.setMessage(messageContent);
        content.setTopic(AppTopic);
        content.setDuration(60);
        return content;
    }


    // Register Return Message
    private void RegisterMessage(SwapData swp){
        //logger.info("Register Message");
        try {
            int counter = getCounterAsInteger(swp.getMessage());
            if(!receiveMsgMap.containsKey(counter)){
                receiveMsgMap.put(counter, Instant.now());
            }
        } catch (Exception e) {
            logger.error("Error Register Message", e);
        }
    }


    // Counter Manipulation
    private byte[] getCounterAsByte(Integer counter){
        return counter.toString().getBytes(StandardCharsets.UTF_8);
    }

    private Integer getCounterAsInteger(byte[] counter){
        return new Integer (new String(counter, StandardCharsets.UTF_8));
    }


    // Send MR-UPD Message to Gateway
    void SendMessageToGateway(Message message){
        try {
            //logger.info("Send new message");
            this.connection.sendMessage(message);
        } catch (Exception e) {
            logger.error("Message could not be sent to Gateway", e);
            Finalize();
        }
    }

    private void Finalize()  {
        try {
            logger.info("Finalize");
            super.finalize();
        } catch (Exception e) {
            logger.error("Could not Finalize", e);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    private void Disconnect(){
        try {
            logger.info("Disconnect");
            this.connection.disconnect();
        } catch (Exception e) {
            logger.error("Could not Disconnect", e);
        }
    }


    @Override
    public void unsentMessages(NodeConnection nodeConnection, List<Message> list) {

    }
    @Override
    public void internalException(NodeConnection nodeConnection, Exception e) {

    }

}
