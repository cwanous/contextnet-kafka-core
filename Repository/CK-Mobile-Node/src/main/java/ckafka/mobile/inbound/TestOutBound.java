package ckafka.mobile.inbound;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class TestOutBound {

    final Logger logger = LoggerFactory.getLogger(TestOutBound.class);

    public static ArrayList<MNOutboundTask> ListMNT;

    private Map<String,String> envVariables;

    private static Integer interval;        // mn.inbound.interval
    private static Integer nmQtd;        // mn.inbound.nmQtd

    private static Integer port;        // mn.inbound.port
    private static String idAddress;    // mn.inbound.idAddress
    private static Integer msgInterval; // mn.inbound.msgInterval
    private static Integer msgRate; // mn.inbound.msgRate
    private static String topic;        // mn.inbound.topic

    private static Integer timeOff;        // mn.inbound.port
    private static Integer probability;
    private static Integer percDisconnected;

    private FileWriter writer;
    private static String file;        // mn.inbound.file

    private static ScheduledThreadPoolExecutor threadPool;

    public static void main(String[] args) {
        new TestOutBound();
    }

    private TestOutBound(){
        logger.info("Test OutBound");
        ListMNT = new ArrayList<>();

        //GetLocalVariables();
        GetEnvVariables();

        threadPool = new ScheduledThreadPoolExecutor(nmQtd+10);
        //threadPool.prestartAllCoreThreads();

        AddMultipleMNTask();

        StartRegister();

        RegisterGroupTask register = new RegisterGroupTask(this);
        threadPool.scheduleWithFixedDelay(register, 10, 180, TimeUnit.SECONDS);
        logger.info("SetUp Outbound End");
    }

    private void StartRegister() {
        logger.info("Start Register");
        for (MNOutboundTask mn : TestOutBound.ListMNT) {
            mn.Register();
        }
    }

    private void GetEnvVariables() {
        envVariables = System.getenv();

        interval = new Integer(envVariables.get("mn.outbound.interval"));

        nmQtd = new Integer(envVariables.get("mn.outbound.nmQtd"));

        port = new Integer(envVariables.get("mn.outbound.port"));
        idAddress = envVariables.get("mn.outbound.idAddress");
        msgInterval = new Integer(envVariables.get("mn.outbound.msgInterval"));
        msgRate = new Integer(envVariables.get("mn.outbound.msgRate"));
        topic = envVariables.get("mn.outbound.topic");

        file = envVariables.get("mn.outbound.file");

        probability = new Integer(envVariables.get("mn.outbound.probability"));
        timeOff = new Integer(envVariables.get("mn.outbound.timeOff"));
        percDisconnected = new Integer(envVariables.get("mn.outbound.percDisconnected"));
    }

    private void GetLocalVariables() {
        envVariables = System.getenv();

        interval = 40000;
        nmQtd = 1;
        port = 5500;
        idAddress = "127.0.0.1";
        topic = "Teste";
    }

    private static void AddMultipleMNTask() {

        double disconnected = ((double)percDisconnected/100d) * (double)nmQtd;
        double full_connected = nmQtd - disconnected + 1;
        System.out.println("Disconnected: " + disconnected);

        for(int i =0; i <full_connected; i++){
            try {
                System.out.println("MN: " + String.valueOf(i));
                /*
                MNOutboundTask mn = new MNOutboundTask(port,
                        idAddress,
                        topic,
                        timeOff);
                ListMNT.add(mn);
                threadPool.execute(mn);
                Thread.sleep(interval);*/
            }catch (Exception e){

            }
        }

        for(int i =0; i < disconnected; i++){
            try {
                System.out.println("MN: " + String.valueOf(i));
                MNOutboundTask mn = new MNOutboundTask(port,
                        idAddress,
                        topic,
                        timeOff,
                        probability);
                ListMNT.add(mn);
                threadPool.execute(mn);
                Thread.sleep(interval);
            }catch (Exception e){

            }
        }
    }

    public void CreateFile()  {
        try {
            writer = new FileWriter(file, true);
        } catch (IOException e) {
            logger.error("Unable to Create File", e);
        }
    }

    public void Register(){
        logger.info("Register");
        CreateFile();

        for (MNOutboundTask mn : TestOutBound.ListMNT) {
            Write(mn.BuildReport());
            //Thread.sleep(regInterval);
        }
        CloseFile();

        System.out.println(threadPool.getActiveCount());
    }

    public void Write(String line){
        try {
            writer.write(line);
            writer.write('\n');
        } catch (IOException e) {
            logger.error("Unable to Write File", e);
        }
    }

    public void CloseFile(){
        try {
            writer.close();
        } catch (IOException e) {
            logger.error("Unable to Close File", e);
        }
    }
}
