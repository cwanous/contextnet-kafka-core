package ckafka.mobile.inbound;

import lac.cnclib.sddl.message.ApplicationMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SendGDMessageTask implements Runnable{

    private MNGroupTask mnt;
    ApplicationMessage Msg;
    final Logger logger = LoggerFactory.getLogger(SendMessageTask.class);


    public SendGDMessageTask (MNGroupTask MN){
        this.mnt = MN;
    }

    public SendGDMessageTask (MNGroupTask MN, ApplicationMessage msg){
        this.mnt = MN;
        this.Msg = msg;
    }

    @Override
    public void run() {
        try {
            ApplicationMessage msg = this.Msg;
            if(this.Msg == null){
                msg = mnt.createMessage();
            }

            if(msg != null){
                mnt.SendMessageToGateway(msg);
            }
        }catch (Exception e){
            logger.error("Error Send Message Task", e);
        }
    }
}
