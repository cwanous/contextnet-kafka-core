package ckafka.mobile.inbound;

import ckafka.mobile.tasks.RegisterOutTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


public class TestGroup {

    final Logger logger = LoggerFactory.getLogger(TestGroup.class);

    public static ArrayList<MNGroupTask> ListMNT;
    private Map<String,String> envVariables;

    private static Integer interval;
    private static Integer nmQtd;

    private static Integer port;
    private static String idAddress;
    private static Integer msgInterval;
    private static String topic;

    private static Integer changeInterval;

    private FileWriter writer;
    private static String file;        // mn.inbound.file

    private static ScheduledThreadPoolExecutor threadPool;

    public static void main(String[] args) {
        new TestGroup();
    }

    private TestGroup(){
        logger.info("Test Group");
        ListMNT = new ArrayList<>();

        GetEnvVariables();

        threadPool = new ScheduledThreadPoolExecutor(nmQtd+10);
        //threadPool.prestartAllCoreThreads();

        AddMultipleMNTask();

        StartRegister();

        RegisterOutTask register = new RegisterOutTask(this);
        threadPool.scheduleWithFixedDelay(register, 10, 180, TimeUnit.SECONDS);
        logger.info("SetUp Outbound End");
    }

    private void StartRegister() {
        logger.info("Start Register");
        for (MNGroupTask mn : TestGroup.ListMNT) {
            mn.Register();
        }
    }

    private void GetEnvVariables() {
        envVariables = System.getenv();

        interval = new Integer(envVariables.get("mn.group.interval"));

        nmQtd = new Integer(envVariables.get("mn.group.nmQtd"));

        port = new Integer(envVariables.get("mn.group.port"));
        idAddress = envVariables.get("mn.group.idAddress");
        msgInterval = new Integer(envVariables.get("mn.group.msgInterval"));
        topic = envVariables.get("mn.group.topic");

        file = envVariables.get("mn.group.file");

        changeInterval = new Integer(envVariables.get("mn.group.changeInterval"));
    }

    private void GetLocalVariables() {
        envVariables = System.getenv();

        interval = 40000;
        nmQtd = 1;
        port = 5500;
        idAddress = "127.0.0.1";
        topic = "Teste";
    }

    private static void AddMultipleMNTask() {

        for(int i =0; i < nmQtd; i++){
            try {
                System.out.println("MN: " + String.valueOf(i));
                MNGroupTask mn = new MNGroupTask(port,
                        idAddress,
                        msgInterval,
                        topic,
                        changeInterval);
                ListMNT.add(mn);
                threadPool.execute(mn);
                Thread.sleep(interval);
            }catch (Exception e){

            }
        }
    }

    public void CreateFile()  {
        try {
            writer = new FileWriter(file, true);
        } catch (IOException e) {
            logger.error("Unable to Create File", e);
        }
    }

    public void Register(){
        logger.info("Register");
        CreateFile();

        for (MNGroupTask mn : TestGroup.ListMNT) {
            Write(mn.BuildReport());
        }
        CloseFile();

        System.out.println(threadPool.getActiveCount());
    }

    public void Write(String line){
        try {
            writer.write(line);
            writer.write('\n');
        } catch (IOException e) {
            logger.error("Unable to Write File", e);
        }
    }

    public void CloseFile(){
        try {
            writer.close();
        } catch (IOException e) {
            logger.error("Unable to Close File", e);
        }
    }
}
