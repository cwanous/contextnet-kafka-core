package ckafka.mobile.inbound;

import ckafka.mobile.tasks.RegisterTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class TestInbound {

    final Logger logger = LoggerFactory.getLogger(TestInbound.class);

    public static ArrayList<MNInbound> ListMN;
    public static ArrayList<MNInboundTask> ListMNT;


    private Map<String,String> envVariables;

    private static Integer interval;        // mn.inbound.interval
    private static Integer nmQtd;        // mn.inbound.nmQtd

    private static Integer port;        // mn.inbound.port
    private static String idAddress;    // mn.inbound.idAddress
    private static Integer msgInterval; // mn.inbound.msgInterval
    private static Integer msgRate; // mn.inbound.msgRate
    private static String topic;        // mn.inbound.topic

    private FileWriter writer;
    private static String file;        // mn.inbound.file

    private static ScheduledThreadPoolExecutor threadPool;

    public static void main(String[] args) {
        new TestInbound();

        //Runtime.getRuntime().addShutdownHook(new Thread(TestInbound::closeMultipleMN));
    }

    private TestInbound(){
        logger.info("Test Inbound");
        ListMN = new ArrayList<>();
        ListMNT = new ArrayList<>();

        GetEnvVariables();

        threadPool = new ScheduledThreadPoolExecutor(nmQtd+10);
        //threadPool.prestartAllCoreThreads();

        AddMultipleMNTask();

        //ReSchedule();

        StartRegister();

        RegisterTask register = new RegisterTask(this);
        threadPool.scheduleWithFixedDelay(register, 10, 180, TimeUnit.SECONDS);
        logger.info("SetUp Inbound End");
    }


    private void StartRegister() {
        logger.info("Start Register");
        for (MNInboundTask mn : TestInbound.ListMNT) {
            mn.Register();
        }
    }

    public void CreateFile()  {
        try {
            writer = new FileWriter(file, true);
        } catch (IOException e) {
            logger.error("Unable to Create File", e);
        }
    }

    public void Register(){
        logger.info("Register");
        CreateFile();

        for (MNInboundTask mn : TestInbound.ListMNT) {
            Write(mn.BuildReport());
            //Thread.sleep(regInterval);
        }
        CloseFile();

        NumberOfThreads();
    }

    public void Write(String line){
        try {
            writer.write(line);
            writer.write('\n');
        } catch (IOException e) {
            logger.error("Unable to Write File", e);
        }
    }

    public void CloseFile(){
        try {
            writer.close();
        } catch (IOException e) {
            logger.error("Unable to Close File", e);
        }
    }

    private void PrintNumberOfThreads() {
        while (true){
            try {
                Thread.sleep(10000);
                System.out.println(threadPool.getActiveCount());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void NumberOfThreads() {
        System.out.println(threadPool.getActiveCount());
    }


    private void GetEnvVariables() {
        envVariables = System.getenv();

        interval = new Integer(envVariables.get("mn.inbound.interval"));

        nmQtd = new Integer(envVariables.get("mn.inbound.nmQtd"));

        port = new Integer(envVariables.get("mn.inbound.port"));
        idAddress = envVariables.get("mn.inbound.idAddress");
        msgInterval = new Integer(envVariables.get("mn.inbound.msgInterval"));
        msgRate = new Integer(envVariables.get("mn.inbound.msgRate"));
        topic = envVariables.get("mn.inbound.topic");

        file = envVariables.get("mn.inbound.file");
    }

    private void GetEnvVariablesUn() {
        envVariables = System.getenv();

        interval = new Integer(envVariables.get("mn_inbound_interval"));

        nmQtd = new Integer(envVariables.get("mn_inbound_nmQtd"));

        port = new Integer(envVariables.get("mn_inbound_port"));
        idAddress = envVariables.get("mn_inbound_idAddress");
        msgInterval = new Integer(envVariables.get("mn_inbound_msgInterval"));
        msgRate = new Integer(envVariables.get("mn_inbound_msgRate"));
        topic = envVariables.get("mn_inbound_topic");

        file = envVariables.get("mn_inbound_file");
    }


    private static void AddMultipleMN() {
        for(int i =0; i <nmQtd; i++){
            try {
                Thread.sleep(1000);
                System.out.println("MN: " + String.valueOf(i));
                MNInbound mn = new MNInbound(port,
                        idAddress,
                        msgInterval,
                        topic);
                ListMN.add(mn);
            }catch (Exception e){

            }
        }
    }

    private static void AddMultipleMNTask() {
        for(int i =0; i <nmQtd; i++){
            try {
                System.out.println("MN: " + String.valueOf(i));
                MNInboundTask mn = new MNInboundTask(port,
                        idAddress,
                        msgInterval,
                        msgRate,
                        topic);
                ListMNT.add(mn);

                threadPool.execute(mn);
                Thread.sleep(interval);
            }catch (Exception e){

            }
        }
    }



}
