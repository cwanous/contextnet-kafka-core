package ckafka.mobile.inbound;


import java.time.Instant;
import java.util.Map;
import java.util.UUID;

public class MNData {
    public UUID Id;
    public Map<Integer, Instant> sendMsgMap;
    public Map<Integer, Instant> receiveMsgMap;


    public MNData(UUID id,
                  Map<Integer, Instant> send,
                  Map<Integer, Instant> receive){

        Id = id;
        sendMsgMap = send;
        receiveMsgMap = receive;

    }

    public void Add(MNData data){
        sendMsgMap.putAll(data.sendMsgMap);
        receiveMsgMap.putAll(data.receiveMsgMap);
    }
}
