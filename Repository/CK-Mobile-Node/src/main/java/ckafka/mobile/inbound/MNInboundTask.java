package ckafka.mobile.inbound;


import ckafka.data.SwapData;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lac.cnclib.net.NodeConnection;
import lac.cnclib.net.NodeConnectionListener;
import lac.cnclib.net.mrudp.MrUdpNodeConnection;
import lac.cnclib.sddl.message.ApplicationMessage;
import lac.cnclib.sddl.message.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class MNInboundTask implements NodeConnectionListener, Runnable {

    final Logger logger = LoggerFactory.getLogger(MNInbound.class);

    private final UUID Id;

    /**
     *  Variables to locate the gateway, its IP address and port number **/
    private String gatewayIP;
    private int gatewayPort;
    private MrUdpNodeConnection connection;

    private volatile ScheduledFuture<?> sendMessageTask;
    private int MessageInterval;
    private int MessageRate;
    private String AppTopic;

    private final ScheduledThreadPoolExecutor threadPool;

    /**
     *  Message Register Map
     */
    public Map<Integer, Instant> sendMsgMap;
    public Map<Integer, Instant> receiveMsgMap;
    public HashSet<Integer> registred;
    private Integer messageCount;
    private Boolean register = false;

    /**
     *  Handle JSON Data
     */
    private ObjectMapper objectMapper;


    /**
     *  Report Data
     */
    public Map<Integer, Long> DurationMap;
    public Integer TotalMessages;
    public Integer UnansweredMessages;

    private Boolean run = true;

    public MNInboundTask(int port,
              String ipAddress,
              int msgInterval,
              int msgRate,
              String appTopic) {

        Id = UUID.randomUUID();
        //logger.info("Starting MNInbound " + Id.toString());

        gatewayPort = port;
        gatewayIP = ipAddress;
        MessageInterval = msgInterval;
        MessageRate = msgRate;
        AppTopic = appTopic;

        threadPool = new ScheduledThreadPoolExecutor(4);
        threadPool.prestartAllCoreThreads();

        sendMsgMap = new ConcurrentHashMap<>();
        receiveMsgMap = new ConcurrentHashMap<>();
        registred = new HashSet<>();
        messageCount = 0;

        objectMapper = new ObjectMapper();
    }

    public String BuildReport(){
        try {
            DurationMap = new HashMap<>();

            TotalMessages = sendMsgMap.keySet().size();

            UnansweredMessages = TotalMessages - receiveMsgMap.keySet().size();

            sendMsgMap.keySet().forEach(
                    (Integer count) -> {
                        if(!registred.contains(count)){
                            if(receiveMsgMap.containsKey(count)){
                                Long roundTrip =
                                        Duration.between(sendMsgMap.get(count), receiveMsgMap.get(count)).toMillis();
                                DurationMap.put(count, roundTrip);
                                registred.add(count);
                            }
                        }
                    }
            );

            return Id.toString() + ";" + UnansweredMessages + ";" + DurationMap.toString();

        }catch (Exception ignore){ return Id.toString(); }
    }


    public void ReSchedule(){
        if(sendMessageTask != null){
            sendMessageTask.cancel(true);
        }
        final SendMessageTask sendTask = new SendMessageTask(this);
        sendMessageTask = this.threadPool.scheduleWithFixedDelay(sendTask, 1000, MessageRate, TimeUnit.MILLISECONDS);
    }

    public void Register(){
        register = true;
        ReStartCount();
    }

    // MR-UDP Methods
    @Override
    public void connected(NodeConnection nodeConnection) {
        logger.info("Connected");
        try{
            logger.info("Connected");
            final SendMessageTask sendTask = new SendMessageTask(this);
            sendMessageTask = this.threadPool.scheduleWithFixedDelay(sendTask, 1000, MessageInterval, TimeUnit.MILLISECONDS);
        }catch (Exception e){
            logger.error("Error scheduling SendLocationTask", e);
        }
    }

    @Override
    public void reconnected(NodeConnection nodeConnection, SocketAddress socketAddress, boolean b, boolean b1) {

    }

    @Override
    public void disconnected(NodeConnection nodeConnection) {

    }

    @Override
    public void newMessageReceived(NodeConnection nodeConnection, Message message) {
        //logger.info("New Message Received");
        try{
            Instant now = Instant.now();
            SwapData swp = (SwapData) message.getContentObject();
            String topic = swp.getTopic();

            if(topic.equals(AppTopic)){
                RegisterMessage(swp, now);
            }else if(topic.equals("Ping")){
                message.setSenderID(this.Id);
                SendMessageToGateway(message);
            }

        }catch (Exception e){
            logger.error("Error reading new message received");
        }
    }


    // Message Send Task
    ApplicationMessage createMessage(int count){
        //logger.info("Create Message");
        try {
            ApplicationMessage message = new ApplicationMessage();
            message.setSenderID(Id);

            message.setContentObject(CreateContent(count));

            //Register
            if(sendMsgMap.containsKey(count)){
                // Register already used
                return null;
            }



            return message;
        }catch (Exception e){
            logger.error("Error creating message", e);
            return null;
        }
    }

    public void Register(int count){
        if(register){
            Instant now = Instant.now();
            sendMsgMap.put(count, now);
        }
    }

    public synchronized int IncrementCount(){
        messageCount++;
        return messageCount;
    }

    public synchronized void ReStartCount(){
        messageCount=0;
    }

    private SwapData CreateContent(Integer MessageCounter) {
        ObjectNode context = objectMapper.createObjectNode();

        byte[] messageContent = getCounterAsByte(MessageCounter);

        SwapData content = new SwapData();
        content.setContext(context);
        content.setMessage(messageContent);
        content.setTopic(AppTopic);
        content.setDuration(60);
        return content;
    }


    // Register Return Message
    private void RegisterMessage(SwapData swp, Instant now){
        //logger.info("Register Message");
        try {
            int counter = getCounterAsInteger(swp.getMessage());
            if(!receiveMsgMap.containsKey(counter) && sendMsgMap.containsKey(counter)){
                receiveMsgMap.put(counter, now);
            }
        } catch (Exception e) {
            logger.error("Error Register Message", e);
        }
    }


    // Counter Manipulation
    private byte[] getCounterAsByte(Integer counter){
        return counter.toString().getBytes(StandardCharsets.UTF_8);
    }

    private Integer getCounterAsInteger(byte[] counter){
        return new Integer (new String(counter, StandardCharsets.UTF_8));
    }


    // Send MR-UPD Message to Gateway
    void SendMessageToGateway(Message message){
        try {
            //logger.info("Send new message");
            this.connection.sendMessage(message);
        } catch (Exception e) {
            logger.error("Message could not be sent to Gateway", e);
            run = false;
        }
    }


    private void Disconnect(){
        try {
            logger.info("Disconnect");
            this.connection.disconnect();
        } catch (Exception e) {
            logger.error("Could not Disconnect", e);
        }
    }



    @Override
    public void run() {
        InetSocketAddress address = new InetSocketAddress(gatewayIP, gatewayPort);
        try {
            // Create a connection object that encapsulate the details and protocol involved in the physical connection with GW
            connection = new MrUdpNodeConnection();
            // Add a listener to the connection object which provides methods to react to the node GW connection
            connection.addNodeConnectionListener(this);
            // Establish the connection
            connection.connect(address);
        } catch (IOException e) {
            logger.error("Unable to connect", e);
            logger.info("Stop");
            return;
        }

        while (run){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        try {
            this.connection.disconnect();
            this.connection.getSocket().close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        threadPool.shutdown();
        logger.info("Stop");
    }



    @Override
    public void unsentMessages(NodeConnection nodeConnection, List<Message> list) {

    }
    @Override
    public void internalException(NodeConnection nodeConnection, Exception e) {

    }

}
