package ckafka.mobile.inbound;

import lac.cnclib.sddl.message.ApplicationMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SendMessageOutTask implements Runnable{

    private MNOutboundTask mnt;
    ApplicationMessage Msg;
    final Logger logger = LoggerFactory.getLogger(SendMessageTask.class);

    public SendMessageOutTask (MNOutboundTask MN){
        this.mnt = MN;
    }

    public SendMessageOutTask (MNOutboundTask MN, ApplicationMessage msg){
        this.mnt = MN;
        this.Msg = msg;
    }

    @Override
    public void run() {
        try {
            ApplicationMessage msg = this.Msg;
            if(this.Msg == null){
                msg = mnt.createMessage();
            }

            if(msg != null){
                mnt.SendMessageToGateway(msg);
            }
        }catch (Exception e){
            logger.error("Error Send Message Task", e);
        }
    }
}
