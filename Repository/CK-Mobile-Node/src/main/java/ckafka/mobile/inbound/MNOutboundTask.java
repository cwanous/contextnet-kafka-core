package ckafka.mobile.inbound;

import ckafka.data.SwapData;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lac.cnclib.net.NodeConnection;
import lac.cnclib.net.NodeConnectionListener;
import lac.cnclib.net.mrudp.MrUdpNodeConnection;
import lac.cnclib.sddl.message.ApplicationMessage;
import lac.cnclib.sddl.message.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;

public class MNOutboundTask implements NodeConnectionListener, Runnable {

    final Logger logger = LoggerFactory.getLogger(MNInbound.class);

    private final UUID Id;
    private boolean begin = true;
    /**
     *  Variables to locate the gateway, its IP address and port number **/
    private String gatewayIP;
    private int gatewayPort;
    private MrUdpNodeConnection connection;

    private volatile ScheduledFuture<?> sendMessageTask;
    private int MessageInterval;
    private int MessageRate;
    private String AppTopic;

    private final ScheduledThreadPoolExecutor threadPool;

    /**
     *  Message Register Map
     */
    public Map<Integer, Instant> sendMsgMap;
    public Map<Integer, Instant> receiveMsgMap;
    public HashSet<Integer> registred;
    private Integer messageCount;
    private Boolean register = false;

    /**
     *  Handle JSON Data
     */
    private ObjectMapper objectMapper;


    /**
     *  Report Data
     */
    public Map<Integer, Long> DurationMap;
    public Integer TotalMessages;
    public Integer UnansweredMessages;

    private Boolean run = true;

    public Integer ReceivedMessages = 0;
    public Integer Probability = 0;
    public Integer TimeOff = 60000;
    public Random Rand;


    public MNOutboundTask(int port,
                          String ipAddress,
                          Integer timeOff,
                          String appTopic) {

        Id = UUID.randomUUID();
        //logger.info("Starting MNInbound " + Id.toString());

        gatewayPort = port;
        gatewayIP = ipAddress;
        AppTopic = appTopic;
        TimeOff = timeOff;

        threadPool = new ScheduledThreadPoolExecutor(4);
        threadPool.prestartAllCoreThreads();

        sendMsgMap = new ConcurrentHashMap<>();
        receiveMsgMap = new ConcurrentHashMap<>();
        registred = new HashSet<>();
        messageCount = 0;

        objectMapper = new ObjectMapper();
    }

    public MNOutboundTask(int port,
                          String ipAddress,
                          String appTopic,
                          Integer timeOff,
                          Integer prob) {

        Id = UUID.randomUUID();

        Probability = prob;
        Rand = new Random();
        //logger.info("Starting MNInbound " + Id.toString());

        gatewayPort = port;
        gatewayIP = ipAddress;
        AppTopic = appTopic;
        TimeOff = timeOff;

        threadPool = new ScheduledThreadPoolExecutor(4);
        threadPool.prestartAllCoreThreads();

        sendMsgMap = new ConcurrentHashMap<>();
        receiveMsgMap = new ConcurrentHashMap<>();
        registred = new HashSet<>();
        messageCount = 0;

        objectMapper = new ObjectMapper();
    }



    // MR-UDP Methods
    @Override
    public void connected(NodeConnection nodeConnection) {
        logger.info("Connected");
        try{
            if(begin){
                final SendMessageOutTask sendTask = new SendMessageOutTask(this);
                this.threadPool.execute(sendTask);
                begin = false;
            }else {
                final SendMessageOutTask sendTask = new SendMessageOutTask(this, createContextMessage());
                this.threadPool.execute(sendTask);
            }
        }catch (Exception e){
            logger.error("Error scheduling SendLocationTask", e);
        }
    }

    @Override
    public void reconnected(NodeConnection nodeConnection, SocketAddress socketAddress, boolean b, boolean b1) {

    }

    @Override
    public void disconnected(NodeConnection nodeConnection) {

    }

    @Override
    public void newMessageReceived(NodeConnection nodeConnection, Message message) {
        try{
            SwapData swp = (SwapData) message.getContentObject();
            String topic = swp.getTopic();

            if(topic.equals("PrivateMessageTopic")){
                final SendMessageOutTask sendTask = new SendMessageOutTask(this, createMessage(swp));
                this.threadPool.execute(sendTask);
                RegisterReceived(swp);
            }else if(topic.equals("Ping")){
                message.setSenderID(this.Id);
                SendMessageToGateway(message);
            }

        }catch (Exception e){
            logger.error("Error reading new message received");
        }
    }

    private void RegisterReceived(SwapData swp){
        try{
            Instant now = Instant.now();
            if(register){
                int counter = getCounterAsInteger(swp.getMessage());
                if(counter > 0){
                    receiveMsgMap.putIfAbsent(counter, now);
                    ReceivedMessages++;
                }
            }
        }catch (Exception e){
            logger.error("Error register received msg", e);
        }
    }

    private Integer getCounterAsInteger(byte[] counter){
        return new Integer (new String(counter, StandardCharsets.UTF_8));
    }

    // Message Send Task
    ApplicationMessage createMessage(){
        try {
            ApplicationMessage message = new ApplicationMessage();
            message.setContentObject(CreateContent());
            message.setSenderID(Id);
            return message;
        }catch (Exception e){
            logger.error("Error creating message", e);
            return null;
        }
    }

    private ApplicationMessage createContextMessage(){
        try {
            ApplicationMessage message = new ApplicationMessage();
            message.setContentObject(CreateContextContent());
            message.setSenderID(Id);
            return message;
        }catch (Exception e){
            logger.error("Error creating message", e);
            return null;
        }
    }

    private ApplicationMessage createMessage(SwapData swp){
        try {
            ApplicationMessage message = new ApplicationMessage();
            message.setContentObject(CreateContent(swp));
            message.setSenderID(Id);
            return message;
        }catch (Exception e){
            logger.error("Error creating message", e);
            return null;
        }
    }


    private SwapData CreateContent(SwapData swp) {
        ObjectNode context = objectMapper.createObjectNode();

        SwapData content = new SwapData();
        content.setContext(context);
        content.setMessage(swp.getMessage());
        content.setTopic(AppTopic);
        content.setDuration(60);
        return content;
    }

    private SwapData CreateContent() {
        ObjectNode context = objectMapper.createObjectNode();

        byte[] message = new byte[1];
        SwapData content = new SwapData();
        content.setContext(context);
        content.setMessage(message);
        content.setTopic(AppTopic);
        content.setDuration(60);
        return content;
    }

    private SwapData CreateContextContent() {
        ObjectNode context = objectMapper.createObjectNode();

        SwapData content = new SwapData();
        content.setContext(context);
        content.setDuration(60);
        return content;
    }


    // Send MR-UPD Message to Gateway
    void SendMessageToGateway(Message message){
        try {
            this.connection.sendMessage(message);
        } catch (Exception e) {
            logger.error("Message could not be sent to Gateway", e);
            run = false;
        }
    }


    private void Disconnect(){
        try {
            logger.info("Disconnect");
            this.connection.disconnect();
            this.connection.getSocket().close();
        } catch (Exception e) {
            logger.error("Could not Disconnect", e);
        }
    }


    /*public void Reconnection(){
        if(Probability > 0){
            Integer next = Rand.nextInt(Probability);
            if(next == 0){
                try {
                    Disconnect();
                    Thread.sleep(60000);
                    Connect();
                    Thread.sleep(60000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }*/

    public void Reconnection(){
        if(Probability > 0){
            try {
                Disconnect();
                Thread.sleep(TimeOff);
                Connect();
                Thread.sleep(120000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    public void run() {
        Connect();
        try {
            Thread.sleep(120000);
            while (run){
                Thread.sleep(60000);
                Reconnection();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        try {
            this.connection.disconnect();
            this.connection.getSocket().close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        threadPool.shutdown();
        logger.info("Stop");

    }

    private void Connect(){
        InetSocketAddress address = new InetSocketAddress(gatewayIP, gatewayPort);
        try {
            // Create a connection object that encapsulate the details and protocol involved in the physical connection with GW
            connection = new MrUdpNodeConnection();
            // Add a listener to the connection object which provides methods to react to the node GW connection
            connection.addNodeConnectionListener(this);
            // Establish the connection
            connection.connect(address);
        } catch (IOException e) {
            logger.error("Unable to connect", e);
            logger.info("Stop");
            return;
        }
    }

    public void Register() {
        register = true;
    }

    @Override
    public void unsentMessages(NodeConnection nodeConnection, List<Message> list) {

    }
    @Override
    public void internalException(NodeConnection nodeConnection, Exception e) {

    }

    public String BuildReport(){
        return Id.toString() + ";" + receiveMsgMap.keySet().size() + ";" +ReceivedMessages.toString();
    }

}
