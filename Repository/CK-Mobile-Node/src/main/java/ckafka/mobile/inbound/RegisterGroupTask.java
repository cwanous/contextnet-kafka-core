package ckafka.mobile.inbound;


public class RegisterGroupTask implements Runnable {

    private TestOutBound inbound;
    public RegisterGroupTask(TestOutBound Inbound){
        inbound = Inbound;
    }

    @Override
    public void run() {
        inbound.Register();
    }
}
